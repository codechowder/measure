﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeChowder.CSMS.Roslyn.Parsers
{
    

    public class CSharpAst
    {
        /// <summary>
        /// Contains the collection of AST roots
        /// </summary>
        public List<CompilationUnitSyntax> ASTRoot { get; private set; }
        
        /// <summary>
        /// Constructs the AST Roots
        /// </summary>
        /// <param name="csFiles">C# files to parse</param>
        public CSharpAst(IEnumerable<string> csFiles) {

            ASTRoot = new List<CompilationUnitSyntax>();

            foreach (string csFile in csFiles) {
                SyntaxTree tree = CSharpSyntaxTree.ParseText(File.ReadAllText(csFile));
                ASTRoot.Add((CompilationUnitSyntax)tree.GetRoot());
            }

        }
    }
}