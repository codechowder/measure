﻿using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using System.Linq;

namespace CodeChowder.CSMS.Roslyn.Parsers
{
    public static class Common
    {
        public const string WMC_METRIC_NAME = "WMC";
        public const string DCC_METRIC_NAME = "DCC";
        public const string LCOM_METRIC_NAME = "LCOM";

        public static void RecursiveGrabItem<T>(SyntaxNode node, ref List<T> items) where T : class
        {
            items.AddRange(node.ChildNodes().Where(i => i is T).Select(i => i as T));

            foreach (SyntaxNode childNode in node.ChildNodes())
            {
                RecursiveGrabItem(childNode, ref items);
            }
        }

        public static void RecursiveParentGrabItem<T>(SyntaxNode node, ref List<T> items) where T: class
        {
            items.AddRange(node.Ancestors().Where(i => i is T).Select(i => i as T));

            foreach (SyntaxNode ancestor in node.Ancestors())
            {
                RecursiveParentGrabItem(ancestor, ref items);
            }
        }
    }
}