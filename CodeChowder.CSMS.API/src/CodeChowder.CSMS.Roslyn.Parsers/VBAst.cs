﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.VisualBasic;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using System.Collections.Generic;
using System.IO;

namespace CodeChowder.CSMS.Roslyn.Parsers
{
    public class VBAst
    {
        /// <summary>
        /// Contains the collection of AST roots
        /// </summary>
        public List<CompilationUnitSyntax> ASTRoot { get; private set; }

        /// <summary>
        /// Constructs the AST Roots
        /// </summary>
        /// <param name="csFiles">VB files to parse</param>
        public VBAst(IEnumerable<string> vbFiles) {

            ASTRoot = new List<CompilationUnitSyntax>();

            foreach (string vbFile in vbFiles) {
                SyntaxTree tree = VisualBasicSyntaxTree.ParseText(File.ReadAllText(vbFile));
                ASTRoot.Add((CompilationUnitSyntax)tree.GetRoot());
            }
        }
    }
}