﻿using AutoMapper;
using CodeChowder.CSMS.API.DTOs;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.API.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            AllowNullCollections = true;

            CreateMap<ApplicationUser, ApplicationUserDto>();
            CreateMap<ProjectMetrics, ProjectMetricsDto>();
            CreateMap<MetricResult, MetricResultDto>();
            CreateMap<Project, ProjectDto>();
            CreateMap<SharedProject, SharedProjectDto>();
            CreateMap<ApplicationUserDto, ApplicationUser>()
                .ForMember(u => u.Id, opt => opt.Ignore())
                .ForMember(u => u.Projects, opt => opt.Ignore())
                .ForMember(u => u.SharedProjects, opt => opt.Ignore())
                .ForMember(u => u.License, opt => opt.Ignore());
            CreateMap<ProjectDto, Project>()
                .ForMember(p => p.Id, opt => opt.Ignore())
                .ForMember(p => p.CustomMetrics, opt => opt.Ignore())
                .ForMember(p => p.IgnoreFiles, opt => opt.Ignore())
                .ForMember(p => p.ProjectMetrics, opt => opt.Ignore())
                .ForMember(p => p.SeverityTypes, opt => opt.Ignore())
                .ForMember(p => p.SharedWith, opt => opt.Ignore());
        }
    }
}