﻿using System;
namespace CodeChowder.CSMS.API.Models
{
    public enum LicenseStatus {
        Invalid = 0,
        Valid = 1,
        InUse = 2,
        Expired = 3
    }
}
