﻿using System;
using System.Collections.Generic;

namespace CodeChowder.CSMS.API.Models
{
    public class GitRepoInfo
    {
        public string Url { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string SelectedBranch { get; set; }

        public IList<GitCommit> GitCommits { get; set; }
    }
}
