﻿
using System;
using CodeChowder.CSMS.API.Settings;

namespace CodeChowder.CSMS.API.Models
{
    public class LicenseData : DefaultSettings
    {
        public LicenseData(DefaultSettings settings = null) {
            if (settings != null) {
                this.AllowedProjects = settings.AllowedProjects;
                this.DefaultAccount = settings.DefaultAccount;
                this.ConnectionString = null;
                this.MaxFreeMetrics = settings.MaxFreeMetrics;
            }
        }

        public int AllowedUsers { get; set; }
        public bool AllowShare { get; set; }
        public DateTime? ExperationDate { get; set; } = null;
	}
}
