﻿using System;

public class GitCommit {
    public string Commit { get; set; }
    public string Message { get; set; }
    public DateTime Date { get; set; }
}
