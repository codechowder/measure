﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using CodeChowder.CSMS.DataStructures.Enumerations;
using CodeChowder.CSMS.Roslyn.Parsers;
using System.Diagnostics;
using Newtonsoft.Json;
using CodeChowder.CSMS.API.Security;
using CodeChowder.CSMS.API.Settings;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using CodeChowder.CSMS.API.ResponseModels;
using LibGit2Sharp;
using CodeChowder.CSMS.API.Models;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using CodeChowder.CSMS.API.Helpers;
using CodeChowder.CSMS.API.DTOs;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/projects")]
    public class ProjectsController : BaseController
    {
        private readonly IProjectService _projectService;
        private readonly IProjectMetricsService _projectMetricService;
        private readonly IMetricSeverityService _metricSeverityService;
        private readonly IMetricResultService _metricResultService;
        private readonly IHostingEnvironment _hostingEnv;
        private readonly ExecutionSettings _executionSettings;
        private readonly DefaultSettings _defaultSettings;
        private readonly TokenSettings _tokenSettings;
        private readonly IApplicationUserService _userService;

        public ProjectsController(IProjectService projectService,
                                  IProjectMetricsService projectMetricService,
                                  IMetricSeverityService metricSeverityService,
                                  IMetricResultService metricResultService,
                                  IApplicationUserService userService,
                                  IHostingEnvironment hostingEnv,
                                  IOptions<ExecutionSettings> executionSettings,
                                  IOptions<DefaultSettings> defaultSettings,
                                  IOptions<TokenSettings> tokenSettings) : base()
        {
            _projectService = projectService;
            _projectMetricService = projectMetricService;
            _metricSeverityService = metricSeverityService;
            _metricResultService = metricResultService;
            _userService = userService;
            _hostingEnv = hostingEnv;
            _executionSettings = executionSettings.Value;
            _defaultSettings = defaultSettings.Value;
            _tokenSettings = tokenSettings.Value;
        }

        // GET: api/values
        [HttpGet("{skip}/{limit}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Get(int skip, int limit)
        {
            //TODO:  This is probably wrong...
            ApplicationUser user = await _userService.ReadById(u => u.Id == CurrentUserId, u => u.Projects);
            int count = user.Projects.Count();
            IEnumerable<Project> projects = user.Projects.Skip(skip).Take(limit);
            return Ok(new GenericResponse() { Success = true, Count = count, Message = projects });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Get(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project foundProject = await _projectService.Queryable
                                                        .Include(p => p.SeverityTypes)
                                                        .Include(p => p.IgnoreFiles)
                                                        .Include(p => p.ProjectMetrics)
                                                        .ThenInclude(pm => pm.MetricResults)
                                                        .FirstOrDefaultAsync(p => p.Id == id);

            if (foundProject == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            ProjectDto projectDto = Mapper.Map<Project, ProjectDto>(foundProject);

            return Ok(new GenericResponse() { Success = true, Message = projectDto });
        }


        // GET: api/values
        [HttpPost("git/branches")]
        public IActionResult GetBranches([FromBody]GitRepoInfo gitRepoInfo)
        {
            LibGit2Sharp.Handlers.CredentialsHandler handler = (_url, _user, _cred) => new UsernamePasswordCredentials { Username = gitRepoInfo.UserName, Password = gitRepoInfo.Password };
            IEnumerable<Reference> refs = LibGit2Sharp.Repository.ListRemoteReferences(gitRepoInfo.Url, handler);

            return Ok(new GenericResponse() { Success = true, Message = refs.Where(r => r.IsLocalBranch || r.IsRemoteTrackingBranch).Select(r => r.CanonicalName.Replace("refs/heads/", "")) });
        }

        [HttpGet("{id}/metricsCount")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetMetricsCount(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project project = await _projectService.ReadById(p => p.Id == id);

            if (project == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            int metricsCount = _metricResultService.Queryable.Count(m => m.ProjectMetric.Project.Id == id);

            return Ok(new GenericResponse() { Success = true, Message = metricsCount });
        }

		[HttpGet("{id}/totalSloc")]
		[ClaimRequirement("UserId", "{UserId}")]
		public async Task<IActionResult> GetSlocTotal(int id)
		{
			if (!(await UserHasPermissions(id)))
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
			}

			Project project = await _projectService.ReadById(p => p.Id == id);

			if (project == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
			}

			ProjectMetrics projectMetrics = await _projectMetricService.Queryable.OrderByDescending(pm => pm.Id).FirstOrDefaultAsync(pm => pm.Project.Id == id);

			int lastRunSlocTotal = 0;

			if (projectMetrics != null)
			{
				int projectMetricId = projectMetrics.Id;

				lastRunSlocTotal = _metricResultService.Queryable.Where(m => m.ProjectMetric.Id == projectMetricId && m.MetricName == "SLOC").Sum(mr => mr.Value);
			}

			return Ok(new GenericResponse() { Success = true, Message = lastRunSlocTotal });
		}


		[HttpGet("{id}/severeMetrics")]
        [ClaimRequirement("UserId", "{UserId}")]
		public async Task<IActionResult> GetSevereMetrics(int id)
		{
			if (!(await UserHasPermissions(id)))
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
			}

			Project project = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

			if (project == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
			}

			ProjectMetrics projectMetrics = await _projectMetricService.Queryable.OrderByDescending(pm => pm.Id).FirstOrDefaultAsync(pm => pm.Project.Id == id);

			List<MetricResultDto> severeMetricsResults = new List<MetricResultDto>();

            if (projectMetrics != null)
            {
                int projectMetricId = projectMetrics.Id;

                IEnumerable<MetricResult> severeMetrics = _metricResultService.Queryable.Where(m => m.ProjectMetric.Id == projectMetricId &&
                                                                                               project.SeverityTypes.Any(s => s.MetricName == m.MetricName &&
                                                                                                                         s.SeverityType == MetricSeverityType.Severe &&
                                                                                                                         s.SourceCode == m.SourceType &&
                                                                                                                         m.Value >= s.Value));
                severeMetricsResults = Mapper.Map<List<MetricResult>, List<MetricResultDto>>(severeMetrics.ToList());
            }
			return Ok(new GenericResponse() { Success = true, Message = severeMetricsResults });
		}

		[HttpGet("{id}/cautiousMetrics")]
		[ClaimRequirement("UserId", "{UserId}")]
		public async Task<IActionResult> GetCautiousMetrics(int id)
		{
			if (!(await UserHasPermissions(id)))
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
			}

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

			if (project == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
			}

			ProjectMetrics projectMetrics = await _projectMetricService.Queryable.OrderByDescending(pm => pm.Id).FirstOrDefaultAsync(pm => pm.Project.Id == id);

            List<MetricResultDto> cautiousMetricsResults = new List<MetricResultDto>();

            if (projectMetrics != null)
            {
                int projectMetricId = projectMetrics.Id;
                IEnumerable<MetricResult> cautiousMetrics = _metricResultService.Queryable.Where(m => m.ProjectMetric.Id == projectMetricId &&
                                                                                               project.SeverityTypes.Any(s => s.MetricName == m.MetricName &&
                                                                                                     s.SeverityType == MetricSeverityType.Acceptable &&
                                                                                                     s.SourceCode == m.SourceType &&
                                                                                                     m.Value > s.Value &&
                                                                                                     project.SeverityTypes.Any(ss => ss.MetricName == m.MetricName
                                                                                                                               && ss.SeverityType == MetricSeverityType.Severe
                                                                                                                               && s.SourceCode == m.SourceType
                                                                                                                               && m.Value < ss.Value)));
                cautiousMetricsResults = Mapper.Map<List<MetricResult>, List<MetricResultDto>>(cautiousMetrics.ToList());
            }
			return Ok(new GenericResponse() { Success = true, Message = cautiousMetricsResults });
		}

        // GET: api/values
        [HttpGet("{id}/ignoreFiles/{skip}/{limit}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetIgnoreFiles(int id, int skip, int limit)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles);

            if (project == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            int count = project.IgnoreFiles.Count;

            List<IgnoreFile> ignoreFiles = project.IgnoreFiles.OfType<IgnoreFile>().Skip(skip).Take(limit).ToList();

            return Ok(new GenericResponse() { Success = true, Count = count, Message = ignoreFiles });
        }

        // GET: api/values
        [HttpGet("{id}/ignoreFiles")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetIgnoreFiles(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles);

            if (project == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            return Ok(new GenericResponse() { Success = true, Message = project.IgnoreFiles });
        }

        // GET: api/values
        [HttpGet("{id}/metricSeverities/{skip}/{limit}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetMetricSeverities(int id, int skip, int limit)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

            if (project == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            int count = project.SeverityTypes.Count;

            List<MetricSeverity> metricSeverities = project.SeverityTypes.OfType<MetricSeverity>().Skip(skip).Take(limit).ToList();

            return Ok(new GenericResponse() { Success = true, Count = count, Message = metricSeverities });
        }

        // GET api/values/5
        // GET: api/values
        [HttpGet("{id}/metricSeverities")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetMetricSeverities(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

            if (project == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            return Ok(new GenericResponse() { Success = true, Message = project.SeverityTypes });
        }

        // GET api/values/5
        [HttpGet("{id}/settings")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetProjectSettings(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project foundProject = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes, p => p.IgnoreFiles);

            ProjectDto projectDto = Mapper.Map<Project, ProjectDto>(foundProject);

            return Ok(new GenericResponse() { Success = true, Message = projectDto });
        }

        [HttpGet("{id}/metricsExcel/{projectMetricId?}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetProjectMetricsExcel(int id, int? projectMetricId)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project foundProject = await _projectService.ReadById(p => p.Id == id, p => p.ProjectMetrics);

            if (foundProject == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            MemoryStream memoryStream = new MemoryStream();
            using (SpreadsheetDocument spreadSheet = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook))
            {
                WorkbookPart workbookpart = spreadSheet.AddWorkbookPart();

                workbookpart.Workbook = new Workbook();

                if (workbookpart.Workbook.Sheets == null)
                {
                    workbookpart.Workbook.AppendChild<Sheets>(new Sheets());
                }

                workbookpart.Workbook.Save();

                WorkbookStylesPart wbsp = spreadSheet.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                wbsp.Stylesheet = ExcelHelper.GenerateStyleSheet();
                wbsp.Stylesheet.Save();

                if (projectMetricId == null)
                {
                    int runNumber = 1;
                    IList<ProjectMetrics> projectMetricsList = _projectMetricService.ReadMany(pm => pm.Project.Id == id, pm => pm.MetricResults).ToList();
                    foreach (ProjectMetrics projectMetrics in projectMetricsList)
                    {
                        AddMetricWorksheet(spreadSheet, projectMetrics, "WMC", "Complexity", runNumber);
                        AddMetricWorksheet(spreadSheet, projectMetrics, "DCC", "Coupling", runNumber);
                        AddMetricWorksheet(spreadSheet, projectMetrics, "LCOM", "Cohesion", runNumber);
                        AddMetricWorksheet(spreadSheet, projectMetrics, "SLOC", "SLOC", runNumber);
                        runNumber++;
                    }
                }
                else
                {
                    ProjectMetrics projectMetrics = await _projectMetricService.ReadById(pm => pm.Id == projectMetricId.Value, pm => pm.MetricResults);
                    AddMetricWorksheet(spreadSheet, projectMetrics, "WMC", "Complexity", 0);
                    AddMetricWorksheet(spreadSheet, projectMetrics, "DCC", "Coupling", 0);
                    AddMetricWorksheet(spreadSheet, projectMetrics, "LCOM", "Cohesion", 0);
                    AddMetricWorksheet(spreadSheet, projectMetrics, "SLOC", "SLOC", 0);
                }

                spreadSheet.Save();
            }

            //reset the position to the start of the stream
            memoryStream.Seek(0, SeekOrigin.Begin);

            return new FileStreamResult(memoryStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        }

        [HttpGet("{id}/companyName")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetCompanyName(int id) {
            ApplicationUser user = await _userService.Queryable
                .FirstOrDefaultAsync(u => u.Projects.Any(p => p.Id == id));
            
            if (user == null) {
                return BadRequest(new GenericResponse { Success = false, Message = "Could not find project." });
            }

            if (!(await UserHasPermissions(id))) {
                return BadRequest(new GenericResponse { Success = false, Message = "Insufficient permissions." });
            }

            return Ok(new GenericResponse { Success = true, Message = user.Company });
        }

        private void AddMetricWorksheet(SpreadsheetDocument spreadSheet, ProjectMetrics projectMetrics, string metricName, string worksheetName, int runNumber)
        {
            //get the severity for each source type
            // Add a blank WorksheetPart.
            // Give the new worksheet a name.
            string sheetName = $"{worksheetName}";
            if (runNumber > 0)
            {
                sheetName = $"{sheetName} Run - {runNumber}";
            }

            WorksheetPart worksheetPart = ExcelHelper.InsertWorksheet(spreadSheet.WorkbookPart, sheetName);
            uint row = 1;

            //Header

            ExcelHelper.InsertText(spreadSheet, worksheetPart, "A", row, "Location", null);
            ExcelHelper.InsertText(spreadSheet, worksheetPart, "B", row, "Source Type", null);
            ExcelHelper.InsertText(spreadSheet, worksheetPart, "C", row, "Namespace", null);
            ExcelHelper.InsertText(spreadSheet, worksheetPart, "D", row, "Object Name", null);
            ExcelHelper.InsertText(spreadSheet, worksheetPart, "E", row, "Value", null);

            row++;

            List<MetricSeverity> metricSeverities = _metricSeverityService.Queryable.Where(ms => ms.MetricName == metricName).ToList();

            foreach (MetricResult metricResult in projectMetrics.MetricResults.Where(mr => mr.MetricName == metricName).OrderByDescending(mr => mr.Value))
            {

                ExcelHelper.InsertText(spreadSheet, worksheetPart, "A", row, metricResult.Location, null);
                ExcelHelper.InsertText(spreadSheet, worksheetPart, "B", row, metricResult.SourceType.ToString(), null);
                ExcelHelper.InsertText(spreadSheet, worksheetPart, "C", row, metricResult.NameSpace, null);
                ExcelHelper.InsertText(spreadSheet, worksheetPart, "D", row, metricResult.ObjectName, null);

                MetricSeverity metricSeverity = metricSeverities.Where(ms => (int)ms.SourceCode == (int)metricResult.SourceType &&
                                                                                            ms.Value < metricResult.Value).OrderBy(ms => ms.SeverityType)
                                                                            .FirstOrDefault();

                MetricSeverityType severityType = MetricSeverityType.Acceptable;

                if (metricSeverity != null)
                {
                    severityType = metricSeverity.SeverityType;
                    if (severityType == MetricSeverityType.Acceptable)
                    {
                        severityType = MetricSeverityType.Cautious;
                    }
                }

                ExcelHelper.InsertText(spreadSheet, worksheetPart, "E", row, metricResult.Value.ToString(), severityType);

                row++;

            }
        }

        [HttpGet("{id}/projectMetrics")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetProjectMetrics(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            IEnumerable<ProjectMetrics> foundProjectMetrics = _projectMetricService.ReadMany(pm => pm.Project.Id == id, pm => pm.MetricResults);

            if (foundProjectMetrics == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            IEnumerable<ProjectMetricsDto> projectMetricsDto = Mapper.Map<IEnumerable<ProjectMetrics>, IEnumerable<ProjectMetricsDto>>(foundProjectMetrics);

            return Ok(new GenericResponse() { Success = true, Message = projectMetricsDto });
        }

        [HttpGet("{id}/metrics")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetMetricResults(int id)
        {
            if (!(await UserHasPermissions(id)))
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find User" });
            }

            Project foundProject = await _projectService.Queryable.Include(p => p.SeverityTypes)
                                                        .Include(p => p.ProjectMetrics)
                                                        .ThenInclude(pm => pm.MetricResults)
                                                        .FirstOrDefaultAsync(p => p.Id == id);

            if (foundProject == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            ProjectDto projectDto = Mapper.Map<Project, ProjectDto>(foundProject);

            return Ok(new GenericResponse() { Success = true, Message = projectDto });
        }

        // PUT api/values/5
        [HttpPut]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Put([FromBody]ProjectDto value)
        {
            if(!(await UserHasPermissions(value.Id))) {
                return NotFound(new GenericResponse() { Success = false, Message = "Could not find project on this user" });
            }

            Project project = null;
            if (value != null && value.Id > 0)
            {
                Project currentProject = await _projectService.ReadById(p => p.Id == value.Id);

                if (currentProject == null)
                {
                    return NotFound(new GenericResponse() { Success = false, Message = "Could not find project for current user" });
                }

                Mapper.Map(value, currentProject);

                bool success = _projectService.Update(currentProject) && _projectService.Save();
                if (success)
                {
                    project = currentProject;
                }
                else
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not save project" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = project });
        }

        [HttpPost("{id}/ignoreFiles")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> AddIgnoreFiles(int id, [FromBody]IgnoreFile value)
        {
            if (value != null && (await UserHasPermissions(id)))
            {
                Project currentProject = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles);

                if (currentProject == null)
                {
                    return NotFound(new GenericResponse() { Success = false, Message = "Could not find project for current user" });
                }

                currentProject.IgnoreFiles.Add(value);

                bool success = _projectService.Update(currentProject) && _projectService.Save();
                if (!success)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not save project" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = value });
        }

        [HttpPut("{id}/ignoreFiles")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> EditIgnoreFile(int id, [FromBody]IgnoreFile value)
        {
            if (value != null && value.Id > 0 && (await UserHasPermissions(id)))
            {
                Project currentProject = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles);

                if (currentProject == null)
                {
                    return NotFound(new GenericResponse() { Success = false, Message = "Could not find project for current user" });
                }

                currentProject.IgnoreFiles[value.Id] = value;

                bool success = _projectService.Update(currentProject) && _projectService.Save();
                if (!success)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not save project" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = value });
        }

        [HttpPost("{id}/metricSeverities")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> AddMetricSeverity(int id, [FromBody]MetricSeverity value)
        {
            if (value != null && (await UserHasPermissions(id)))
            {
                Project currentProject = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

                if (currentProject == null)
                {
                    return NotFound(new GenericResponse() { Success = false, Message = "Could not find project for current user" });
                }

                currentProject.SeverityTypes.Add(value);

                bool success = _projectService.Update(currentProject) && _projectService.Save();
                if (!success)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not save project" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = value });
        }

        [HttpPut("{id}/metricSeverities")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> EditMetricSeverity(int id, [FromBody]MetricSeverity value)
        {
            if (value != null && value.Id > 0 && (await UserHasPermissions(id)))
            {
                Project currentProject = await _projectService.ReadById(p => p.Id == id, p => p.SeverityTypes);

                if (currentProject == null)
                {
                    return NotFound(new GenericResponse() { Success = false, Message = "Could not find project for current user" });
                }

                bool success = false;
                MetricSeverity metricSeverity = currentProject.SeverityTypes.FirstOrDefault(ms => ms.Id == value.Id);
                if (metricSeverity != null)
                {
                    metricSeverity.Value = value.Value;
                    success = _metricSeverityService.Update(metricSeverity) && _metricSeverityService.Save();
                }
                else
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not find severity" });
                }

                if (!success)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "could not save project" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = value });
        }

        private bool CanAddMetrics(int maxAllowed, int projectId, ref int metricCount) {
            if (maxAllowed != -1)
            {
                if (metricCount == 0)
                {
                    metricCount = _metricResultService.Queryable
                                                          .Include(mr => mr.ProjectMetric)
                                                          .ThenInclude(pm => pm.Project)
                                                          .Where(mr => mr.ProjectMetric.Project.Id == projectId)
                                                          .Count();
                }

                return metricCount < maxAllowed;
            }
            return true;
        }


        [HttpPost("{id}/runGitMetrics")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> RunMetrics(int id, [FromBody]GitRepoInfo gitRepoInfo)
        {
            ApplicationUser user = await UserWithPermissions(id);
            if (user == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }
            LicenseData license = UserLicenseInfo.GetLicenseData(user, _tokenSettings, _defaultSettings);

            int metricCount = 0;

			if (!CanAddMetrics(license.MaxFreeMetrics, id, ref metricCount))
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Max Allowed metrics met. Upgrade or delete this project." });
			}

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles, p => p.ProjectMetrics);
            List<ProjectMetrics> projectMetricRuns = new List<ProjectMetrics>();

            if (project != null)
            {
                project.SourceRepoUrl = gitRepoInfo.Url;
                if (project.ProjectMetrics == null)
                {
                    project.ProjectMetrics = new List<ProjectMetrics>();
                }

                Console.WriteLine("Attempting to use Git");
                string repoPath = _hostingEnv.ContentRootPath + $@"{Path.DirectorySeparatorChar}Git{Path.DirectorySeparatorChar}{id}{Path.DirectorySeparatorChar}";

                //Remove files from the server
                DirectoryHelper.RemoveDirectory(_hostingEnv, project.Id, repoPath);

                System.IO.Directory.CreateDirectory(repoPath);

                bool success = true;
                bool saveItems = true;

                try
                {
                    string gitFolder = GitHelper.RunGitProcess(_executionSettings, repoPath, gitRepoInfo.Url, gitRepoInfo.Password);

                    Console.WriteLine("Git Folder: " + gitFolder);

                    using (var repo = new LibGit2Sharp.Repository(repoPath + Path.DirectorySeparatorChar + gitFolder)) {
                        //TODO: Remove this when we support SSH
                        repo.Config.Set("http.sslVerify", false);

                        if (!string.IsNullOrEmpty(gitRepoInfo.SelectedBranch))
                        {
                            var branch = repo.Branches.FirstOrDefault(b => b.CanonicalName.EndsWith(gitRepoInfo.SelectedBranch, StringComparison.CurrentCulture));

                            if (branch != null)
                            {
                                Branch currentBranch = Commands.Checkout(repo, branch);

                                if (gitRepoInfo.GitCommits != null && gitRepoInfo.GitCommits.Count() > 0)
                                {
                                    IEnumerable<GitCommit> orderedGitCommits = gitRepoInfo.GitCommits.OrderBy(gc => gc.Date);
                                    foreach (GitCommit gitCommit in orderedGitCommits)
                                    {
                                        try
                                        {
                                            var commit = branch.Commits.FirstOrDefault(c => c.Sha == gitCommit.Commit);
                                            if (commit != null)
                                            {
                                                currentBranch = Commands.Checkout(repo, commit);
                                                success = GitSetupMetrics(project, ref projectMetricRuns, repoPath);
                                                if (!success)
                                                {
                                                    break;
                                                }


                                                ProjectMetrics lastProjectMetricRun = projectMetricRuns.Last();

                                                lastProjectMetricRun.Name = gitCommit.Commit.Substring(0, 11);
                                                lastProjectMetricRun.Created = gitCommit.Date;

                                                project.ProjectMetrics.Add(projectMetricRuns.Last());

                                                metricCount += projectMetricRuns.Last().MetricResults.Count();

                                                if(!CanAddMetrics(license.MaxFreeMetrics, id, ref metricCount)){
                                                    return BadRequest(new GenericResponse() { Success = false, Message = "Max Allowed metrics met. Upgrade or delete this project." });
                                                }

                                                success = _projectService.Update(project) && _projectService.Save();

                                                if (!success)
                                                {
                                                    break;
                                                }

                                                saveItems = false;
                                            }
                                        }
                                        catch (Exception exp)
                                        {
                                            Console.WriteLine(exp.Message);
                                        }
                                    }
                                }
                                else
                                {
                                    success = GitSetupMetrics(project, ref projectMetricRuns, repoPath);
                                    projectMetricRuns.Last().Name = branch.Commits.FirstOrDefault().Sha.Substring(0, 11);
                                }
                            }
                        }
                        else
                        {
                            success = GitSetupMetrics(project, ref projectMetricRuns, repoPath);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error with Git: " + e.Message);
                    success = false;
                }
                //Remove files from the server
                DirectoryHelper.RemoveDirectory(_hostingEnv, project.Id, repoPath);

                if (success)
                {
                    if (saveItems)
                    {
                        foreach (ProjectMetrics pm in projectMetricRuns)
                        {
                            project.ProjectMetrics.Add(pm);
                        }
                        success = _projectService.Update(project) && _projectService.Save();
                    }

                    if (!success)
                    {
                        return BadRequest(new GenericResponse() { Success = false, Message = "Issue Saving Metrics" });
                    }
                }
                else
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Issue Running Metrics" });
                }
            }
            else
            {
                return NotFound(new GenericResponse() { Success = false, Message = "Could not find project" });
            }

            IEnumerable<ProjectMetricsDto> projectMetricsDto = Mapper.Map<IEnumerable<ProjectMetrics>, IEnumerable<ProjectMetricsDto>>(projectMetricRuns);

            return Ok(new GenericResponse() { Success = true, Message = projectMetricsDto });
        }


        [HttpPost("{id}/runMetrics")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> RunMetrics(int id)
        {

            List<IFormFile> files = Request.Form.Files.ToList();

            ApplicationUser user = await UserWithPermissions(id);

            if (user == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Cannot Find Project" });
            }

            LicenseData license = UserLicenseInfo.GetLicenseData(user, _tokenSettings, _defaultSettings);

            int metricCount = 0;

			if (!CanAddMetrics(license.MaxFreeMetrics, id, ref metricCount))
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Max Allowed metrics met. Upgrade or delete this project." });
			}

            Project project = await _projectService.ReadById(p => p.Id == id, p => p.IgnoreFiles, p => p.ProjectMetrics);

            ProjectMetrics projectMetricsRun = new ProjectMetrics();

            if (project != null)
            {

                //Remove files from the server
                DirectoryHelper.RemoveDirectory(_hostingEnv, project.Id);

                List<IFormFile> cSharpFiles = new List<IFormFile>();
                List<IFormFile> vbFiles = new List<IFormFile>();
                List<IFormFile> tsFiles = new List<IFormFile>();
                List<IFormFile> tsxFiles = new List<IFormFile>();
                List<IFormFile> jsFiles = new List<IFormFile>();
                List<IFormFile> jsxFiles = new List<IFormFile>();
                List<IFormFile> javaFiles = new List<IFormFile>();
                List<IFormFile> otherFiles = new List<IFormFile>();

                files.ForEach((f) =>
                {
                    var fileName = f.FileName.Trim('"');

                    if (fileName.EndsWith(".cs", StringComparison.CurrentCultureIgnoreCase))
                    {
                        cSharpFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".vb", StringComparison.CurrentCultureIgnoreCase))
                    {
                        vbFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".ts", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tsFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".tsx", StringComparison.CurrentCultureIgnoreCase))
                    {
                        tsxFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".js", StringComparison.CurrentCultureIgnoreCase))
                    {
                        jsFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".jsx", StringComparison.CurrentCultureIgnoreCase))
                    {
                        jsxFiles.Add(f);
                        metricCount += 4;
                    }
                    else if (fileName.EndsWith(".java", StringComparison.CurrentCultureIgnoreCase))
                    {
                        javaFiles.Add(f);
                        metricCount += 4;
                    }
                    else
                    {
                        otherFiles.Add(f);
                        metricCount += 1;
                    }

                    //What about cshtml?  Does it even work?
                });


                //Double check with the files being uploaded...
				if (!CanAddMetrics(license.MaxFreeMetrics, id, ref metricCount))
				{
					return BadRequest(new GenericResponse() { Success = false, Message = "Max Allowed metrics met. Upgrade or delete this project." });
				}

                bool hadException = false;
                string errorMsg = "";
                try
                {

                    if (project.ProjectMetrics == null)
                    {
                        project.ProjectMetrics = new List<ProjectMetrics>();
                    }

                    projectMetricsRun.MetricResults = new List<MetricResult>();

                    projectMetricsRun.Created = DateTime.UtcNow;


                    if (cSharpFiles.Count > 0)
                    {
                        RunMetrics(project, cSharpFiles, ref projectMetricsRun, SourceCodeType.CSharp);
                    }
                    if (vbFiles.Count > 0)
                    {
                        RunMetrics(project, vbFiles, ref projectMetricsRun, SourceCodeType.VisualBasic);
                    }
                    if (tsFiles.Count > 0)
                    {
                        RunMetrics(project, tsFiles, ref projectMetricsRun, SourceCodeType.TypeScript);
                    }
                    if (tsxFiles.Count > 0)
                    {
                        RunMetrics(project, tsxFiles, ref projectMetricsRun, SourceCodeType.TypeScriptReact);
                    }
                    if (jsFiles.Count > 0)
                    {
                        RunMetrics(project, jsFiles, ref projectMetricsRun, SourceCodeType.JavaScript);
                    }
                    if (jsxFiles.Count > 0)
                    {
                        RunMetrics(project, jsxFiles, ref projectMetricsRun, SourceCodeType.JavaScriptReact);
                    }

                    //For SLOC
                    RunMetrics(project, otherFiles, ref projectMetricsRun, SourceCodeType.Other);

                    project.ProjectMetrics.Add(projectMetricsRun);

                }
                catch (Exception exp)
                {
                    errorMsg = exp.Message + Environment.NewLine + exp.StackTrace;
                    hadException = true;
                }

                //Remove files from the server
                DirectoryHelper.RemoveDirectory(_hostingEnv, project.Id);


                if (hadException)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Issue Running Metrics" });
                }

                bool success = _projectService.Update(project) && _projectService.Save();

                if (!success)
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Issue Saving Metrics" });
                }
            }
            else
            {
                return NotFound(new GenericResponse() { Success = false, Message = "Could not find project" });
            }

            ProjectMetricsDto projectMetricsDto = Mapper.Map<ProjectMetrics, ProjectMetricsDto>(projectMetricsRun);

            return Ok(new GenericResponse() { Success = true, Message = projectMetricsDto });
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Delete(int id)
        {
            Project project = await _projectService.Queryable
                .Include(p => p.SeverityTypes)
                .Include(p => p.ProjectMetrics)
                .ThenInclude(pm => pm.MetricResults)
                .Include(p => p.IgnoreFiles)
                .Include(p => p.SharedWith)
                .FirstOrDefaultAsync(p => p.Id == id);
            if (project != null)
            {
                if (!(await UserOwnsProject(id)))
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Insufficient permissions." });
                }
                else
                {
                    bool success = _projectService.Delete(project) && _projectService.Save();
                    if (success) {
                        return Ok(new GenericResponse() { Success = true, Message = true });
                    }
                    else {
                        return StatusCode(StatusCodes.Status500InternalServerError, "Could not delete project.");
                    }
                }
            }
            else
            {
                return NotFound(new GenericResponse { Success = false, Message = "Could not find project." });
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}/metrics/{projectMetricId}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> DeleteMetricsRun(int id, int projectMetricId)
        {
            if (!(projectMetricId > 0)) {
                return BadRequest(new GenericResponse { Success = false, Message = "Invalid ID." });
            }

            if (!(await UserHasPermissions(id))) {
                return BadRequest(new GenericResponse { Success = false, Message = "Insufficient permissions." });
            }

            ProjectMetrics projectMetricRun = await _projectMetricService.ReadById(a => a.Id == projectMetricId, pm => pm.MetricResults);
            if (projectMetricRun != null) {
                bool success = _projectMetricService.Delete(projectMetricRun) && _projectMetricService.Save();
                if (success) {
                    return Ok(new GenericResponse { Success = true, Message = true });
                }
                else {
                    return StatusCode(StatusCodes.Status500InternalServerError, new GenericResponse { Success = false, Message = false });
                }
            }
            else {
                return NotFound(new GenericResponse { Success = false, Message = "Could not find project metric run." });
            }
        }

        [HttpGet("{id}/sharedWith")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetSharedWith(int id) {
            Project project = await _projectService.Queryable
                .Include(p => p.SharedWith)
                .ThenInclude(sp => sp.SharedWith)
                .FirstOrDefaultAsync(p => p.Id == id);
            
            if (project == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Could not find project." });
            }
            
            if (!(await UserHasPermissions(id))) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Insufficient permissions." });
            }
            
            List<ApplicationUser> sharedUsers = project.SharedWith.Select(sp => sp.SharedWith).ToList();
            List<ApplicationUserDto> message = Mapper.Map<List<ApplicationUser>, List<ApplicationUserDto>>(sharedUsers);

            return Ok(new GenericResponse() { Success = true, Message = message });
        }

        private bool GitSetupMetrics(Project project, ref List<ProjectMetrics> projectMetricsRuns, string repoPath)
        {

            List<string> otherFiles = Directory.EnumerateFiles(repoPath, "*.*", SearchOption.AllDirectories).Where(f => Path.GetExtension(f) != ".cs" &&
                                                                                                                      Path.GetExtension(f) != ".vb" &&
                                                                                                                      Path.GetExtension(f) != ".ts" &&
                                                                                                                      Path.GetExtension(f) != ".tsx" &&
                                                                                                                      Path.GetExtension(f) != ".js" &&
                                                                                                                      Path.GetExtension(f) != ".jsx" &&
                                                                                                                      Path.GetExtension(f) != ".java").ToList();
            
            List<string> cSharpFiles = Directory.EnumerateFiles(repoPath, "*.cs", SearchOption.AllDirectories).ToList();
            List<string> vbFiles = Directory.EnumerateFiles(repoPath, "*.vb", SearchOption.AllDirectories).ToList();
            List<string> tsFiles = Directory.EnumerateFiles(repoPath, "*.ts", SearchOption.AllDirectories).ToList();
            List<string> tsxFiles = Directory.EnumerateFiles(repoPath, "*.tsx", SearchOption.AllDirectories).ToList();
            List<string> jsFiles = Directory.EnumerateFiles(repoPath, "*.js", SearchOption.AllDirectories).ToList();
            List<string> jsxFiles = Directory.EnumerateFiles(repoPath, "*.jsx", SearchOption.AllDirectories).ToList();
            List<string> javaFiles = Directory.EnumerateFiles(repoPath, "*.java", SearchOption.AllDirectories).ToList();

            bool hadNoException = true;

            string errorMsg = "";
            try
            {

                ProjectMetrics projectMetricsRun = new ProjectMetrics();

                projectMetricsRun.Created = DateTime.UtcNow;


                if (cSharpFiles.Count > 0)
                {
                    RunMetrics(project, cSharpFiles, ref projectMetricsRun, SourceCodeType.CSharp, repoPath);
                }
                if (vbFiles.Count > 0)
                {
                    RunMetrics(project, vbFiles, ref projectMetricsRun, SourceCodeType.VisualBasic, repoPath);
                }
                if (tsFiles.Count > 0)
                {
                    RunMetrics(project, tsFiles, ref projectMetricsRun, SourceCodeType.TypeScript, repoPath);
                }
                if (tsxFiles.Count > 0)
                {
                    RunMetrics(project, tsxFiles, ref projectMetricsRun, SourceCodeType.TypeScriptReact, repoPath);
                }
                if (jsFiles.Count > 0)
                {
                    RunMetrics(project, jsFiles, ref projectMetricsRun, SourceCodeType.JavaScript, repoPath);
                }
                if (jsxFiles.Count > 0)
                {
                    RunMetrics(project, jsxFiles, ref projectMetricsRun, SourceCodeType.JavaScriptReact, repoPath);
                }
                if(javaFiles.Count > 0) {
                    RunMetrics(project, javaFiles, ref projectMetricsRun, SourceCodeType.Java, repoPath);
                }

                //Run for SLOC
                RunMetrics(project, otherFiles, ref projectMetricsRun, SourceCodeType.Other, repoPath);

                projectMetricsRuns.Add(projectMetricsRun);

            }
            catch (Exception exp)
            {
                errorMsg = exp.Message + Environment.NewLine + exp.StackTrace;
                hadNoException = false;
            }

            return hadNoException;

        }

        private void ExecuteMetrics(List<string> files,
                                    ref ProjectMetrics projectMetricsRun,
                                    SourceCodeType sourceCodeType,
                                    string uploadPath,
                                    List<string> ignoreFiles = null)
        {
            List<MetricResult> results = new List<MetricResult>();

            switch (sourceCodeType)
            {
                case SourceCodeType.CSharp:
                    {
                        CSharpAst csAst = new CSharpAst(files);

                        SDK.Metrics.CSharp.Wmc wmc = new SDK.Metrics.CSharp.Wmc();
                        SDK.Metrics.CSharp.Dcc dcc = new SDK.Metrics.CSharp.Dcc();
                        SDK.Metrics.CSharp.Lcom lcomMetric = new SDK.Metrics.CSharp.Lcom();

                        results.AddRange(wmc.WmcMetric(csAst.ASTRoot, files, uploadPath));

                        results.AddRange(dcc.DccMetric(csAst.ASTRoot, files, uploadPath));

                        results.AddRange(lcomMetric.LcomMetric(csAst.ASTRoot, files, uploadPath));
                    }
                    break;
                case SourceCodeType.VisualBasic:
                    {
                        VBAst vbAst = new VBAst(files);

                        SDK.Metrics.Vb.Wmc wmc = new SDK.Metrics.Vb.Wmc();
                        SDK.Metrics.Vb.Dcc dcc = new SDK.Metrics.Vb.Dcc();
                        SDK.Metrics.Vb.Lcom lcomMetric = new SDK.Metrics.Vb.Lcom();

                        results.AddRange(wmc.WmcMetric(vbAst.ASTRoot, files, uploadPath));

                        results.AddRange(dcc.DccMetric(vbAst.ASTRoot, files, uploadPath));

                        results.AddRange(lcomMetric.LcomMetric(vbAst.ASTRoot, files, uploadPath));
                    }
                    break;
                case SourceCodeType.Java:
                    {
                        results = RunJavaProcess(uploadPath);
                    }
                    break;
                case SourceCodeType.Other:
                case SourceCodeType.TypeScript:
                case SourceCodeType.JavaScript:
                case SourceCodeType.TypeScriptReact:
                case SourceCodeType.JavaScriptReact:
                    {
                        results = RunNodeProcess(uploadPath, sourceCodeType, ignoreFiles);
                    }
                    break;
            }

            if (projectMetricsRun.MetricResults == null)
            {
                projectMetricsRun.MetricResults = new List<MetricResult>();
            }

            foreach (MetricResult result in results)
            {
                projectMetricsRun.MetricResults.Add(result);
            }

        }

        private void RunMetrics(Project activeProject, List<string> files, ref ProjectMetrics projectMetricsRun, SourceCodeType sourceCodeType, string repoPath)
        {
            List<string> filePaths = new List<string>();
            List<string> ignoreFiles = new List<string>();

            foreach (string file in files)
            {

                if (AllowFile(activeProject, file))
                {
                    filePaths.Add(file);
                }
                else
                {
                    ignoreFiles.Add(file);
                }
            }

            ExecuteMetrics(filePaths, ref projectMetricsRun, sourceCodeType, repoPath, ignoreFiles);
        }

        private void RunMetrics(Project activeProject, List<IFormFile> files, ref ProjectMetrics projectMetricsRun, SourceCodeType sourceCodeType)
        {
            List<string> filePaths = new List<string>();

            string uploadPath = $@"{_hostingEnv.ContentRootPath}{Path.DirectorySeparatorChar}Uploads{Path.DirectorySeparatorChar}{activeProject.Id}{Path.DirectorySeparatorChar}{sourceCodeType.ToString()}{Path.DirectorySeparatorChar}";

            //Temporary hold files
            System.IO.Directory.CreateDirectory(uploadPath);

            foreach (IFormFile file in files)
            {

                string filePath = $@"{uploadPath}" + file.FileName.Replace(Path.DirectorySeparatorChar + file.Name, "") + Path.DirectorySeparatorChar;

                System.IO.Directory.CreateDirectory(filePath);


                string fileName = file.Name.Trim('"');

                string ext = Path.GetExtension(fileName);

                fileName = $@"{filePath}{fileName}";

                //Filter files
                if (AllowFile(activeProject, file.FileName))
                {

                    using (FileStream fs = System.IO.File.Create(fileName))
                    {
                        file.CopyTo(fs);
                        fs.Flush();
                        filePaths.Add(fileName);
                    }

                    System.IO.File.SetAttributes(fileName, FileAttributes.ReadOnly);
                }
            }

            ExecuteMetrics(filePaths, ref projectMetricsRun, sourceCodeType, uploadPath);

        }

        private bool AllowFile(Project activeProject, string fileName)
        {

            return activeProject.IgnoreFiles == null ||
                    activeProject.IgnoreFiles.Count == 0 ||
                    !activeProject.IgnoreFiles.Any(i =>
                    {
                        // *.ts
                        // path/*.ts
                        if (i.Pattern.Contains("*"))
                        {
                            int starIndex = i.Pattern.IndexOf("*");
                            // path/*.ts
                            if (i.Pattern.Contains(Path.DirectorySeparatorChar))
                            {
                                string left = i.Pattern.Substring(0, starIndex);
                                string right = i.Pattern.Substring(starIndex);
                                if (fileName.StartsWith(left) && fileName.EndsWith(right))
                                {
                                    return true;
                                }
                            }
                            // *.ts
                            else
                            {
                                if (fileName.EndsWith(i.Pattern.Substring(starIndex)))
                                {
                                    return true;
                                }
                            }
                        }
                        // path/path
                        // path/path/file.ts
                        else if (i.Pattern.Contains(Path.DirectorySeparatorChar))
                        {
                            // path/path/file.ts
                            if (i.Pattern.Contains("."))
                            {
                                return fileName == i.Pattern;
                            }
                            // path/path
                            else
                            {
                                return fileName.StartsWith(i.Pattern);
                            }
                        }
                        // node_modules
                        // file.ts
                        else
                        {
                            // file.ts
                            if (i.Pattern.Contains("."))
                            {
                                return fileName.EndsWith(i.Pattern);
                            }
                            // node_modules
                            else
                            {
                                return fileName.Contains(i.Pattern);
                            }
                        }
                        return false;
                    });
        }

        private List<MetricResult> RunNodeProcess(string uploadPath, SourceCodeType sourceCodeType, List<string> ignoreFiles = null)
        {
            List<MetricResult> results = new List<MetricResult>();
            string arg1 = $@"{uploadPath}";

            ProcessStartInfo pInfo = new ProcessStartInfo();
            pInfo.FileName = _executionSettings.NodePath;
            pInfo.Arguments = $@"{_executionSettings.TsMetricsPath} {arg1} {sourceCodeType.ToString()}";
            if (ignoreFiles != null)
            {
                pInfo.Arguments += " " + string.Join(",", ignoreFiles);
            }
            pInfo.RedirectStandardOutput = true;
            pInfo.UseShellExecute = false;
            pInfo.CreateNoWindow = true;

            Process process = Process.Start(pInfo);

            string line = process.StandardOutput.ReadToEnd();
            try
            {
                if (!string.IsNullOrEmpty(line))
                {
                    results = JsonConvert.DeserializeObject<List<MetricResult>>(line);
                }
                else
                {
                    Console.WriteLine("Node returned blank result!");
                }
            }
            catch
            {
                //Do nothing here...
            }
            process.WaitForExit();

            return results;
        }

        private List<MetricResult> RunJavaProcess(string uploadPath) {
			List<MetricResult> results = new List<MetricResult>();

			ProcessStartInfo pInfo = new ProcessStartInfo();
            pInfo.FileName = _executionSettings.JavaPath;
            pInfo.Arguments = $@" -jar {_executionSettings.JavaMetricsPath} {uploadPath}";
			pInfo.RedirectStandardOutput = true;
			pInfo.UseShellExecute = false;
			pInfo.CreateNoWindow = true;

			Process process = Process.Start(pInfo);

			string line = process.StandardOutput.ReadToEnd();
			try
			{
				if (!string.IsNullOrEmpty(line))
				{
					results = JsonConvert.DeserializeObject<List<MetricResult>>(line);
				}
				else
				{
					Console.WriteLine("Java returned blank result!");
				}
			}
			catch
			{
				//Do nothing here...
			}
			process.WaitForExit();

			return results;
        }

        private async Task<bool> UserHasPermissions(int projectId) {
            return (await UserWithPermissions(projectId)) != null;
        }

        private async Task<bool> UserOwnsProject(int projectId) {
            return (await _userService.Queryable
                .FirstOrDefaultAsync(u => u.Id == CurrentUserId && (
                    u.Projects.Any(p => p.Id == projectId)
                ))) != null;
        }

        private async Task<ApplicationUser> UserWithPermissions(int projectId)
		{
			return (await _userService.Queryable
                    .Include(u => u.License)
				    .FirstOrDefaultAsync(u => u.Id == CurrentUserId && (
					u.Projects.Any(p => p.Id == projectId) ||
					u.SharedProjects.Any(sp => sp.Project.Id == projectId)
				)));
		}

    }
}