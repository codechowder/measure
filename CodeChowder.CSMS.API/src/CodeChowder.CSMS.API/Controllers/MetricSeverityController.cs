﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CodeChowder.CSMS.API.ResponseModels;
using CodeChowder.CSMS.API.Security;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/metricSeverities")]
    public class MetricSeverityController : BaseController
    {
        private readonly IMetricSeverityService _metricSeverityService;
		private readonly IProjectService _projectService;
		private readonly IHostingEnvironment _hostingEnv;
		private readonly IApplicationUserService _userService;

        public MetricSeverityController(IMetricSeverityService applicationUserService,
									   IProjectService projectService,
                                       IApplicationUserService userService): base()
        {
            _metricSeverityService = applicationUserService;
			_projectService = projectService;
			_userService = userService;
        }

		// DELETE api/values/5
		[HttpDelete("{id}")]
		[ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Delete(int id)
		{
			bool success = false;

            MetricSeverity metricSeverity = await _metricSeverityService.ReadById(a => a.Id == id);
			if (metricSeverity != null)
			{
				success = _metricSeverityService.Delete(metricSeverity) && _metricSeverityService.Save();
				if (success)
				{
                    success = true;
				}
			}
			else
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Issue attempting to delete" });
			}

            return Ok(new GenericResponse() { Success = true, Message = success });

        }
    }
}
