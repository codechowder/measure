﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using CodeChowder.CSMS.API.Helpers;
using CodeChowder.CSMS.API.Models;
using CodeChowder.CSMS.API.ResponseModels;
using CodeChowder.CSMS.API.Security;
using CodeChowder.CSMS.API.Settings;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/git")]
    public class GitController : BaseController
    {
        private readonly ExecutionSettings _executionSettings;
        private readonly IHostingEnvironment _hostingEnv;

		public GitController(IHostingEnvironment hostingEnv,
								  IOptions<ExecutionSettings> executionSettings) : base() {
            _executionSettings = executionSettings.Value;
            _hostingEnv = hostingEnv;
        }

        [HttpPost]
        [ClaimRequirement("UserId", "{UserId}")]
        public IActionResult Post([FromBody]GitRepoInfo gitRepoInfo)
        {
            if (gitRepoInfo == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
			}

            string repoPath = $"{_hostingEnv.ContentRootPath}{Path.DirectorySeparatorChar}{Guid.NewGuid().ToString()}"; 

            Directory.CreateDirectory(repoPath);
            IList<GitCommit> gitCommits = new List<GitCommit>();

            try
            {

                string gitFolder = GitHelper.RunGitProcess(_executionSettings, repoPath, gitRepoInfo.Url, gitRepoInfo.Password);

                Console.WriteLine("Git Folder: " + gitFolder);

                using (var repo = new LibGit2Sharp.Repository(repoPath + Path.DirectorySeparatorChar + gitFolder)) {
                    //TODO: Remove this when we support SSH
                    repo.Config.Set("http.sslVerify", false);

                    if (!string.IsNullOrEmpty(gitRepoInfo.SelectedBranch))
                    {
                        var branch = repo.Branches.FirstOrDefault(b => b.CanonicalName.EndsWith(gitRepoInfo.SelectedBranch, StringComparison.CurrentCulture));

                        foreach (var commit in branch.Commits)
                        {
                            GitCommit gitCommit = new GitCommit();
                            gitCommit.Commit = commit.Sha;
                            gitCommit.Message = commit.MessageShort;
                            gitCommit.Date = commit.Committer.When.DateTime;

                            gitCommits.Add(gitCommit);
                        }
                    }
                }
            }
            catch (Exception e) {
                return StatusCode(500, new GenericResponse() { Success = false, Message = "Issue fetching Git info." });
            }

            DirectoryHelper.RemoveDirectory(_hostingEnv, 0, repoPath);


            return Ok(new GenericResponse() { Success = true, Message = gitCommits });
        }
    }
}
