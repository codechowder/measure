﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using CodeChowder.CSMS.API.Helpers;
using CodeChowder.CSMS.API.Models;
using CodeChowder.CSMS.API.ResponseModels;
using CodeChowder.CSMS.API.Security;
using CodeChowder.CSMS.API.Settings;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/licenses")]
    public class LicenseKeyController : BaseController
    {
        
        private const string ADMIN_ROLE = "Admin";
        private readonly ILicenseKeyService _licenseKeyService;
        private readonly IApplicationUserService _userService;
        private readonly TokenSettings _tokenSettings;

        public LicenseKeyController(ILicenseKeyService licenseKeyService,
                                    IApplicationUserService userService,
                                    IOptions<TokenSettings> tokenSettings) : base() {
            _licenseKeyService = licenseKeyService;
            _userService = userService;
            _tokenSettings = tokenSettings.Value;
        }

        // GET: api/values
        [HttpGet]
        [ClaimRequirement("Role", ADMIN_ROLE)]
        public async Task<IActionResult> Get()
        {
            return Ok(new GenericResponse() { Success = true, Message = _licenseKeyService.ReadMany(l => l.Id > 0) });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [ClaimRequirement("Role", ADMIN_ROLE)]
        public async Task<IActionResult> Get(int id)
        {
            if(id <= 0) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }
            return Ok(new GenericResponse() { Success = true, Message = await _licenseKeyService.ReadById(l => l.Id == id)});
        }

        // POST api/values
        [HttpPost]
        [ClaimRequirement("Role", ADMIN_ROLE)]
        public async Task<IActionResult> Post([FromBody]LicenseData value)
        {
            if(value == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }
            string license = JsonConvert.SerializeObject(value);

            string encrypted = Crypto.EncryptString(license, _tokenSettings.SecretKey);

            LicenseKey licenseKey = new LicenseKey();

            licenseKey.License = encrypted;

            bool success = await _licenseKeyService.Create(licenseKey) && _licenseKeyService.Save();

            if(!success) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Could not save License" });
            }
            return Ok(new GenericResponse() { Success = true, Message = licenseKey });
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [ClaimRequirement("Role", ADMIN_ROLE)]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ClaimRequirement("Role", ADMIN_ROLE)]
        public async Task<IActionResult> Delete(int id)
        {
            if(id <= 0) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            LicenseKey licenseKey = await _licenseKeyService.ReadById(l => l.Id == id);
            if(licenseKey == null) {
                return NotFound(new GenericResponse() { Success = false, Message = "Could not find license key" });
            }

            bool success = _licenseKeyService.Delete(licenseKey) && _licenseKeyService.Save();

            if(!success) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Could not delete license key"});
            }

            return Ok(new GenericResponse() { Success = true, Message = true });

        }

		
    }
}
