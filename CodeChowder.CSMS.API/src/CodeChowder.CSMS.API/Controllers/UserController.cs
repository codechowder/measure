﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DAL.Services;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using System.Net.Http;
using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using CodeChowder.CSMS.API.ResponseModels;
using CodeChowder.CSMS.API.Security;
using Microsoft.AspNetCore.Cors;
using AutoMapper;
using CodeChowder.CSMS.API.DTOs;
using CodeChowder.CSMS.API.Settings;
using Microsoft.Extensions.Options;
using CodeChowder.CSMS.DataStructures.Enumerations;
using Microsoft.EntityFrameworkCore;
using CodeChowder.CSMS.API.Helpers;
using CodeChowder.CSMS.API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/users")]
    public class UserController : BaseController
    {

        private readonly IApplicationUserService _applicationUserService;
        private readonly DefaultSettings _defaultSettings;
        private readonly TokenSettings _tokenSettings;
        private readonly ISharedProjectService _sharedProjectService;

        public UserController(IApplicationUserService applicationUserService,
                              IOptions<DefaultSettings> defaultSettings,
                              IOptions<TokenSettings> tokenSettings,
                              ISharedProjectService sharedProjectService)
        {
            _applicationUserService = applicationUserService;
            _defaultSettings = defaultSettings.Value;
            _tokenSettings = tokenSettings.Value;
            _sharedProjectService = sharedProjectService;
        }
        // GET: api/values
        [HttpGet("{skip}/{limit}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Get(int skip, int limit)
        {
            int count = await _applicationUserService.Count();
            IEnumerable<ApplicationUser> applicationUsers = _applicationUserService.ReadPaging(skip, limit);

            IEnumerable<ApplicationUserDto> users = Mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<ApplicationUserDto>>(applicationUsers);

            return Ok(new GenericResponse() { Success = true, Count = count, Message = users });
        }

        // GET api/values/5
        [HttpGet("{email}/user")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetByEmail(string email)
        {
            ApplicationUser appUser = await _applicationUserService.ReadById(a => a.Email == email);

            ApplicationUserDto user = Mapper.Map<ApplicationUser, ApplicationUserDto>(appUser);
            return Ok(new GenericResponse() { Success = true, Message = user });
        }

        [HttpGet("{id}/licenseData")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetLicenseData(int id) {
            ApplicationUser user = await _applicationUserService.ReadById(a => a.Id == id, a => a.License);

			if (user == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
			}

            LicenseData license = UserLicenseInfo.GetLicenseData(user, _tokenSettings, _defaultSettings);

            return Ok(new GenericResponse() { Success = true, Message = license });

        }

        // GET api/values/5
        [HttpGet("{id}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Get(int id)
        {
            ApplicationUser appUser = await _applicationUserService.Queryable
                                                                   .Include(a => a.Projects)
                                                                   .FirstOrDefaultAsync(a => a.Id == id);

            if (appUser == null)
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            ApplicationUserDto user = Mapper.Map<ApplicationUser, ApplicationUserDto>(appUser);
            return Ok(new GenericResponse() { Success = true, Message = user });
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ApplicationUser value)
        {
            ApplicationUserDto user = null;
            if (value != null && !string.IsNullOrEmpty(value.Email) && !string.IsNullOrEmpty(value.Password))
            {
                byte[] data = System.Text.Encoding.ASCII.GetBytes(value.Password);
                using (var sha512 = System.Security.Cryptography.SHA512.Create())
                {
                    data = sha512.ComputeHash(data);
                    string hash = System.Text.Encoding.ASCII.GetString(data);
                    value.Password = hash;
                }

                value.AllowedProjects = _defaultSettings.AllowedProjects;
                value.Account = (AccountType)_defaultSettings.DefaultAccount;

                bool success = await _applicationUserService.Create(value) && _applicationUserService.Save();
                if (success)
                {
                    user = Mapper.Map<ApplicationUser, ApplicationUserDto>(value);
                }
                else
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Unable to save.  Check credentials or email may be already registered." });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = user });
        }

        [HttpPut("{id}/license")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> ChangeLicense(int id, [FromBody]LicenseKey value) {

            if(id <= 0) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }
			ApplicationUser user = await _applicationUserService.ReadById(a => a.Id == id, a => a.License);
			if (user == null)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
			}

			if (value.License != null && !string.IsNullOrEmpty(value.License))
			{
				if (user.License == null || user.License.License != value.License)
				{
					LicenseStatus status = UserLicenseInfo.CanUseLicense(value.License, _applicationUserService, _tokenSettings);
					if (status != LicenseStatus.Valid)
					{
						return BadRequest(new GenericResponse() { Success = false, Message = $"Issue with updating the license: {status}" });
					}

                    user.License = new LicenseKey();
                    user.License.License = value.License;
				}
			}

			bool success = _applicationUserService.Update(user) && _applicationUserService.Save();
			if (!success)
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Issue updating User" });
			}

            return Ok(new GenericResponse() { Success = true, Message = "Successfully associated license" });
        }
        // PUT api/values/5
        [HttpPut]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Put([FromBody]ApplicationUserDto value)
        {
            ApplicationUser user = null;
            if (value != null && value.Id > 0)
            {
                user = await _applicationUserService.ReadById(a => a.Id == value.Id, a => a.License);
				if (user == null)
				{
					return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
				}

                Mapper.Map(value, user);

                bool success = _applicationUserService.Update(user) && _applicationUserService.Save();
                if(!success) {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Issue updating User" });
                }

            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = user });
        }

        [HttpPut("{id}/project")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> CreateProject(int id, [FromBody]Project project)
        {
            ApplicationUser user = await _applicationUserService.ReadById(a => a.Id == id, a => a.License, a => a.Projects);

            if (user == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
            }

            if (project != null)
            {
                LicenseData license = UserLicenseInfo.GetLicenseData(user, _tokenSettings, _defaultSettings);
                if (user.Projects.Count < license.AllowedProjects)
                {
                    /*Default Severity
					 * Complexity: Values < 20 are good, values >= 20 and < 40 are cautious, values >= 40 are concerning.
					 * Cohesion: Values < 1000 are good, values >= 1000 and < 2000 are cautious, values >= 2000 are concerning.
					 * Coupling: Values < 10 are good, values >= 10 and < 20 are cautious, values >= 20 are concerning.
                     */

                    project.SeverityTypes = new List<MetricSeverity>();

                    foreach(SourceCodeType sourceCodeType in Enum.GetValues(typeof(SourceCodeType))) {
                        project.SeverityTypes.Add(GetMetricSeverity("WMC", MetricSeverityType.Acceptable, sourceCodeType, 20));
						project.SeverityTypes.Add(GetMetricSeverity("WMC", MetricSeverityType.Cautious, sourceCodeType, 40));
						project.SeverityTypes.Add(GetMetricSeverity("WMC", MetricSeverityType.Severe, sourceCodeType, 40));

						project.SeverityTypes.Add(GetMetricSeverity("LCOM", MetricSeverityType.Acceptable, sourceCodeType, 1000));
						project.SeverityTypes.Add(GetMetricSeverity("LCOM", MetricSeverityType.Cautious, sourceCodeType, 2000));
						project.SeverityTypes.Add(GetMetricSeverity("LCOM", MetricSeverityType.Severe, sourceCodeType, 2000));

						project.SeverityTypes.Add(GetMetricSeverity("DCC", MetricSeverityType.Acceptable, sourceCodeType, 10));
						project.SeverityTypes.Add(GetMetricSeverity("DCC", MetricSeverityType.Cautious, sourceCodeType, 20));
						project.SeverityTypes.Add(GetMetricSeverity("DCC", MetricSeverityType.Severe, sourceCodeType, 20));

						project.SeverityTypes.Add(GetMetricSeverity("SLOC", MetricSeverityType.Acceptable, sourceCodeType, 1000));
						project.SeverityTypes.Add(GetMetricSeverity("SLOC", MetricSeverityType.Cautious, sourceCodeType, 4000));
						project.SeverityTypes.Add(GetMetricSeverity("SLOC", MetricSeverityType.Severe, sourceCodeType, 4000));
                        
                    }



					user.Projects.Add(project);
                    bool success = _applicationUserService.Update(user) && _applicationUserService.Save();
                }
                else
                {
                    return BadRequest(new GenericResponse() { Success = false, Message = "Need upgraded plan" });
                }
            }
            else
            {
                return BadRequest(new GenericResponse() { Success = false, Message = "Invalid Parameters" });
            }

            return Ok(new GenericResponse() { Success = true, Message = Mapper.Map<ApplicationUser, ApplicationUserDto>(user) });
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Delete(int id)
        {
            bool success = false;
            ApplicationUser user = await _applicationUserService.ReadById(a => a.Id == id, a => a.Projects);
            if(user != null)
            {
                success = _applicationUserService.Delete(user) && _applicationUserService.Save();
				if (success)
				{
                    success = true;
				}
			}
			else
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Issue attempting to delete" });
			}

            return Ok(new GenericResponse() { Success = true, Message = success });
        }

        [HttpPost("{sharedUserId}/sharedProjects")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> AddSharedProject(int sharedUserId, [FromBody] Project sharedProject) {
            ApplicationUser user = await _applicationUserService
                .Queryable
                .Include(u => u.License)
                .Include(u => u.Projects)
                .FirstOrDefaultAsync(u => u.Id == CurrentUserId);
            
            if (user == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
            }
            
            Project project = user.Projects.FirstOrDefault(p => p.Id == sharedProject.Id);

            if (project == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Project not found." });
            }

            LicenseData license = UserLicenseInfo.GetLicenseData(user, _tokenSettings, _defaultSettings);

            if(!license.AllowShare) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Current License does not allow projects to be shared." });
            }

            ApplicationUser sharedUser = await _applicationUserService
                .Queryable
                .Include(u => u.SharedProjects)
                .ThenInclude(sp => sp.Project)
                .FirstOrDefaultAsync(u => u.Id == sharedUserId);

            if (sharedUser == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Shared user not found." });
            }

            if (sharedUser.SharedProjects.Any(sp => sp.Project.Id == project.Id)) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Project already shared with that user." });
            }

            SharedProject shared = new SharedProject {
                Permissions = PermissionsType.Write,
                Project = project,
                SharedWith = sharedUser
            };

            sharedUser.SharedProjects.Add(shared);
            bool success = _applicationUserService.Update(sharedUser) && _applicationUserService.Save();
            if (success) {
                return Ok(new GenericResponse() { Success = true, Message = true });
            }
            else {
                return StatusCode(StatusCodes.Status500InternalServerError, new GenericResponse() { Success = false, Message = "Could not add shared project." });
            }
        }

        [HttpGet("{id}/sharedProjects")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> GetSharedProject(int id) {
            ApplicationUser user = await _applicationUserService
                .Queryable
                .Include(u => u.SharedProjects)
                .ThenInclude(sp => sp.Project)
                .FirstOrDefaultAsync(u => u.Id == CurrentUserId);
            
            if (user == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "User not found." });
            }

            List<SharedProjectDto> sharedProjects = user.SharedProjects.Select(sp => new SharedProjectDto {
                Id = sp.Id,
                Permissions = sp.Permissions,
                Project = Mapper.Map<Project, ProjectDto>(sp.Project)
            }).ToList();
            return Ok(new GenericResponse() { Success = true, Message = sharedProjects });
        }

        [HttpDelete("{sharedUserId}/sharedProjects/{sharedProjectId}")]
        [ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> RemoveSharedProject(int sharedUserId, int sharedProjectId) {
            ApplicationUser sharedUser = await _applicationUserService
                .Queryable
                .Include(u => u.SharedProjects)
                .ThenInclude(sp => sp.Project)
                .FirstOrDefaultAsync(u => u.Id == sharedUserId);

            if (sharedUser == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Shared user not found." });
            }

            SharedProject shared = sharedUser.SharedProjects.FirstOrDefault(sp => sp.Project.Id == sharedProjectId);
            if (shared == null) {
                return BadRequest(new GenericResponse() { Success = false, Message = "Project is not shared with that user." });
            }

            bool ownerRequestsRemoval = (await _applicationUserService
                .Queryable
                .FirstOrDefaultAsync(u => u.Id == CurrentUserId && u.Projects.Any(p => p.Id == sharedProjectId))) != null;
            if (!ownerRequestsRemoval) {
                return BadRequest(new GenericResponse() { Success = false, Message = "You do not have permission to unshare that project." });
            }

            bool success = _sharedProjectService.Delete(shared) && _sharedProjectService.Save();
            if (success) {
                return Ok(new GenericResponse() { Success = true, Message = true });
            }
            else {
                return StatusCode(StatusCodes.Status500InternalServerError, new GenericResponse() { Success = false, Message = "Could not add shared project." });
            }
        }

		private MetricSeverity GetMetricSeverity(string name, MetricSeverityType metricSeverityType, SourceCodeType sourceType, double value)
		{
			MetricSeverity severity = new MetricSeverity();
			severity.MetricName = name;
			severity.SeverityType = metricSeverityType;
			severity.Value = value;
			severity.SourceCode = sourceType;

			return severity;
		}
    }
}
