﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CodeChowder.CSMS.API.Security;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.API.ResponseModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.API.Controllers
{
    [Route("api/ignoreFiles")]
    public class IgnoreFilesController : BaseController
    {

		private readonly IProjectService _projectService;
        private readonly IIgnoreFileService _ignoreFileSerivce;
		private readonly IApplicationUserService _userService;

		public IgnoreFilesController(IProjectService projectService,
                                     IIgnoreFileService ignoreFileService,
                                     IApplicationUserService userService) : base()
        {
			_projectService = projectService;
            _ignoreFileSerivce = ignoreFileService;
			_userService = userService;
		}

        // DELETE api/values/5
        [HttpDelete("{id}")]
		[ClaimRequirement("UserId", "{UserId}")]
        public async Task<IActionResult> Delete(int id)
		{
			bool success = false;

            IgnoreFile ignoreFile = await _ignoreFileSerivce.ReadById(a => a.Id == id);
			if (ignoreFile != null)
			{
                success = _ignoreFileSerivce.Delete(ignoreFile) && _ignoreFileSerivce.Save();
				if (success)
				{
                    success = true;
				}
			}
			else
			{
				return BadRequest(new GenericResponse() { Success = false, Message = "Issue attempting to delete" });
			}

            return Ok(new GenericResponse() { Success = true, Message = success });
		}


    }
}
