﻿using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace CodeChowder.CSMS.API.Helpers
{
    public static class DirectoryHelper
    {
		/// <summary>
		/// Removes the files from the uploads directroy for the given project
		/// </summary>
        public static void RemoveDirectory(IHostingEnvironment hostingEnv, int id, string repopath = null)
		{
			if (!string.IsNullOrEmpty(repopath))
			{
				if (Directory.Exists(repopath))
				{
					var directory = new DirectoryInfo(repopath) { Attributes = FileAttributes.Normal };

					foreach (var info in directory.GetFileSystemInfos("*", SearchOption.AllDirectories))
					{
						info.Attributes = FileAttributes.Normal;
					}

					directory.Delete(true);
				}
			}
			else
			{
				string directoryPath = $@"{hostingEnv.ContentRootPath}{Path.DirectorySeparatorChar}Uploads{Path.DirectorySeparatorChar}{id}{Path.DirectorySeparatorChar}";
				if (Directory.Exists(directoryPath))
				{
					var directory = new DirectoryInfo(directoryPath) { Attributes = FileAttributes.Normal };

					foreach (var info in directory.GetFileSystemInfos("*", SearchOption.AllDirectories))
					{
						info.Attributes = FileAttributes.Normal;
					}

					directory.Delete(true);
				}
			}
		}
    }
}
