﻿using System;
using System.Diagnostics;
using CodeChowder.CSMS.API.Settings;

namespace CodeChowder.CSMS.API.Helpers
{
    public static class GitHelper
    {
        public static string RunGitProcess(ExecutionSettings executionSettings, string path, string gitUrl, string password)
		{
			string folder = string.Empty;
			//Setup git clone with password
			int indexOfAt = gitUrl.IndexOf('@');
			password = Uri.EscapeDataString(password);
			string cloneArg = $"{gitUrl.Substring(0, indexOfAt)}:{password}{gitUrl.Substring(indexOfAt)}";
			ProcessStartInfo pInfo = new ProcessStartInfo();
			pInfo.FileName = executionSettings.TerminalPath;

			string ext = System.IO.Path.GetExtension(executionSettings.TerminalPath);
			string arg = $"cd { path } && git config --global http.sslVerify false && " +
				$"git clone { cloneArg }";
			if (!string.IsNullOrEmpty(ext) && ext == ".exe") {
				// Windows
				arg += " && exit";
			}
			else {
				// Unix
				arg = $"-c \"{ arg }\"";
			}
			pInfo.Arguments = arg;

			pInfo.RedirectStandardOutput = true;
			pInfo.RedirectStandardError = true;
			pInfo.UseShellExecute = false;
			pInfo.CreateNoWindow = true;

			Process process = Process.Start(pInfo);
			process.WaitForExit();

			string line = process.StandardOutput.ReadToEnd();
			string other = process.StandardError.ReadToEnd();
			if (!string.IsNullOrEmpty(line) || !string.IsNullOrEmpty(other))
			{
				int firstIndex = other.IndexOf("'");
				int lastIndex = other.IndexOf("'", firstIndex + 1);

				folder = other.Substring(firstIndex + 1, lastIndex - firstIndex - 1);

			}
			else
			{
				folder = string.Empty;
			}

			return folder;
		}
    }
}
