﻿using System;
using System.Linq;
using Newtonsoft.Json;
using CodeChowder.CSMS.API.Models;
using CodeChowder.CSMS.API.Settings;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.API.Helpers
{
    public static class UserLicenseInfo
    {
        public static LicenseData GetLicenseData(ApplicationUser user, TokenSettings tokenSettings, DefaultSettings defaultSettings)
        {
            if (user.License != null)
            {
                var encLicenseKey = user.License.License;

                try
                {
                    var decryptedString = Crypto.DecryptString(encLicenseKey, tokenSettings.SecretKey);

                    LicenseData license = JsonConvert.DeserializeObject<LicenseData>(decryptedString);

                    if (DateTime.Compare(license.ExperationDate.Value, DateTime.Now) >= 0)
                    {
                        return license;
                    }
                }
                catch (Exception e) {
                }
            }
            return new LicenseData(defaultSettings);
        }

        public static LicenseStatus CanUseLicense(string licenseEncrypted, IApplicationUserService userService, TokenSettings tokenSettings)
        {
            try
            {
                var decryptedString = Crypto.DecryptString(licenseEncrypted, tokenSettings.SecretKey);
                LicenseData license = JsonConvert.DeserializeObject<LicenseData>(decryptedString);

                if (userService.Queryable.Count(u => u.License.License == decryptedString) < license.AllowedUsers)
                {
                    if (DateTime.Compare(license.ExperationDate.Value, DateTime.Now) < 0)
                    {
                        return LicenseStatus.Expired;
                    }
                    return LicenseStatus.Valid;
                }
                else
                {
                    return LicenseStatus.InUse;
                }
            }
            catch (Exception e)
            {
                return LicenseStatus.Invalid;
            }
        }
    }
}
