﻿using Autofac;
using AutoMapper;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using CodeChowder.CSMS.DAL.Services;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.Contexts;
using CodeChowder.CSMS.Repository.Repositories;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;
using CodeChowder.CSMS.API.Settings;
using CodeChowder.CSMS.API.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using CodeChowder.CSMS.SDK.Models;

namespace CodeChowder.CSMS.API
{
    public partial class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    b => b.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            var defaultSettings = Configuration.GetSection("Defaults");

            services.Configure<DefaultSettings>(defaultSettings);

            var executionSettings = Configuration.GetSection("ExecutionSettings");
            services.Configure<ExecutionSettings>(executionSettings);

            var tokenSettings = Configuration.GetSection("TokenAuthentication");
            services.Configure<TokenSettings>(tokenSettings);

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IAuthorizationHandler, AccountHandler>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Default",
                                  policy => policy.Requirements.Add(new AccountRequirement()));
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            });
            services.AddAutoMapper();

            //Create the container builder
            var builder = new ContainerBuilder();

            // Register dependencies, populate the services from
            // the collection, and build the container. If you want
            // to dispose of the container at the end of the app,
            // be sure to keep a reference to it as a property or field.
            builder.Populate(services);

            string connectionString = defaultSettings.GetValue<string>("ConnectionString");

            string envConnectionString = Environment.GetEnvironmentVariable("MYSQL_CONNECTION_STRING");

            if(!string.IsNullOrEmpty(envConnectionString)) {
                connectionString = envConnectionString;
            }

            builder.RegisterType<CSMSDbContext>().WithParameter("connectionString", connectionString);
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

            // Repositories
            builder.RegisterType<ApplicationUserRepository>().As<IRepository<ApplicationUser>>();
            builder.RegisterType<DesignMetricRepository>().As<IRepository<DesignMetric>>();
            builder.RegisterType<IgnoreFileRepository>().As<IRepository<IgnoreFile>>();
            builder.RegisterType<MetricSeverityRepository>().As<IRepository<MetricSeverity>>();
            builder.RegisterType<MetricResultRepository>().As<IRepository<MetricResult>>();
            builder.RegisterType<ProjectRepository>().As<IRepository<Project>>();
            builder.RegisterType<ProjectMetricsRepository>().As<IRepository<ProjectMetrics>>();
            builder.RegisterType<SharedProjectRepository>().As<IRepository<SharedProject>>();
            builder.RegisterType<LicenseKeyRepository>().As<IRepository<LicenseKey>>();

            // Services
            builder.RegisterType<ApplicationUserService>().As<IApplicationUserService>();
            builder.RegisterType<DesignMetricService>().As<IDesignMetricService>();
            builder.RegisterType<IgnoreFileService>().As<IIgnoreFileService>();
            builder.RegisterType<MetricSeverityService>().As<IMetricSeverityService>();
            builder.RegisterType<MetricResultService>().As<IMetricResultService>();
            builder.RegisterType<ProjectService>().As<IProjectService>();
            builder.RegisterType<ProjectMetricsService>().As<IProjectMetricsService>();
            builder.RegisterType<SharedProjectService>().As<ISharedProjectService>();
            builder.RegisterType<LicenseKeyService>().As<ILicenseKeyService>();

            this.ApplicationContainer = builder.Build();
            
            

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(this.ApplicationContainer);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            ConfigureAuth(app);

            app.UseCors("CorsPolicy");

            app.UseMvc();

            // If you want to dispose of resources that have been resolved in the
            // application container, register for the "ApplicationStopped" event.
            // You can only do this if you have a direct reference to the container,
            // so it won't work with the above ConfigureContainer mechanism.
            appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
        }
    }
}