﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChowder.CSMS.API.Settings
{
    public class DefaultSettings
    {
        public int AllowedProjects { get; set; }
        public int DefaultAccount { get; set; }
        public string ConnectionString { get; set; }
        public int MaxFreeMetrics { get; set; }
    }
}
