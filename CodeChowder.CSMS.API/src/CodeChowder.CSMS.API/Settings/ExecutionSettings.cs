﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChowder.CSMS.API.Settings
{
    public class ExecutionSettings
    {
        public string NodePath { get; set; }

        public string TsMetricsPath { get; set; }

        public string GitPath { get; set; }

        public string TerminalPath { get; set; }

        public string JavaPath { get; set; }

        public string JavaMetricsPath { get; set; }
    }
}
