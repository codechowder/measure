"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const common_1 = require("./common");
const MetricResult_1 = require("../Models/MetricResult");
class dcc {
    constructor() {
        this._customTypes = [];
    }
    dccMetric(node, sourceCodeType, fileName) {
        let dccMetricResults = [];
        let obtainObjects = new common_1.common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes = obtainObjects.retVal;
        this.obtainDccMetric(obtainObjects, node, sourceCodeType, fileName, dccMetricResults);
        classes.forEach((aClass) => {
            this.obtainDccMetric(obtainObjects, aClass, sourceCodeType, fileName, dccMetricResults);
        });
        return dccMetricResults;
    }
    obtainDccMetric(obtainObjects, node, sourceCodeType, fileName, dccMetricResults) {
        this._customTypes = [];
        let objectName = "Global";
        if (node) {
            let aClass = node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }
        let dccMetric = new MetricResult_1.MetricResult();
        dccMetric.NameSpace = "Default";
        dccMetric.ObjectName = objectName;
        dccMetric.MetricName = "DCC";
        dccMetric.Location = fileName;
        dccMetric.SourceType = sourceCodeType;
        dccMetric.CreationDate = new Date();
        dccMetric.Value = 0;
        obtainObjects = new common_1.common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions = obtainObjects.retVal;
        constructors.forEach((construct) => {
            this.recursiveParameterList(construct);
        });
        methods.forEach((method) => {
            this.recursiveParameterList(method);
            this.recursiveVars(method);
            if (method && method.type && method.kind && method.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(method.type.getText()) === -1) {
                    this._customTypes.push(method.type.getText());
                }
            }
        });
        functions.forEach((func) => {
            this.recursiveParameterList(func);
            this.recursiveVars(func);
            if (func && func.type && func.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(func.type.getText()) === -1) {
                    this._customTypes.push(func.type.getText());
                }
            }
        });
        obtainObjects = new common_1.common(ts.SyntaxKind.PropertyDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let fields = obtainObjects.retVal;
        fields.forEach((field) => {
            this.recursiveFields(field);
        });
        obtainObjects = new common_1.common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters = obtainObjects.retVal;
        getters.forEach((property) => {
            this.recursiveParameterList(property);
            if (property && property.type && property.type.kind && property.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(property.type.getText()) === -1) {
                    this._customTypes.push(property.type.getText());
                }
            }
        });
        obtainObjects = new common_1.common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters = obtainObjects.retVal;
        setters.forEach((property) => {
            this.recursiveParameterList(property);
            if (property && property.type && property.type.kind && property.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(property.type.getText()) === -1) {
                    this._customTypes.push(property.type.getText());
                }
            }
        });
        dccMetric.Value += this._customTypes.length;
        dccMetricResults.push(dccMetric);
    }
    recursiveVars(node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.VariableDeclaration) {
            let variable = node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveVars(node);
        });
    }
    recursiveFields(node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.PropertyDeclaration) {
            let variable = node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveFields(node);
        });
    }
    recursiveParameterList(node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.Parameter) {
            let variable = node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveParameterList(node);
        });
    }
}
exports.dcc = dcc;
//# sourceMappingURL=dcc.js.map