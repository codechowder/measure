﻿import * as ts from 'typescript';
import { common } from './common';
import { MetricResult } from '../Models/MetricResult';
import { SourceCodeType } from "../Models/SourceCodeType";

export class wmc {

    private _wmc: number;
    public constructor() {
        this._wmc = 0;
    }

    public wmcMetric(node: ts.Node, sourceCodeType: SourceCodeType, fileName: string): MetricResult[] {

        let wmcMetricResults: MetricResult[] = [];

        let obtainObjects: common = new common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes: ts.ClassDeclaration[] = <ts.ClassDeclaration[]>obtainObjects.retVal;

        this.obtainWmcMetrics(obtainObjects, node, sourceCodeType, fileName, wmcMetricResults);

        classes.forEach((aClass: ts.ClassDeclaration) => {
            this.obtainWmcMetrics(obtainObjects, aClass, sourceCodeType, fileName, wmcMetricResults);
        });

        return wmcMetricResults;
    }

    private obtainWmcMetrics(obtainObjects: common, node: ts.Node | ts.ClassDeclaration, sourceCodeType: SourceCodeType, fileName: string, wmcMetricResults: MetricResult[]) {
        let wmcMetric: MetricResult = new MetricResult();

        let objectName = "Global";

        if (<ts.ClassDeclaration>node) {
            let aClass = <ts.ClassDeclaration>node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }

        wmcMetric.NameSpace = "Default";
        wmcMetric.ObjectName = objectName;
        wmcMetric.MetricName = "WMC";
        wmcMetric.SourceType = sourceCodeType;
        wmcMetric.Location = fileName;
        wmcMetric.CreationDate = new Date();
        wmcMetric.Value = 0;

        obtainObjects = new common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors: ts.ConstructorDeclaration[] = <ts.ConstructorDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods: ts.MethodDeclaration[] = <ts.MethodDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions: ts.FunctionDeclaration[] = <ts.FunctionDeclaration[]>obtainObjects.retVal;

        constructors.forEach((construct: ts.ConstructorDeclaration) => {
            this._wmc = 0;
            this.recursiveBodyWmc(construct);
            wmcMetric.Value += this._wmc + 1;
        });

        methods.forEach((method: ts.MethodDeclaration) => {
            this._wmc = 0;
            this.recursiveBodyWmc(method.body);
            wmcMetric.Value += this._wmc + 1;
        });

        functions.forEach((func: ts.FunctionDeclaration) => {
            this._wmc = 0;
            this.recursiveBodyWmc(func.body);
            wmcMetric.Value += this._wmc + 1;
        });

        obtainObjects = new common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters: ts.GetAccessorDeclaration[] = <ts.GetAccessorDeclaration[]>obtainObjects.retVal;
        getters.forEach((property: ts.GetAccessorDeclaration) => {
            this._wmc = 0;
            this.recursiveBodyWmc(property);
            wmcMetric.Value += this._wmc + 1;
        });

        obtainObjects = new common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters: ts.SetAccessorDeclaration[] = <ts.SetAccessorDeclaration[]>obtainObjects.retVal;
        setters.forEach((property: ts.SetAccessorDeclaration) => {
            this._wmc = 0;
            this.recursiveBodyWmc(property);
            wmcMetric.Value += this._wmc + 1;
        });

        wmcMetricResults.push(wmcMetric);
    }

    private recursiveBodyWmc(node: ts.Node) {
        if (node && node.kind) {
            switch (node.kind) {
                case ts.SyntaxKind.IfStatement:
                case ts.SyntaxKind.SwitchStatement:
                case ts.SyntaxKind.WhileStatement:
                case ts.SyntaxKind.CatchClause:
                case ts.SyntaxKind.ForInStatement:
                case ts.SyntaxKind.ForOfStatement:
                case ts.SyntaxKind.ForStatement:
                case ts.SyntaxKind.FunctionDeclaration:
                case ts.SyntaxKind.ArrowFunction:
                case ts.SyntaxKind.MethodDeclaration:
                case ts.SyntaxKind.ConditionalExpression:
                    this._wmc++;
                    break;
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveBodyWmc(node);
        });
    }   
}