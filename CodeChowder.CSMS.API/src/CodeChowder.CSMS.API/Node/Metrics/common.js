"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
class common {
    constructor(kind) {
        this._kind = kind;
        this._retVal = [];
    }
    get retVal() {
        return this._retVal;
    }
    /**
     * Recursively Grabs the item based off the kind from the constructed kind object
     * @param node typescript node
     */
    recursiveGrabItem(node) {
        if (node.kind === this._kind) {
            this._retVal.push(node);
        }
        ts.forEachChild(node, (node) => {
            this.recursiveGrabItem(node);
        });
    }
}
exports.common = common;
//# sourceMappingURL=common.js.map