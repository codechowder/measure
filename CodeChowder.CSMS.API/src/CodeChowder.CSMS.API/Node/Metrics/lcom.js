"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const common_1 = require("./common");
const MetricResult_1 = require("../Models/MetricResult");
class lcom {
    constructor() {
        this._vars = [];
        this._methodSet = [];
    }
    lcomMetric(node, sourceCodeType, fileName) {
        let lcomMetricResults = [];
        let obtainObjects = new common_1.common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes = obtainObjects.retVal;
        this.obtainLcomMetrics(obtainObjects, node, sourceCodeType, fileName, lcomMetricResults);
        classes.forEach((aClass) => {
            this._methodSet = [];
            this._vars = [];
            this.obtainLcomMetrics(obtainObjects, aClass, sourceCodeType, fileName, lcomMetricResults);
        });
        return lcomMetricResults;
    }
    obtainLcomMetrics(obtainObjects, node, sourceCodeType, fileName, lcomMetricResults) {
        let lcom = 0;
        //The cardinality of set P
        let p = 0;
        //The cardinality of set Q
        let q = 0;
        obtainObjects = new common_1.common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.PropertyDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let fields = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.ArrowFunction);
        obtainObjects.recursiveGrabItem(node);
        let lambdas = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions = obtainObjects.retVal;
        let methodCount = constructors.length + methods.length + functions.length + lambdas.length + getters.length + setters.length;
        this._fields = fields.map((f) => {
            return f.name.getText();
        });
        let I = [];
        let objectName = "Global";
        if (node) {
            let aClass = node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }
        let lcomResult = new MetricResult_1.MetricResult();
        lcomResult.NameSpace = "Default";
        lcomResult.ObjectName = objectName;
        lcomResult.MetricName = "LCOM";
        lcomResult.Location = fileName;
        lcomResult.SourceType = sourceCodeType;
        lcomResult.CreationDate = new Date();
        lcomResult.Value = 0;
        constructors.forEach((c) => {
            this.recursiveFields(c);
            this.recursiveVars(c);
        });
        methods.forEach((method) => {
            this.recursiveFields(method);
            this.recursiveVars(method);
        });
        functions.forEach((funct) => {
            this.recursiveFields(funct);
            this.recursiveVars(funct);
        });
        fields.forEach((field) => {
            this.recursiveFields(field);
            this.recursiveVars(field);
        });
        getters.forEach((getter) => {
            this.recursiveFields(getter);
            this.recursiveVars(getter);
        });
        setters.forEach((setter) => {
            this.recursiveFields(setter);
            this.recursiveVars(setter);
        });
        if (this._methodSet.length > 0) {
            I.push(this._methodSet);
            for (let i = 0; i < methodCount; i++) {
                for (let j = 0; j < methodCount; j++) {
                    let intersection = [];
                    if (i < I.length && j < I.length) {
                        intersection = this.intersect(I[i], I[j]);
                    }
                    if (!intersection || !intersection.length) {
                        ++p;
                    }
                    else {
                        ++q;
                    }
                }
            }
        }
        //# Now p is the number of pairs of methods which don't have
        //# have any instance variable accesses in common.
        //#
        //# q is the number of pairs of methods which DO have an instance
        //# variable access in common
        if (p > q) {
            lcom = p - q;
        }
        lcomResult.Value = lcom;
        lcomMetricResults.push(lcomResult);
    }
    intersect(a, b) {
        let setA = new Set(a);
        let setB = new Set(b);
        let intersection = new Set([...setA].filter(x => setB.has(x)));
        return Array.from(intersection);
    }
    recursiveVars(node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.VariableDeclaration) {
            let variable = node;
            let name = variable.name.getText();
            if (this._vars.indexOf(name) > -1 && this._methodSet.indexOf(name) === -1) {
                this._methodSet.push(name);
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveVars(node);
        });
    }
    recursiveFields(node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.PropertyDeclaration) {
            let variable = node;
            let name = variable.name.getText();
            if (this._fields.indexOf(name) > -1 && this._methodSet.indexOf(name) === -1) {
                this._methodSet.push(name);
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveFields(node);
        });
    }
}
exports.lcom = lcom;
//# sourceMappingURL=lcom.js.map