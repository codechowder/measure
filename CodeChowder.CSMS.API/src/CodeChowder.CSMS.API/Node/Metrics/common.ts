﻿import * as ts from 'typescript';

export class common {

    private _kind: ts.SyntaxKind;
    private _retVal: ts.Node[];

    public constructor(kind: ts.SyntaxKind) {
        this._kind = kind;
        this._retVal = [];
    }

    public get retVal(): ts.Node[] {
        return this._retVal;
    }

    /**
     * Recursively Grabs the item based off the kind from the constructed kind object
     * @param node typescript node
     */
    public recursiveGrabItem(node: ts.Node) {
        if (node.kind === this._kind) {
            this._retVal.push(node);
        }



        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveGrabItem(node);
        });
    }
}