import * as ts from 'typescript';
import { common } from './common';
import { MetricResult } from '../Models/MetricResult';
import { SourceCodeType } from "../Models/SourceCodeType";

export class lcom {

    private _vars: string[];
    private _fields: string[];
    private _methodSet: string[];

    public constructor() {
        this._vars = [];
        this._methodSet = [];
    }

    public lcomMetric(node: ts.Node, sourceCodeType: SourceCodeType, fileName: string): MetricResult[] {

        let lcomMetricResults: MetricResult[] = [];

        let obtainObjects: common = new common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes: ts.ClassDeclaration[] = <ts.ClassDeclaration[]>obtainObjects.retVal;

        this.obtainLcomMetrics(obtainObjects, node, sourceCodeType, fileName, lcomMetricResults);

        classes.forEach((aClass: ts.ClassDeclaration) => {
            this._methodSet = [];
            this._vars = [];
            this.obtainLcomMetrics(obtainObjects, aClass, sourceCodeType, fileName, lcomMetricResults);

        });

        return lcomMetricResults;
    }

    private obtainLcomMetrics(obtainObjects: common, node: ts.Node | ts.ClassDeclaration, sourceCodeType: SourceCodeType, fileName: string, lcomMetricResults: MetricResult[]) {
        let lcom: number = 0;
        //The cardinality of set P
        let p: number = 0;
        //The cardinality of set Q
        let q: number = 0;

        obtainObjects = new common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors: ts.ConstructorDeclaration[] = <ts.ConstructorDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods: ts.MethodDeclaration[] = <ts.MethodDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters: ts.GetAccessorDeclaration[] = <ts.GetAccessorDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters: ts.SetAccessorDeclaration[] = <ts.SetAccessorDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.PropertyDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let fields: ts.PropertyDeclaration[] = <ts.PropertyDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.ArrowFunction);
        obtainObjects.recursiveGrabItem(node);
        let lambdas: ts.ArrowFunction[] = <ts.ArrowFunction[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions: ts.FunctionDeclaration[] = <ts.FunctionDeclaration[]>obtainObjects.retVal;

        let methodCount: number = constructors.length + methods.length + functions.length + lambdas.length + getters.length + setters.length;

        this._fields = fields.map((f: ts.PropertyDeclaration) => {
            return f.name.getText();
        });

        let I: string[][] = [];

        let objectName = "Global";
        if (<ts.ClassDeclaration>node) {
            let aClass = <ts.ClassDeclaration>node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }

        let lcomResult: MetricResult = new MetricResult();
        lcomResult.NameSpace = "Default";
        lcomResult.ObjectName = objectName;
        lcomResult.MetricName = "LCOM";
        lcomResult.Location = fileName;
        lcomResult.SourceType = sourceCodeType;
        lcomResult.CreationDate = new Date();
        lcomResult.Value = 0;

        constructors.forEach((c: ts.ConstructorDeclaration) => {
            this.recursiveFields(c);
            this.recursiveVars(c);
        });

        methods.forEach((method: ts.MethodDeclaration) => {
            this.recursiveFields(method);
            this.recursiveVars(method);
        });

        functions.forEach((funct: ts.FunctionDeclaration) => {
            this.recursiveFields(funct);
            this.recursiveVars(funct);
        });

        fields.forEach((field: ts.PropertyDeclaration) => {
            this.recursiveFields(field);
            this.recursiveVars(field);
        });

        getters.forEach((getter: ts.GetAccessorDeclaration) => {
            this.recursiveFields(getter);
            this.recursiveVars(getter);
        });

        setters.forEach((setter: ts.SetAccessorDeclaration) => {
            this.recursiveFields(setter);
            this.recursiveVars(setter);
        });

        if (this._methodSet.length > 0) {
            I.push(this._methodSet);

            for (let i: number = 0; i < methodCount; i++) {
                for (let j: number = 0; j < methodCount; j++) {
                    let intersection: string[] = [];

                    if (i < I.length && j < I.length) {
                        intersection = this.intersect(I[i], I[j]);
                    }

                    if (!intersection || !intersection.length) {
                        ++p;
                    }
                    else {
                        ++q;
                    }
                }
            }
        }

        //# Now p is the number of pairs of methods which don't have
        //# have any instance variable accesses in common.
        //#
        //# q is the number of pairs of methods which DO have an instance
        //# variable access in common

        if (p > q) {
            lcom = p - q;
        }

        lcomResult.Value = lcom;

        lcomMetricResults.push(lcomResult);
    }

    private intersect(a: string[], b: string[]): string[] {
        let setA: Set<string> = new Set(a);
        let setB: Set<string> = new Set(b);
        let intersection: Set<string> = new Set([...setA].filter(x => setB.has(x)));
        return Array.from(intersection);
    }

    private recursiveVars(node: ts.Node): void {
        if (node && node.kind && node.kind === ts.SyntaxKind.VariableDeclaration) {
            let variable: ts.VariableDeclaration = <ts.VariableDeclaration>node;

            let name: string = variable.name.getText();

            if (this._vars.indexOf(name) > -1 && this._methodSet.indexOf(name) === -1) {
                this._methodSet.push(name);
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveVars(node);
        });
    }

    private recursiveFields(node: ts.Node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.PropertyDeclaration) {
            let variable: ts.PropertyDeclaration = <ts.PropertyDeclaration>node;

            let name: string = variable.name.getText();

            if (this._fields.indexOf(name) > -1 && this._methodSet.indexOf(name) === -1) {
                this._methodSet.push(name);
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveFields(node);
        });
    }
}