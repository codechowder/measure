﻿using System;
namespace CodeChowder.CSMS.API.ResponseModels
{
    public class GenericResponse
    {
        public bool Success { get; set; }
        public object Message { get; set; }
        public int Count { get; set; } = 0;
    }
}
