


export enum SourceCodeType { 
    cSharp = 0,
    typeScript = 1,
    typeScriptReact = 2,
    javaScript = 3,
    javaScriptReact = 4,
    visualBasic = 5
}