﻿using System;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.API.DTOs
{
    public class MetricResultDto
    {
		/// <summary>
		/// The Id for storing into a table
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Name of the Metric
		/// </summary>
		public string MetricName { get; set; }

		/// <summary>
		/// Namespace of the Metric
		/// </summary>
		public string NameSpace { get; set; }

		/// <summary>
		/// Object Name of the Metric
		/// </summary>
		public string ObjectName { get; set; }

		/// <summary>
		/// Location for this Metric
		/// </summary>
		public string Location { get; set; }

		/// <summary>
		/// Method Name of the Metric
		/// </summary>
		public string MethodName { get; set; }

		/// <summary>
		/// Cyclomatic Complexity
		/// </summary>
		public int Value { get; set; }

		/// <summary>
		/// Line Number
		/// </summary>
		public int LineNummber { get; set; }

		/// <summary>
		/// Column Number
		/// </summary>
		public int ColumnNumber { get; set; }

		/// <summary>
		/// The Date the metric was created
		/// </summary>
		public DateTime CreationDate { get; set; }

		public SourceCodeType SourceType { get; set; }
    }
}
