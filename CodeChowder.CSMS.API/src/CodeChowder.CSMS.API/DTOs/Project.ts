
export interface IProject { 
    id: number;
    name: string;
    projectMetrics: ProjectMetrics[];
    customMetrics: DesignMetric[];
    ignoreFiles: IgnoreFile[];
    severityTypes: MetricSeverity[];
}

