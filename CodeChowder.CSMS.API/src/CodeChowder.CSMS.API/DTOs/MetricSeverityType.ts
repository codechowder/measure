


export enum MetricSeverityType { 
    severe = 0,
    cautious = 1,
    acceptable = 2
}