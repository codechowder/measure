﻿$Classes(CodeChowder.CSMS.DataStructures.*)[
export interface I$Name { $Properties[
    $name: $Type;]
}]

$Enums(CodeChowder.CSMS.DataStructures.*)[
export enum $Name { $Values[
    $name = $Value][,]
}]