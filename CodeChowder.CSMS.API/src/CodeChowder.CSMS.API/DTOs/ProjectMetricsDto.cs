﻿using System;
using System.Collections.Generic;

namespace CodeChowder.CSMS.API.DTOs
{
    public class ProjectMetricsDto
    {
		public int Id { get; set; }

		public string Name { get; set; }

		public DateTime Created { get; set; }

		public IList<MetricResultDto> MetricResults { get; set; }
    }
}
