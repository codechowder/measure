
export interface IIgnoreFile { 
    id: number;
    fileExtenstion: string;
    specificFile: string;
}

