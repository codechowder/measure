﻿using System.Collections.Generic;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.API.DTOs
{
    public class SharedProjectDto
    {
        public int Id { get; set; }

        public PermissionsType Permissions { get; set; }

        public ProjectDto Project { get; set; }
    }
}