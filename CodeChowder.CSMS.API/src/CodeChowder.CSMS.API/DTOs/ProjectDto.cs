﻿using System;
using System.Collections.Generic;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.API.DTOs
{
    public class ProjectDto
    {
		public int Id { get; set; }

		public string Name { get; set; }

		public string SourceRepoUrl { get; set; }

		public IList<ProjectMetricsDto> ProjectMetrics { get; set; }

		public IList<DesignMetric> CustomMetrics { get; set; }

		public IList<IgnoreFile> IgnoreFiles { get; set; }

		public IList<MetricSeverity> SeverityTypes { get; set; }
    }
}
