
export interface IMetricSeverity { 
    id: number;
    metric: DesignMetric;
    severityType: MetricSeverityType;
    sourceCode: SourceCodeType;
    value: number;
}

