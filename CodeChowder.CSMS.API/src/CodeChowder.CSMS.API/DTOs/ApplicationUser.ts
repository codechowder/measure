
export interface IApplicationUser { 
    id: number;
    email: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    company: string;
    active: boolean;
    password: string;
    allowedProjects: number;
    projects: Project[];
}

