
export interface IProjectMetrics { 
    id: number;
    name: string;
    created: Date;
    metricResults: MetricResult[];
    project: Project;
}

