﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;

namespace CodeChowder.CSMS.DAL.Services
{
    public class SharedProjectService : CoreDataAccess<SharedProject>, ISharedProjectService
    {
        #region Public Constructors

        public SharedProjectService(IRepository<SharedProject> repository)
            : base(repository)
        {
        }

        #endregion Public Constructors
    }
}