﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.DAL.Services
{
    public class MetricSeverityService : CoreDataAccess<MetricSeverity>, IMetricSeverityService
    {
        #region Public Constructors

        public MetricSeverityService(IRepository<MetricSeverity> repository)
            : base(repository) {
        }

        #endregion Public Constructors
    }
}
