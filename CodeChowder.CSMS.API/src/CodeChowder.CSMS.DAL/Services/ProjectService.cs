﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;

namespace CodeChowder.CSMS.DAL.Services
{
    public class ProjectService : CoreDataAccess<Project>, IProjectService
    {
        #region Public Constructors

        public ProjectService(IRepository<Project> repository)
            : base(repository)
        {
        }

        #endregion Public Constructors
    }
}