﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.DAL.Services.Interfaces
{
    public interface IMetricResultService : ICoreDataAccess<MetricResult>
    {
    }
}