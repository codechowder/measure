﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.DAL.Services
{
    public class IgnoreFileService : CoreDataAccess<IgnoreFile>, IIgnoreFileService
    {
        #region Public Constructors

        public IgnoreFileService(IRepository<IgnoreFile> repository)
            : base(repository) {
        }

        #endregion Public Constructors
    }
}
