﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.DAL.Services
{
    public class DesignMetricService : CoreDataAccess<DesignMetric>, IDesignMetricService
    {
        #region Public Constructors

        public DesignMetricService(IRepository<DesignMetric> repository)
            : base(repository) {
        }

        #endregion Public Constructors
    }
}
