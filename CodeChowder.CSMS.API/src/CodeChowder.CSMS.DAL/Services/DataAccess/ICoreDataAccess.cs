﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CodeChowder.CSMS.DAL.Services.DataAccess
{
    public interface ICoreDataAccess<T> where T : class, new()
    {
        /// <summary>
        /// Gets the queryable.
        /// </summary>
        /// <value>The queryable.</value>
        IQueryable<T> Queryable { get; }

        #region Public Methods

        /// <summary>
        /// Gets the count of all items in the table
        /// </summary>
        /// <returns>count</returns>
        Task<int> Count();

        /// <summary>
        /// Creates a new entry into the Database
        /// </summary>
        /// <param name="entry">entry to insert</param>
        /// <returns>bool - true if successful</returns>
        Task<bool> Create(T entry);

		/// <summary>
		/// Creates new entries into the Database
		/// </summary>
		/// <param name="entries">entries to insert</param>
		/// <returns>bool - true if successful</returns>
		Task<bool> CreateMany(IEnumerable<T> entries);

        /// <summary>
        /// Entry to remove from the database
        /// </summary>
        /// <param name="entry">the entry to remove</param>
        /// <returns>bool - true if successful</returns>
        bool Delete(T entry);

		/// <summary>
		/// Entry to remove from the database
		/// </summary>
		/// <param name="entry">the entry to remove</param>
		/// <returns>bool - true if successful</returns>
		Task<bool> Delete(IQueryable<T> entry);

        /// <summary>
        /// Reads an entity given the id
        /// </summary>
        /// <param name="where">The query to find the entities</param>
        /// <param name="includes">additional tables to include</param>
        /// <returns>Entity found</returns>
        Task<T> ReadById(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Reads all possible entities that are in the database given the where clause
        /// </summary>
        /// <param name="where">The query to find the entities</param>
        /// <param name="includes">Additional tables to include</param>
        /// <returns>collection of entities</returns>
        IEnumerable<T> ReadMany(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Reads by skip and limit
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="limit"></param>
        /// <param name="includes"></param>
        /// <returns></returns>
        IEnumerable<T> ReadPaging(int skip, int limit, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Save changes to the entries
        /// </summary>
        /// <returns>bool - true if successful</returns>
        bool Save();

        /// <summary>
        /// Updates the entry
        /// </summary>
        /// <param name="entry">entry to be updated</param>
        /// <returns>bool - true if successful</returns>
        bool Update(T entry);

        #endregion Public Methods
    }
}