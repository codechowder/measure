﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.DAL.Services.DataAccess
{
    public class CoreDataAccess<T> : ICoreDataAccess<T> where T : class, new()
    {
        #region Protected Fields

        /// <summary>
        /// Generic Repository
        /// </summary>
        protected readonly IRepository<T> _repository;
        
        #endregion Protected Fields

        #region Public Constructors

        public CoreDataAccess(IRepository<T> repository) {
            _repository = repository;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// Gets the queryable.
        /// </summary>
        /// <value>The queryable.</value>
        public IQueryable<T> Queryable
        {
            get { return _repository.Queryable; }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.Count"/>
        /// </summary>
        public async Task<int> Count()
        {
            return await _repository.Count();
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.Create(T)"/>
        /// </summary>
        public virtual async Task<bool> Create(T entry) {
            return await _repository.Create(entry);
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.CreateMany"/> 
        /// </summary>
        /// <returns>The many.</returns>
        /// <param name="entries">Entries.</param>
        public virtual async Task<bool> CreateMany(IEnumerable<T> entries) {
            return await _repository.CreateMany(entries);
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.Delete(T)"/>
        /// </summary>
        public virtual bool Delete(T entry) {
            return _repository.Delete(entry);
        }

		/// <summary>
		/// <see cref="ICoreDataAccess{T}.Delete(IQueryable)"/>
		/// </summary>
        public virtual async Task<bool> Delete(IQueryable<T> entry)
		{
			return await _repository.Delete(entry);
		}

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.ReadById(Expression{Func{T, bool}}, Expression{Func{T, object}}[])"/>
        /// </summary>
        public virtual async Task<T> ReadById(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes) {
            return await _repository.ReadyById(where, includes);
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.ReadMany(Expression{Func{T, bool}}, Expression{Func{T, object}}[])"/>
        /// </summary>
        public virtual IEnumerable<T> ReadMany(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes) {
            return _repository.ReadyMany(where, includes);
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.Save"/>
        /// </summary>
        public virtual bool Save() {
            bool result = true;
            try {
                result = _repository.SaveChanges();
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

        public IEnumerable<T> ReadPaging(int skip, int limit, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _repository.Queryable;
            if (includes != null && includes.Count() > 0)
            {
                foreach (var property in includes)
                {
                    query = query.Include(property);
                }
            }

            return query.Skip(skip).Take(limit);
        }

        /// <summary>
        /// <see cref="ICoreDataAccess{T}.Update(T)"/>
        /// </summary>
        public virtual bool Update(T entry) {
            return _repository.Update(entry);
        }

        #endregion Public Methods
    }
}