﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.DAL.Services
{
    /// <summary>
    /// Implementation of the Application User Service
    /// </summary>
    public class LicenseKeyService : CoreDataAccess<LicenseKey>, ILicenseKeyService
    {
        #region Public Constructors

        public LicenseKeyService(IRepository<LicenseKey> repository)
            : base(repository) {
        }

        #endregion Public Constructors
    }
}