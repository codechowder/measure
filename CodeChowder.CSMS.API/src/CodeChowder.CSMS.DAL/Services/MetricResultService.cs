﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;

namespace CodeChowder.CSMS.DAL.Services
{
    /// <summary>
    /// Implementation of the Metric Result Service
    /// </summary>
    public class MetricResultService : CoreDataAccess<MetricResult>, IMetricResultService
    {
        #region Public Constructors

        public MetricResultService(IRepository<MetricResult> repository)
            : base(repository) {
        }

        #endregion Public Constructors
    }
}