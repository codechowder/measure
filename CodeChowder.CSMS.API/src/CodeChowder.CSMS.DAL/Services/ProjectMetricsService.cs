﻿using CodeChowder.CSMS.DAL.Services.DataAccess;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository;

namespace CodeChowder.CSMS.DAL.Services
{
    public class ProjectMetricsService : CoreDataAccess<ProjectMetrics>, IProjectMetricsService
    {
        #region Public Constructors

        public ProjectMetricsService(IRepository<ProjectMetrics> repository)
            : base(repository)
        {
        }

        #endregion Public Constructors
    }
}