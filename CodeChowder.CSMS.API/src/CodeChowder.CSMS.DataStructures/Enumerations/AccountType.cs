﻿public enum AccountType {
    Free = 0,
    Medium = 1,
    Large = 2,
    Admin = 99
}
