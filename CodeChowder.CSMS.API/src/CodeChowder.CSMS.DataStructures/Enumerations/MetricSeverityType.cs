﻿namespace CodeChowder.CSMS.DataStructures.Enumerations
{
    /// <summary>
    /// Enumeration for Metric Severity
    /// </summary>
    public enum MetricSeverityType
    {
        /// <summary>
        /// Refactor is necessary.
        /// </summary>
        Severe,

        /// <summary>
        /// Potentially problematic.  If developing against this, be cautious
        /// </summary>
        Cautious,

        /// <summary>
        /// Acceptable Range.
        /// </summary>
        Acceptable
    }
}