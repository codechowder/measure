﻿using System;
namespace CodeChowder.CSMS.DataStructures.Enumerations
{
    public enum PermissionsType
    {
        Read = 0,   //000
        Write = 1   //001
    }
}
