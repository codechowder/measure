﻿using System.ComponentModel.DataAnnotations;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class MetricSeverity
    {
        [Key]
        public int Id { get; set; }

        public string MetricName { get; set; }

        public MetricSeverityType SeverityType { get; set; }

        public SourceCodeType SourceCode { get; set; }

        public double Value { get; set; }
    }
}