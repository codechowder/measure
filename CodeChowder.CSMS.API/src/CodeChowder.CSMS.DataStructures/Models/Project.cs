﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CodeChowder.CSMS.SDK.Models;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class Project
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string SourceRepoUrl { get; set; }

        public IList<ProjectMetrics> ProjectMetrics { get; set; }

        public IList<DesignMetric> CustomMetrics { get; set; }

        public IList<IgnoreFile> IgnoreFiles { get; set; }

        public IList<MetricSeverity> SeverityTypes { get; set; }

        public IList<SharedProject> SharedWith { get; set; }
    }
}
