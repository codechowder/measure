﻿using System.ComponentModel.DataAnnotations;

namespace CodeChowder.CSMS.DataStructures.Models
{
    /// <summary>
    /// Contains a specific file or extension to ignore when parsing.
    /// </summary>
    public class IgnoreFile
    {
        [Key]
        public int Id { get; set; }

        public string Pattern { get; set; }
    }
}