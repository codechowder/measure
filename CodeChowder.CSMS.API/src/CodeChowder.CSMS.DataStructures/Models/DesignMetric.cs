﻿using System.ComponentModel.DataAnnotations;

namespace CodeChowder.CSMS.DataStructures.Models
{
    /// <summary>
    /// Design Metric for creating custom metrics - not implemented yet!
    /// </summary>
    public class DesignMetric
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public string JsLocation { get; set; }
    }
}