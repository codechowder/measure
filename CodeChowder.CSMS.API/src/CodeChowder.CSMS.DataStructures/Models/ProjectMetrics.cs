﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CodeChowder.CSMS.SDK.Models;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class ProjectMetrics
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public IList<MetricResult> MetricResults { get; set; }

        public Project Project { get; set; }
    }
}
