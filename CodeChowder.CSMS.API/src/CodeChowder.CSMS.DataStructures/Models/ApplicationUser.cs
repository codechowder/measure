﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class ApplicationUser
    {
        
        [Key]
        public int Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Company { get; set; }

        public bool Active { get; set; }

        public string Password { get; set; }

        public int AllowedProjects { get; set; }

        public IList<Project> Projects { get; set; }

        public AccountType Account { get; set; }

        public IList<SharedProject> SharedProjects { get; set; }

        public LicenseKey License { get; set; }
    }
}
