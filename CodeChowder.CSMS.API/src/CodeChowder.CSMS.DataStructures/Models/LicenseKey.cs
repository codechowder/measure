﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class LicenseKey
    {
        [Key]
        public int Id { get; set; }

        public string License { get; set; }
    }
}
