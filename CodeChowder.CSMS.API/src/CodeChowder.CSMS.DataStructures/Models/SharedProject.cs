﻿using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.DataStructures.Models
{
    public class SharedProject
    {
        public int Id { get; set; }

        public PermissionsType Permissions { get; set; }

        public Project Project { get; set; }

        public ApplicationUser SharedWith { get; set; }
    }
}
