﻿using CodeChowder.CSMS.Repository.Contexts;

namespace CodeChowder.CSMS.Repository.UnitOfWork
{
    /// <summary>
    /// Interface for Unit of Work
    /// </summary>
    public interface IUnitOfWork
    {

        CSMSDbContext DbContext { get; }

        #region Public Methods

        void Dispose();

        /// <summary>
        /// Commits the database context data changes
        /// </summary>
        bool Commit();



        #endregion Public Methods
    }
}