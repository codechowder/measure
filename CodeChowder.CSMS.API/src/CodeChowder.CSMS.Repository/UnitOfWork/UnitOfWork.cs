﻿using CodeChowder.CSMS.Repository.Contexts;

namespace CodeChowder.CSMS.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        /// <summary>
        /// Kicak Variable for the Database Context
        /// </summary>
        private readonly CSMSDbContext _dbContext;

        /// <summary>
        /// Constructor that takes in the Database Factory
        /// </summary>
        /// <param name="dbFactory">Database Factory Interface</param>
        public UnitOfWork(CSMSDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public CSMSDbContext DbContext {
            get {
                return _dbContext;
            }
        }

        /// <summary>
        /// <see cref="IUnitOfWork.Commit"/>
        /// </summary>
        public bool Commit()
        {
            int result = 0;
            result = _dbContext.SaveChanges();

            return result > 0;
        }

        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}