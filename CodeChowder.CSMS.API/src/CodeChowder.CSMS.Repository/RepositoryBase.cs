﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CodeChowder.CSMS.Repository.Contexts;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository
{
    /// <summary>
    /// Implementation of the Repository
    /// </summary>
    /// <typeparam name="T">Database Code First Model</typeparam>
    public class RepositoryBase<T> : IRepository<T> where T : class, new()
    {
        #region Private Fields

        /// <summary>
        /// Local Variable for the database set
        /// </summary>
        private readonly DbSet<T> _dbSet;

        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        /// <summary>
        /// <see cref="IRepository{T}.Queryable"/>
        /// </summary>
        public IQueryable<T> Queryable { get; set; }

        #region Public Constructors

        public RepositoryBase(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
            _dbSet = _unitOfWork.DbContext.Set<T>();
            Queryable = _dbSet;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Dispose()
        {
            if (_unitOfWork.DbContext != null)
                _unitOfWork.DbContext.Dispose();
        }

        /// <summary>
        /// <see cref="IRepository{T}.Count"/>
        /// </summary>
        public async Task<int> Count()
        {
            return await _dbSet.CountAsync();
        }

        /// <summary>
        /// <see cref="IRepository{T}.Create(T)"/>
        /// </summary>
        public async Task<bool> Create(T entity) {
            bool result = true;
            if (entity == null) {
                throw new ArgumentNullException("entity");
            }
            try {
                await _dbSet.AddAsync(entity);
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// <see cref="IRepository{T}.CreateMany"/> 
        /// </summary>
        public async Task<bool> CreateMany(IEnumerable<T> entities) {
			bool result = true;
			if (entities == null)
			{
				throw new ArgumentNullException("entities");
			}
			try
			{
				await _dbSet.AddRangeAsync(entities);
			}
			catch (Exception exp)
			{
				result = false;
			}

			return result;
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(Expression{Func{T, bool}})"/>
        /// </summary>
        public bool Delete(Expression<Func<T, bool>> where) {
            bool result = true;
            if (where == null) {
                throw new ArgumentNullException("where");
            }
            try {
                IEnumerable<T> objects = _dbSet.Where<T>(where).AsEnumerable();
                _dbSet.RemoveRange(objects);
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// <see cref="IRepository{T}.Delete(T)"/>
        /// </summary>
        public bool Delete(T entity) {
            bool result = true;
            if (entity == null) {
                throw new ArgumentNullException("entity");
            }
            try {
                _dbSet.Remove(entity);
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

		/// <summary>
		/// <see cref="IRepository{T}.Delete(IQueryable<T>)"/>
		/// </summary>
        public async Task<bool> Delete(IQueryable<T> queryItem)
		{
			bool result = true;
            T entity = await queryItem.FirstOrDefaultAsync();
			if (entity == null)
			{
				throw new ArgumentNullException("entity");
			}
			try
			{
				_dbSet.Remove(entity);
			}
			catch (Exception exp)
			{
				result = false;
			}

			return result;
		}

        /// <summary>
        /// <see cref="IRepository{T}.DeleteById(Expression{Func{T, bool}})"/>
        /// </summary>
        public bool DeleteById(Expression<Func<T, bool>> where) {
            bool result = true;

            if (where == null) {
                throw new ArgumentNullException("where");
            }

            try {
                T itemToDelete = _dbSet.Where(where).FirstOrDefault();
                _dbSet.Remove(itemToDelete);
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// <see cref="IRepository{T}.Read(Expression{Func{T, bool}}, Expression{Func{T, object}}[])"/>
        /// </summary>
        public async Task<T> Read(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes) {
            if (where == null) {
                throw new ArgumentNullException("where");
            }

            IQueryable<T> query = _dbSet.Where(where);

            if (includes != null && includes.Count() > 0) {
                foreach (var property in includes) {
                    query = query.Include(property);
                }
            }
            return await query.FirstOrDefaultAsync();
        }

        /// <summary>
        /// <see cref="IRepository{T}.ReadyById(Expression{Func{T, bool}}, Expression{Func{T, object}}[])"/>
        /// </summary>
        public async Task<T> ReadyById(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes) {
            if (where == null) {
                throw new ArgumentNullException("where");
            }

            IQueryable<T> query = _dbSet.Where(where);

            if (includes != null && includes.Count() > 0) {
                foreach (var property in includes) {
                    query = query.Include(property);
                }
            }

            return await query.FirstOrDefaultAsync();
        }

        /// <summary>
        /// <see cref="IRepository{T}.ReadyMany(Expression{Func{T, bool}}, Expression{Func{T, object}}[])"/>
        /// </summary>
        public IQueryable<T> ReadyMany(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes) {
            if (where == null) {
                throw new ArgumentNullException("where");
            }

            IQueryable<T> query = _dbSet.Where(where);

            if (includes != null && includes.Count() > 0) {
                foreach (var property in includes) {
                    query = query.Include(property);
                }
            }

            return query;
        }

        /// <summary>
        /// <see cref="IRepository{T}.Update(T)"/>
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(T entity) {
            bool result = true;

            if (entity == null) {
                throw new ArgumentNullException("entity");
            }

            try {
                _dbSet.Attach(entity);
                _unitOfWork.DbContext.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception exp) {
                result = false;
            }

            return result;
        }

        public bool SaveChanges()
        {
            int result = 0;
            result = _unitOfWork.DbContext.SaveChanges();

            return result > 0;

        }

        #endregion Public Methods
    }
}