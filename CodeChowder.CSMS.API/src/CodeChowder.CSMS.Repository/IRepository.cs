﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CodeChowder.CSMS.Repository
{
    /// <summary>
    /// Interface Repository for CRUD Operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class, new()
    {
        #region Public Methods

        /// <summary>
        /// Gets the Count of items in the table
        /// </summary>
        /// <returns>count</returns>
        Task<int> Count();

        /// <summary>
        /// Creates a new entity into the database
        /// </summary>
        /// <param name="entity">The entity to create</param>
        /// <exception cref="ArgumentNullException">Thrown when entity is null</exception>
        /// <returns>bool - true if successful</returns>
        Task<bool> Create(T entity);

		/// <summary>
		/// Creates many entities into the database
		/// </summary>
		/// <param name="entities">The entities to create</param>
		/// <exception cref="ArgumentNullException">Thrown when entity is null</exception>
		/// <returns>bool - true if successful</returns>
		Task<bool> CreateMany(IEnumerable<T> entities);

        /// <summary>
        /// Deletes an entity from the database
        /// </summary>
        /// <param name="entity">The entity to delete</param>
        /// <exception cref="ArgumentNullException">Thrown when entity is null</exception>
        /// <returns>bool - true if successful</returns>
        bool Delete(T entity);

        /// <summary>
        /// Deletes entities that match the where clause
        /// </summary>
        /// <param name="where">The query to find entities to delete</param>
        /// <exception cref="ArgumentNullException">thrown when where is null</exception>
        /// <returns>bool - true if successful</returns>
        bool Delete(Expression<Func<T, bool>> where);

		/// <summary>
		/// Deletes entity based off queryable - will only delete first item
		/// </summary>
		/// <param name="queryItem">The query to find entities to delete</param>
		/// <exception cref="ArgumentNullException">thrown when where is null</exception>
		/// <returns>bool - true if successful</returns>
		Task<bool> Delete(IQueryable<T> queryItem);

        /// <summary>
        /// Deletes an entity based off the id
        /// </summary>
        /// <param name="where">The query to find the entity</param>
        /// <exception cref="ArgumentNullException">thrown when id is null</exception>
        /// <returns>bool - true if successful</returns>
        bool DeleteById(Expression<Func<T, bool>> where);

        /// <summary>
        /// Reads a single entity based off the where clause
        /// </summary>
        /// <param name="where">The query to find the entity</param>
        /// <param name="includes">Additional tables to include</param>
        /// <exception cref="ArgumentNullException">thrown when where is null</exception>
        /// <returns>The entity found</returns>
        Task<T> Read(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Reads an entity given the id
        /// </summary>
        /// <param name="where">The query to find the entity</param>
        /// <param name="includes">additional tables to include</param>
        /// <exception cref="ArgumentNullException">thrown when id is null</exception>
        /// <returns>Entity found</returns>
        Task<T> ReadyById(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Reads all possible entities that are in the database given the where clause
        /// </summary>
        /// <param name="where">The query to find the entities</param>
        /// <param name="includes">Additional tables to include</param>
        /// <exception cref="ArgumentNullException">thrown when where is null</exception>
        /// <returns>IQueryable collection of entities - this allows addtional database functionality on the object</returns>
        IQueryable<T> ReadyMany(Expression<Func<T, bool>> where, params Expression<Func<T, object>>[] includes);

        /// <summary>
        /// Updates the given entity
        /// </summary>
        /// <param name="entity">the entity to update</param>
        /// <returns>bool - true if successful</returns>
        bool Update(T entity);

        /// <summary>
        /// Saves changes on the context
        /// </summary>
        /// <returns></returns>
        bool SaveChanges();
        
        /// <summary>
        /// Expose Queryable
        /// </summary>
        IQueryable<T> Queryable { get; set; }

        #endregion Public Methods
    }
}