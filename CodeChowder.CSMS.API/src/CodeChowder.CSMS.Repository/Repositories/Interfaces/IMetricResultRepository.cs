﻿using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.Repository.Repositories.Interfaces
{
	/// <summary>
	/// The Application User Repository
	/// NOTE: There is nothing here but if something specific to this repository should be done, this is where you create the contract
	/// </summary>
	public interface IMetricResultRepository : IRepository<MetricResult>
	{
	}
}