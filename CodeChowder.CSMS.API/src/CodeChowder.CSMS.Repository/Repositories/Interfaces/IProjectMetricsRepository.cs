﻿using System;
using System.Collections.Generic;
using System.Text;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.Repository.Repositories.Interfaces
{
    public interface IProjectMetricsRepository: IRepository<ProjectMetrics>
    {
    }
}
