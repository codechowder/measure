﻿using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.Repository.Repositories.Interfaces
{
    public interface IIgnoreFileRepository : IRepository<IgnoreFile>
    {
    }
}
