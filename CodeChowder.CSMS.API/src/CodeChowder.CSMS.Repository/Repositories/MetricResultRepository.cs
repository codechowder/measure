﻿using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository.Repositories
{
    /// <summary>
    /// The implementation of the Metric Result Repository
    /// </summary>
    public class MetricResultRepository : RepositoryBase<MetricResult>, IMetricResultRepository
    {
        #region Public Constructors

        public MetricResultRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors
    }
}