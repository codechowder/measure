﻿using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository.Repositories
{
    public class DesignMetricRepository : RepositoryBase<DesignMetric>, IDesignMetricRepository
    {
        #region Public Constructors

        public DesignMetricRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors
    }
}