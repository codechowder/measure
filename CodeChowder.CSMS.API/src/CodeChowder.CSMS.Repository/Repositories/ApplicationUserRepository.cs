﻿using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository.Repositories
{
    /// <summary>
    /// The implementation of the Application User Repository
    /// </summary>
    public class ApplicationUserRepository : RepositoryBase<ApplicationUser>, IApplicationUserRepository
    {
        #region Public Constructors

        public ApplicationUserRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors
    }
}