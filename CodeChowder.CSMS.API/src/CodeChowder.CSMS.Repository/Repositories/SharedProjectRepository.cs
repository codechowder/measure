﻿using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository.Repositories
{
    public class SharedProjectRepository : RepositoryBase<SharedProject>, ISharedProjectRepository
    {
        #region Public Constructors

        public SharedProjectRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors
    }
}