﻿using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.Repository.Repositories.Interfaces;
using CodeChowder.CSMS.Repository.UnitOfWork;

namespace CodeChowder.CSMS.Repository.Repositories
{
    public class ProjectMetricsRepository : RepositoryBase<ProjectMetrics>, IProjectMetricsRepository
    {
        #region Public Constructors

        public ProjectMetricsRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors
    }
}