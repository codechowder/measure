﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using MySQL.Data.EntityFrameworkCore.Extensions;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.SDK.Models;

namespace CodeChowder.CSMS.Repository.Contexts
{
    /// <summary>
    /// The database context for CSMS
    /// </summary>
    public class CSMSDbContext : DbContext
    {
        #region Public Constructors

        public CSMSDbContext(string connectionString, string database = null) : base(new DbContextOptionsBuilder<DbContext>().UseMySQL(connectionString).Options)
        {
            bool test = Database.EnsureCreated();
            //Database.Migrate();
        }

        public CSMSDbContext(DbContextOptions<DbContext> options, string databaseName = null) : base(options)
        {
            DatabaseName = databaseName;
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// The database name.  Used specifically for any Initializers
        /// </summary>
        public string DatabaseName { get; private set; }

        //TODO:  Add DBSets here

        /// <summary>
        /// Database Table for Application Users
        /// </summary>
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public DbSet<DesignMetric> DesignMetrics { get; set; }

        public DbSet<IgnoreFile> IgnoreFiles { get; set; }

        public DbSet<MetricSeverity> MetricSeverities { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<ProjectMetrics> ProjectMetrics { get; set; }

        public DbSet<MetricResult> MetricResults { get; set; }

        public DbSet<SharedProject> SharedProjects { get; set; }

        public DbSet<LicenseKey> LicenseKeys { get; set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Forces a SaveChanges on the database context
        /// </summary>
        public void Commit()
        {
            base.SaveChanges();
        }

        #endregion Public Methods

        #region Protected Methods

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationUser>()
                .HasIndex(u => u.Email)
                .IsUnique();

            modelBuilder.Entity<Project>()
                .HasMany(p => p.SharedWith)
                .WithOne(sp => sp.Project)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<LicenseKey>()
                .Property(l => l.License).HasColumnType("text");

            base.OnModelCreating(modelBuilder);
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder) {
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        //    Database.SetInitializer(new CreateDatabaseIfNotExists<MGDbContext>());

        //    base.OnModelCreating(modelBuilder);
        //}

        #endregion Protected Methods
    }
}