codechowder.com
Readme File

Prerequisites
1. Make sure you have installed and are running docker before installation. (https://www.docker.com/)
2. Make sure you have the installation script permissions set correctly to execute.
2.1 chmod 777 install.sh
3. Make sure you have internet connection.
4. Make sure you are on the web server you wish to install the web applicaiton to.

How to Install
1. Open up terminal or command prompt
2. Unzip CodeChowder-Measure-v.1.0-Beta.zip
2. Change directory to directory of installation (unzipped directory from previous step)
3. Type ./install.sh -a
3.1 If you are using Ubuntu use the command: /bin/bash ./install.sh -a
4. Answer Yes on MySQL question, if it is the first time setting up the web server.
5. Wait for complete installation.

After installation wait for about 5 minutes before use. MYSQL docker image will need time to boot up.

Navigate to localhost:3000 for application.