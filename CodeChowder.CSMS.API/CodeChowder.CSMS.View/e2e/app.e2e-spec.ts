import { Zenoware.CSMS.ViewPage } from './app.po';

describe('zenoware.csms.view App', () => {
  let page: Zenoware.CSMS.ViewPage;

  beforeEach(() => {
    page = new Zenoware.CSMS.ViewPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
