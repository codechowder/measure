export enum SourceCodeType { 
    CSharp = 0,
    TypeScript = 1,
    TypeScriptReact = 2,
    JavaScript = 3,
    JavaScriptReact = 4,
    VisualBasic = 5,
    Java = 6,
    Other = 99
}