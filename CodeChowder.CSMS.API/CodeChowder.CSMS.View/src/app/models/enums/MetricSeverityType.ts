export enum MetricSeverityType { 
    Severe = 0,
    Cautious = 1,
    Acceptable = 2
}