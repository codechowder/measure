import { IMetricResult } from './interfaces/IMetricResult';
import { SourceCodeType } from "./enums/SourceCodeType";
import { IProjectMetrics } from "app/models/interfaces/IProjectMetrics";
import { ProjectMetrics } from "app/models/ProjectMetrics";

export class MetricResult implements IMetricResult {
    public metricName: string;
    public nameSpace: string;
    public objectName: string;
    public location: string;
    public methodName: string;
    public value: number;
    public lineNummber: number;
    public columnNumber: number;
    public sourceType: SourceCodeType;
    public creationDate: Date;
    public projectMetric: IProjectMetrics;

    public constructor(metricResult?: IMetricResult) {
        this.metricName = metricResult && metricResult.metricName || "";
        this.nameSpace= metricResult && metricResult.nameSpace || "";
        this.objectName = metricResult && metricResult.objectName || "";
        this.location= metricResult && metricResult.location || "";
        this.methodName = metricResult && metricResult.methodName || "";
        this.value= metricResult && metricResult.value || 0;
        this.lineNummber= metricResult && metricResult.lineNummber || 0;
        this.columnNumber= metricResult && metricResult.columnNumber || 0;
        this.sourceType = metricResult && metricResult.sourceType || SourceCodeType.TypeScript;
        this.creationDate = metricResult && metricResult.creationDate || new Date();
        this.projectMetric = metricResult && metricResult.projectMetric || new ProjectMetrics();
    }
}