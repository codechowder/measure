import { IChartByLanguage } from "app/models/interfaces/IChartByLanguage";
import { ChartDataSets } from "@types/chart.js";
import { SourceCodeType } from "app/models/enums/SourceCodeType";

export class ChartByLanguage implements IChartByLanguage {
    public sourceType: SourceCodeType;
    public avgWMC: number;
    public wmcCount: number;
    public dataArr: { [key: number]: number[] };
    public avgLCOM: number;
    public lcomCount: number;
    public avgDCC: number;
    public dccCount: number;
    public avgSLOC: number;
    public slocCount: number;

    public constructor() {
        this.sourceType = SourceCodeType.CSharp;
        this.avgWMC = 0;
        this.wmcCount = 0;
        this.dataArr = {};
        this.avgLCOM = 0;
        this.lcomCount = 0;
        this.avgDCC = 0;
        this.dccCount = 0;
        this.avgSLOC = 0;
        this.slocCount = 0;
    }
}