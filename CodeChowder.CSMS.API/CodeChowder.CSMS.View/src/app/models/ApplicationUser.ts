import { LicenseKey } from './LicenseKey';
import { ILicenseKey } from './interfaces/ILicenseKey';
import { IApplicationUser } from './interfaces/IApplicationUser';
import { IProject } from "./interfaces/IProject";

export class ApplicationUser implements IApplicationUser {
    public active: boolean;
    public allowedProjects: number;
    public company: string;
    public email: string;
    public firstName: string;
    public id: number;
    public lastName: string;
    public license: ILicenseKey;
    public password: string;
    public phoneNumber: string;
    public projects: IProject[];

    public constructor(applicationUser?: IApplicationUser) {
        this.active = applicationUser && applicationUser.active || false;
        this.allowedProjects = applicationUser && applicationUser.allowedProjects || 0;
        this.company = applicationUser && applicationUser.company || "";
        this.email = applicationUser && applicationUser.email || "";
        this.firstName = applicationUser && applicationUser.firstName || "";
        this.id = applicationUser && applicationUser.id || 0;
        this.lastName = applicationUser && applicationUser.lastName || "";
        this.license = applicationUser && applicationUser.license && new LicenseKey(applicationUser.license) || null;
        this.phoneNumber = applicationUser && applicationUser.phoneNumber || "";
        this.projects = applicationUser && applicationUser.projects && applicationUser.projects.map(p => p) || [];
    }
}