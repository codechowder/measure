import { Project } from './Project';
import { ApplicationUser } from './ApplicationUser';
import { PermissionsType } from './enums/PermissionsType';
import { IProject } from './interfaces/IProject';
import { IApplicationUser } from './interfaces/IApplicationUser';
import { ISharedProject } from './interfaces/ISharedProject';

export class SharedProject implements ISharedProject {
    public id: number;
    public owner: IApplicationUser;
    public permissions: PermissionsType;
    public project: IProject;
    public sharedWith: IApplicationUser;

    constructor(shared: ISharedProject) {
        this.id = shared && shared.id || 0;
        this.owner = shared && shared.owner && new ApplicationUser(shared.owner) || null;
        this.permissions = shared && shared.permissions || PermissionsType.Read;
        this.project = shared && shared.project && new Project(shared.project) || null;
        this.sharedWith = shared && shared.sharedWith && new ApplicationUser(shared.sharedWith) || null;
    }
}