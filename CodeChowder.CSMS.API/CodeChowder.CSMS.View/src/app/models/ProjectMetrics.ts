import { IProject } from './interfaces/IProject';
import { IDesignMetric } from './interfaces/IDesignMetric';
import { IIgnoreFile } from './interfaces/IIgnoreFile';
import { IMetricResult } from './interfaces/IMetricResult';
import { IMetricSeverity } from './interfaces/IMetricSeverity';
import { IProjectMetrics } from './interfaces/IProjectMetrics';
import { Project } from './Project';

export class ProjectMetrics implements IProjectMetrics {
    public id: number;
    public name: string;
    public created: Date;
    public metricResults: IMetricResult[];
    public project: Project;

    public constructor(projectMetrics?: IProjectMetrics) {
        this.id = projectMetrics && projectMetrics.id || 0;
        this.name = projectMetrics && projectMetrics.name || "";
        this.created = projectMetrics && projectMetrics.created || new Date();
        this.metricResults = projectMetrics && projectMetrics.metricResults || [];
        this.project = projectMetrics && projectMetrics.project || null;
    }
}