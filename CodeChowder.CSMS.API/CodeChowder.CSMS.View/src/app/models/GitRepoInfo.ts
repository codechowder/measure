import { IGitCommit } from './interfaces/IGitCommit';
import { IGitRepoInfo } from './interfaces/IGitRepoInfo';

export class GitRepoInfo implements IGitRepoInfo {
    public gitCommits: IGitCommit[];
    public password: string;
    public selectedBranch: string;
    public url: string;
    public userName: string;

    constructor(info?: IGitRepoInfo) {
        this.gitCommits = info && info.gitCommits && info.gitCommits.map(c => c) || [];
        this.password = info && info.password || "";
        this.selectedBranch = info && info.selectedBranch || "";
        this.url = info && info.url || "";
        this.userName = info && info.userName || "";
    }
}