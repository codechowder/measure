import { IDesignMetric } from "./interfaces/IDesignMetric";

export class DesignMetric implements IDesignMetric {
    public id: number;
    public name: string;
    public jsLocation: string;

    public constructor(designMetric?: IDesignMetric) {
        this.id = designMetric && designMetric.id || 0;
        this.name = designMetric && designMetric.name || "";
        this.jsLocation = designMetric && designMetric.jsLocation || "";
    }
}