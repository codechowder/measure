import { ILicenseData } from './interfaces/ILicenseData';

export class LicenseData implements ILicenseData {
    public allowedProjects: number;
    public allowedUsers: number;
    public allowShare: boolean;
    public experationDate: Date;
    public maxFreeMetrics: number;

    constructor(data?: ILicenseData) {
        this.allowedProjects = data && data.allowedProjects || 0;
        this.allowedUsers = data && data.allowedUsers || 0;
        this.allowShare = data && data.allowShare || false;
        this.experationDate = data && data.experationDate && new Date(data.experationDate) || null;
        this.maxFreeMetrics = data && data.maxFreeMetrics || 0;
    }
}