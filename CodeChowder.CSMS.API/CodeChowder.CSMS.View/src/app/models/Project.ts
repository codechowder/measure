import { IProject } from './interfaces/IProject';
import { IDesignMetric } from './interfaces/IDesignMetric';
import { IIgnoreFile } from './interfaces/IIgnoreFile';
import { IMetricResult } from './interfaces/IMetricResult';
import { IMetricSeverity } from './interfaces/IMetricSeverity';
import { IProjectMetrics } from "./interfaces/IProjectMetrics";

export class Project implements IProject {
    public id: number;
    public name: string;
    public projectMetrics: IProjectMetrics[];
    public customMetrics: IDesignMetric[];
    public ignoreFiles: IIgnoreFile[];
    public severityTypes: IMetricSeverity[];
    public sourceRepoUrl: string;

    public constructor(project?: IProject) {
        this.id = project && project.id || 0;
        this.name = project && project.name || "";
        this.projectMetrics = project && project.projectMetrics && project.projectMetrics.map(m => m) || [];
        this.customMetrics = project && project.customMetrics && project.customMetrics.map(m => m) || [];
        this.ignoreFiles = project && project.ignoreFiles && project.ignoreFiles.map(i => i) || [];
        this.severityTypes = project && project.severityTypes && project.severityTypes.map(s => s) || [];
        this.sourceRepoUrl = project && project.sourceRepoUrl || "";
    }
}