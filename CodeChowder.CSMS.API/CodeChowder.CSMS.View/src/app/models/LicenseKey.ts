import { ILicenseKey } from "./interfaces/ILicenseKey";

export class LicenseKey implements ILicenseKey {
    public id: number;
    public license: string;

    constructor(license?: ILicenseKey) {
        this.id = license && license.id || 0;
        this.license = license && license.license || "";
    }
}