import { ProjectSelectionType } from '../../enums/clientEnums/ProjectSelectionType';
import { IProject } from '../interfaces/IProject';
import { IProjectSelection } from './interfaces/IProjectSelection';

export class ProjectSelection implements IProjectSelection {
    public project: IProject;
    public selectionType: ProjectSelectionType;

    constructor(project: IProject, selectionType: ProjectSelectionType) {
        this.project = project || null;
        this.selectionType = selectionType || ProjectSelectionType.Settings;
    }
}