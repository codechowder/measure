import { ProjectSelectionType } from '../../../enums/clientEnums/ProjectSelectionType';
import { IProject } from '../../interfaces/IProject';

export interface IProjectSelection {
    project: IProject;
    selectionType: ProjectSelectionType;
}