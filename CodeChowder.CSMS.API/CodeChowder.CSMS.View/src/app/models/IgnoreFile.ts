import { IIgnoreFile } from "./interfaces/IIgnoreFile";

export class IgnoreFile implements IIgnoreFile {
    public id: number;
    public pattern: string;

    public constructor(ignoreFile?: IIgnoreFile) {
        this.id = ignoreFile && ignoreFile.id || 0;
        this.pattern = ignoreFile && ignoreFile.pattern || "";
    }
}