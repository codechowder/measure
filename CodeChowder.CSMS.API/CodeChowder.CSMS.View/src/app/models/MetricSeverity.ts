import { IMetricSeverity } from './interfaces/IMetricSeverity';
import { IDesignMetric } from "./interfaces/IDesignMetric";
import { MetricSeverityType } from "./enums/MetricSeverityType";
import { SourceCodeType } from "./enums/SourceCodeType";

export class MetricSeverity implements IMetricSeverity {
    public id: number;
    public metricName: string;
    public severityType: MetricSeverityType;
    public sourceCode: SourceCodeType;
    public value: number;

    public constructor(metricSeverity?: IMetricSeverity) {
        this.id = metricSeverity && metricSeverity.id || 0;
        this.metricName = metricSeverity && metricSeverity.metricName || null;
        this.severityType = metricSeverity && metricSeverity.severityType || MetricSeverityType.Acceptable;
        this.sourceCode = metricSeverity && metricSeverity.sourceCode || SourceCodeType.TypeScript;
        this.value = metricSeverity && metricSeverity.value || 0;
    }
}