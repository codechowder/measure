export interface ILicenseKey {
    id: number;
    license: string;
}