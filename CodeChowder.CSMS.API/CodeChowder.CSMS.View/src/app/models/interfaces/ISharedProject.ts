import { IProject } from './IProject';
import { IApplicationUser } from "app/models/interfaces/IApplicationUser";
import { PermissionsType } from "app/models/enums/PermissionsType";

export interface ISharedProject {
    id: number;
    owner: IApplicationUser;
    permissions: PermissionsType;
    project: IProject;
    sharedWith: IApplicationUser;
}