import { IMetricSeverity } from './IMetricSeverity';


import { IDesignMetric } from "app/models/interfaces/IDesignMetric";
import { IIgnoreFile } from "app/models/interfaces/IIgnoreFile";
import { IProjectMetrics } from "app/models/interfaces/IProjectMetrics";

export interface IProject {
    customMetrics: IDesignMetric[];
    id: number;
    ignoreFiles: IIgnoreFile[];
    name: string;
    projectMetrics: IProjectMetrics[];
    severityTypes: IMetricSeverity[];
    sourceRepoUrl: string;
}

