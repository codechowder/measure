
export interface IIgnoreFile { 
    id: number;
    pattern: string;
}

