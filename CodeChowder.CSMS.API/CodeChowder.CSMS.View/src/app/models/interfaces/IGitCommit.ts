export interface IGitCommit {
    commit: string;
    date: Date;
    message: string;
}