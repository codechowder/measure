
import { IDesignMetric } from "app/models/interfaces/IDesignMetric";
import { MetricSeverityType } from "app/models/enums/MetricSeverityType";
import { SourceCodeType } from "app/models/enums/SourceCodeType";

export interface IMetricSeverity {
    id: number;
    metricName: string;
    severityType: MetricSeverityType;
    sourceCode: SourceCodeType;
    value: number;
}

