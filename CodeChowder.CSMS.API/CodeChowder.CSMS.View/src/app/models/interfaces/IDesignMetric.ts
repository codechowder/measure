
export interface IDesignMetric { 
    id: number;
    name: string;
    jsLocation: string;
}

