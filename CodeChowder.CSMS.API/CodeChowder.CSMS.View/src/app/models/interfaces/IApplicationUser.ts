import { ILicenseKey } from './ILicenseKey';

import { IProject } from "app/models/interfaces/IProject";

export interface IApplicationUser {
    active: boolean;
    allowedProjects: number;
    company: string;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
    license: ILicenseKey;
    password: string;
    phoneNumber: string;
    projects: IProject[];
}
