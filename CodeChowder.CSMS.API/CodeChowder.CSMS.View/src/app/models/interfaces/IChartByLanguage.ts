import { SourceCodeType } from "app/models/enums/SourceCodeType";

export interface IChartByLanguage {
    sourceType: SourceCodeType;
    avgWMC: number;
    wmcCount: number;
    dataArr: { [key: number]: number[] };
    avgLCOM: number;
    lcomCount: number;
    avgDCC: number;
    dccCount: number;
    avgSLOC: number;
    slocCount: number;
}