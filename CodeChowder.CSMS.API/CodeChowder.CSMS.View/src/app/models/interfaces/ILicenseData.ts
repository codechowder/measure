export interface ILicenseData {
    allowedProjects: number;
    allowShare: boolean;
    allowedUsers: number;
    experationDate: Date;
    maxFreeMetrics: number;
}