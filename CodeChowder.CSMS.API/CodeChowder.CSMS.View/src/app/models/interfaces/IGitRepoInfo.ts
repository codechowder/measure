import { IGitCommit } from './IGitCommit';
export interface IGitRepoInfo {
    gitCommits: IGitCommit[];
    password: string;
    selectedBranch: string;
    url: string;
    userName: string;
}