
import { SourceCodeType } from "app/models/enums/SourceCodeType";
import { IProjectMetrics } from "app/models/interfaces/IProjectMetrics";

export interface IMetricResult {
    metricName: string;
    nameSpace: string;
    objectName: string;
    location: string;
    methodName: string;
    value: number;
    lineNummber: number;
    columnNumber: number;
    sourceType: SourceCodeType;
    creationDate: Date;
    projectMetric: IProjectMetrics;
}

