import { MetricResult } from '../MetricResult';
import { Project } from '../Project';

export interface IProjectMetrics { 
    id: number;
    name: string;
    created: Date;
    metricResults: MetricResult[];
    project: Project;
}