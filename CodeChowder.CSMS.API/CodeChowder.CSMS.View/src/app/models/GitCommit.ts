import { IGitCommit } from './interfaces/IGitCommit';

export class GitCommit implements IGitCommit {
    public commit: string;
    public date: Date;
    public message: string;

    constructor(commit?: IGitCommit) {
        this.commit = commit && commit.commit || "";
        this.date = commit && commit.date && new Date(commit.date) || null;
        this.message = commit && commit.message || "";
    }
}