import { IGitCommit } from '../models/interfaces/IGitCommit';
import { IGitRepoInfo } from '../models/interfaces/IGitRepoInfo';
import { ApiTools } from './data-access/ApiTools';
import { AuthManager } from '../config/AuthManager';
import { IGitService } from './interfaces/IGitService';
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class GitService implements IGitService {
    private _apiTools: ApiTools;

    constructor(
        authManager: AuthManager,
        http: Http,
        notificationService: NotificationsService,
        router: Router
    ) {
        this._apiTools = new ApiTools("api/git", authManager, http, notificationService, router);
    }

    public getCommits(info: IGitRepoInfo): Observable<IGitCommit[]> {
        return this._apiTools.post("", info).map(body => {
            return body && body.success && body.message || [];
        });
    }
}