import { ILicenseData } from '../models/interfaces/ILicenseData';
import { ILicenseKey } from '../models/interfaces/ILicenseKey';
import { ISharedProject } from '../models/interfaces/ISharedProject';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import { AuthManager } from '../config/AuthManager';
import { IApplicationUser } from '../models/interfaces/IApplicationUser';
import { IProject } from '../models/interfaces/IProject';
import { BaseService } from './BaseService';
import { ApiTools } from './data-access/ApiTools';
import { IApplicationUserService } from './interfaces/IApplicationUserService';
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class ApplicationUserService extends BaseService<IApplicationUser> implements IApplicationUserService  {

    public constructor(
        authManager: AuthManager,
        http: Http,
        notificationService: NotificationsService,
        router: Router
    ) {
        super(authManager, http, "api/users", router, notificationService);        
    }

    public createProject(userId: number, project: IProject): Observable<IApplicationUser> {
        let routePartial: string = `${userId}/project`;
        return this.mapHttpResponse(
            this.apiTools.put(routePartial, project)
        );
    }

    public getLicenseData(userId: number): Observable<ILicenseData> {
        let routePartial: string = `${userId}/licenseData`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getProjects(userId: number): Observable<IProject[]> {
        let routePartial: string = `${userId}/projects`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getSharedProjects(userId: number): Observable<ISharedProject[]> {
        let routePartial: string = `${userId}/sharedProjects`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public searchByEmail(email: string): Observable<IApplicationUser> {
        let routePartial: string = `${email}/user`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public shareProject(sharedUserId: number, project: IProject): Observable<boolean> {
        let routePartial: string = `${sharedUserId}/sharedProjects`;
        return this.mapHttpResponse(
            this.apiTools.post(routePartial, project)
        );
    }

    public unshareProject(sharedUserId: number, projectId: number): Observable<boolean> {
        let routePartial: string = `${sharedUserId}/sharedProjects/${projectId}`;
        return this.mapHttpResponse(
            this.apiTools.delete(routePartial)
        );
    }

    public updateLicense(userId: number, license: ILicenseKey): Observable<boolean> {
        let routePartial: string = `${userId}/license`;
        return this.mapHttpResponse(
            this.apiTools.put(routePartial, license)
        );
    }
}