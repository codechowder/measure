import { IMetricResult } from '../../models/interfaces/IMetricResult';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { IProjectMetrics } from '../../models/interfaces/IProjectMetrics';
import { IGitRepoInfo } from '../../models/interfaces/IGitRepoInfo';
import { IMetricSeverity } from '../../models/interfaces/IMetricSeverity';
import { Observable } from 'rxjs/Rx';

import { IIgnoreFile } from '../../models/interfaces/IIgnoreFile';
import { IProject } from '../../models/interfaces/IProject';
import { IBaseService } from './IBaseService';

export interface IProjectService extends IBaseService<IProject> {
    addIgnoreFile(id: number, ignoreFile: IIgnoreFile): Observable<IIgnoreFile>;
    delete(id: number): Observable<boolean>;
    deleteMetrics(projectId: number, metricsId: number): Observable<boolean>;
    getCautiousMetrics(id: number): Observable<IMetricResult[]>;
    getCompanyName(id: number): Observable<string>;
    getExportedMetrics(id: number, projectMetricId?: number): Observable<Blob>;
    getIgnoreFiles(id: number): Observable<IIgnoreFile[]>;
    getMetricsCount(id: number): Observable<number>;
    getMetricsRuns(id: number): Observable<IProjectMetrics[]>;
    getProjectWithMetrics(id: number): Observable<IProject>;
    getProjectWithSettings(id: number): Observable<IProject>;
    getSevereMetrics(id: number): Observable<IMetricResult[]>;
    getSharedWith(id: number): Observable<IApplicationUser[]>;
    getTotalSloc(id: number): Observable<number>;
    runGitMetrics(id: number, info: IGitRepoInfo): Observable<IProjectMetrics[]>;
    runMetrics(id: number, files: File[]): Observable<IProjectMetrics>;
    updateMetricSeverity(id: number, metricSeverity: IMetricSeverity): Observable<boolean>;
}