import { IGitCommit } from '../../models/interfaces/IGitCommit';
import { IGitRepoInfo } from '../../models/interfaces/IGitRepoInfo';
import { Observable } from "rxjs";

export interface IGitService {
    getCommits(info: IGitRepoInfo): Observable<IGitCommit[]>;
}