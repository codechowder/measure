import { Observable } from 'rxjs';

export interface IBaseService<T> {
    getMany(skip: number, limit: number): Observable<T>;
    getOne(id: number): Observable<T>;
    create(createObject: T): Observable<T>;
    edit(editObject: T): Observable<T>;
}