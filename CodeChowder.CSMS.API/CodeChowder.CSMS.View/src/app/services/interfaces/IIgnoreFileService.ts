import { IIgnoreFile } from '../../models/interfaces/IIgnoreFile';
import { IBaseService } from './IBaseService';

import { Observable } from "rxjs";

export interface IIgnoreFileService extends IBaseService<IIgnoreFile> {
    delete(id: number): Observable<boolean>;
}