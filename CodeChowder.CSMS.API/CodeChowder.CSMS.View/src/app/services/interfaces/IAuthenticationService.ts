import { Observable } from 'rxjs/Rx';

export interface IAuthenticationService {
    login(email: string, password: string): Observable<boolean>;
}