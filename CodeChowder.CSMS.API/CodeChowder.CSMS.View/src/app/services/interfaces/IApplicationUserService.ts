import { ILicenseData } from '../../models/interfaces/ILicenseData';
import { ILicenseKey } from '../../models/interfaces/ILicenseKey';
import { ISharedProject } from '../../models/interfaces/ISharedProject';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { IProject } from '../../models/interfaces/IProject';

import { Observable } from 'rxjs';
import { IBaseService } from "./IBaseService";

export interface IApplicationUserService extends IBaseService<IApplicationUser> {
    createProject(userId: number, project: IProject): Observable<IApplicationUser>;
    getLicenseData(userId: number): Observable<ILicenseData>;
    getProjects(userId: number): Observable<IProject[]>;
    getSharedProjects(userId: number): Observable<ISharedProject[]>;
    searchByEmail(email: string): Observable<IApplicationUser>;
    shareProject(sharedUserId: number, project: IProject): Observable<boolean>;
    unshareProject(sharedUserId: number, projectId: number): Observable<boolean>;
    updateLicense(userId: number, license: ILicenseKey): Observable<boolean>;
}