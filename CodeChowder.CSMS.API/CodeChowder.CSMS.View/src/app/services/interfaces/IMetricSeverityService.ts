import { Observable } from 'rxjs/Rx';

import { IMetricSeverity } from '../../models/interfaces/IMetricSeverity';
import { IBaseService } from './IBaseService';

export interface IMetricSeverityService extends IBaseService<IMetricSeverity> {
    delete(id: number): Observable<number>;
}