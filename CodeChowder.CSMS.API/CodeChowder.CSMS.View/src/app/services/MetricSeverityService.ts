import { AuthManager } from '../config/AuthManager';
import { IMetricSeverity } from '../models/interfaces/IMetricSeverity';
import { BaseService } from './BaseService';
import { IMetricSeverityService } from './interfaces/IMetricSeverityService';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class MetricSeverityService extends BaseService<IMetricSeverity> implements IMetricSeverityService  {

    public constructor(
        authManager: AuthManager,
        http: Http,
        notificationService: NotificationsService,
        router: Router
    ) {
        super(authManager, http, "api/metricSeverities", router, notificationService);        
    }

    public delete(id: number): Observable<number> {
        let routePartial: string = `${id}`;
        return this.mapHttpResponse(
            this.apiTools.delete(routePartial)
        );
    }
}