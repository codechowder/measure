import { IMetricResult } from '../models/interfaces/IMetricResult';
import { IApplicationUser } from '../models/interfaces/IApplicationUser';
import { IProjectMetrics } from '../models/interfaces/IProjectMetrics';
import { IGitRepoInfo } from '../models/interfaces/IGitRepoInfo';
import { IMetricSeverity } from '../models/interfaces/IMetricSeverity';
import { AuthManager } from '../config/AuthManager';
import { IIgnoreFile } from '../models/interfaces/IIgnoreFile';
import { IProject } from '../models/interfaces/IProject';
import { BaseService } from './BaseService';
import { IProjectService } from './interfaces/IProjectService';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class ProjectService extends BaseService<IProject> implements IProjectService {
    public constructor(
        authManager: AuthManager,
        http: Http,
        notificationService: NotificationsService,
        router: Router
    ) {
        super(authManager, http, "api/projects", router, notificationService);
    }

    public addIgnoreFile(id: number, ignoreFile: IIgnoreFile): Observable<IIgnoreFile> {
        let routePartial: string = `${id}/ignoreFiles`;
        return this.mapHttpResponse(
            this.apiTools.post(routePartial, ignoreFile)
        );
    }

    public delete(id: number): Observable<boolean> {
        let routePartial: string = `${id}`;
        return this.mapHttpResponse(
            this.apiTools.delete(routePartial)
        );
    }

    public deleteMetrics(projectId: number, metricsId: number): Observable<boolean> {
        let routePartial: string = `${projectId}/metrics/${metricsId}`;
        return this.mapHttpResponse(
            this.apiTools.delete(routePartial)
        );
    }

    public getCautiousMetrics(id: number): Observable<IMetricResult[]> {
        let routePartial: string = `${id}/cautiousMetrics`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getCompanyName(id: number): Observable<string> {
        let routePartial: string = `${id}/companyName`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getExportedMetrics(id: number, projectMetricId?: number): Observable<Blob> {
        let routePartial: string = `${id}/metricsExcel`;
        if (projectMetricId > 0) {
            routePartial = `${routePartial}/${projectMetricId}`;
        }
        return this.apiTools.getRaw(routePartial).map((responseData: any) => {
            return responseData || null;
        });
    }

    public getIgnoreFiles(id: number): Observable<IIgnoreFile[]> {
        let routePartial: string = `${id}/ignoreFiles`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getMetricsCount(id: number): Observable<number> {
        let routePartial: string = `${id}/metricsCount`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getMetricsRuns(id: number): Observable<IProjectMetrics[]> {
        let routePartial: string = `${id}/projectMetrics`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getProjectWithMetrics(id: number): Observable<IProject> {
        let routePartial: string = `${id}/metrics`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getProjectWithSettings(id: number): Observable<IProject> {
        let routePartial: string = `${id}/settings`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getSevereMetrics(id: number): Observable<IMetricResult[]> {
        let routePartial: string = `${id}/severeMetrics`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getSharedWith(id: number): Observable<IApplicationUser[]> {
        let routePartial: string = `${id}/sharedWith`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public getTotalSloc(id: number): Observable<number> {
        let routePartial: string = `${id}/totalSloc`;
        return this.mapHttpResponse(
            this.apiTools.get(routePartial)
        );
    }

    public runGitMetrics(id: number, info: IGitRepoInfo): Observable<IProjectMetrics[]> {
        let routePartial: string = `${id}/runGitMetrics`;
        return this.mapHttpResponse(
            this.apiTools.post(routePartial, info)
        );
    }

    public runMetrics(id: number, files: File[]): Observable<IProjectMetrics> {
        let routePartial: string = `${id}/runMetrics`;

        return this.mapHttpResponse(
            this.apiTools.postFiles(routePartial, files)
        );
    }

    public updateMetricSeverity(id: number, metricSeverity: IMetricSeverity): Observable<boolean> {
        let routePartial: string = `${id}/metricSeverities`;
        return this.apiTools.put(routePartial, metricSeverity).map((project: IProject) => {
            if (project) {
                return true;
            }
            else {
                return false;
            }
        });
    }
}