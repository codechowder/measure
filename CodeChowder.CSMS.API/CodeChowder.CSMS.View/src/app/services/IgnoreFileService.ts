import { AuthManager } from '../config/AuthManager';
import { IIgnoreFile } from '../models/interfaces/IIgnoreFile';
import { BaseService } from './BaseService';

import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class IgnoreFileService extends BaseService<IIgnoreFile> {
    constructor(
        authManager: AuthManager,
        http: Http,
        notificationService: NotificationsService,
        router: Router
    ) {
        super(authManager, http, "api/ignoreFiles", router, notificationService);
    }

    public delete(id: number): Observable<boolean> {
        let routePartial: string = `${id}`;
        return this.mapHttpResponse(
            this.apiTools.delete(routePartial)
        );
    }
}