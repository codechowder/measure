import { AuthManager } from '../config/AuthManager';
import { ApplicationUser } from '../models/ApplicationUser';
import { IApplicationUser } from '../models/interfaces/IApplicationUser';
import { SocialMediaTypes } from '../models/SocialMediaTypes';
import { ApplicationUserService } from './ApplicationUserService';
import { AuthenticationService } from './AuthenticationService';
import { IApplicationUserService } from './interfaces/IApplicationUserService';
import { IAuthenticationService } from './interfaces/IAuthenticationService';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';


declare const FB: any;
declare const IN: any;
declare const gapi: any;

export class SocialMediaLogin {
    private _authService: IAuthenticationService;
    private _applicationUserService: IApplicationUserService;
    private _authManager: AuthManager
    private _isRegister: boolean;
    private _notificationService: NotificationsService;
    private _router: Router;
    private _routeLocation: string;
    

    public constructor(
        authService: AuthenticationService,
        applicationUserService: ApplicationUserService,
        authManager: AuthManager,
        notificationService: NotificationsService,
        router: Router,
        routeLocation: string = '/',
        isRegister?: boolean
    ) {
        this._authService = authService;
        this._applicationUserService = applicationUserService;
        this._authManager = authManager;
        this._isRegister = isRegister || false;
        this._notificationService = notificationService;
        this._router = router;
        this._routeLocation = routeLocation;
    }

    public onSocialMediaLogin(socialMediaType: SocialMediaTypes) {
        this.setupSocialMediaLogin();

        switch (socialMediaType) {
            case SocialMediaTypes.Facebook:
                FB.login((response) => {
                    this.facebookStatusChangeCallback(response);
                });
                break;
            case SocialMediaTypes.LinkedIn:
                IN.User.authorize(() => {
                    this.OnLinkedInAuth();
                });
                break;
            case SocialMediaTypes.Google:
                gapi.auth2.getAuthInstance().signIn();
                break;
        }
    }

    public initGoogleApiClient() {
        // Initialize the client with API key and People API, and initialize OAuth with an
        // OAuth 2.0 client ID and scopes (space delimited string) to request access.
        gapi.client.init({
            apiKey: 'AIzaSyDdYroHHb-LQvVCJifIMAf2G_igE1lLfv4',
            discoveryDocs: ["https://people.googleapis.com/$discovery/rest?version=v1"],
            clientId: '426014257941-qmmo4gcudl1jubp6gi1nsdegf9m7q4r2.apps.googleusercontent.com',
            scope: 'profile'
        }).then(() => {
            //WTF?
            var test = "test";
            // Listen for sign-in state changes.
            gapi.auth2.getAuthInstance().isSignedIn.listen((isSignedIn: boolean) => {
                this.updateGoogleSigninStatus(isSignedIn);
            });

            // Handle the initial sign-in state.
            this.updateGoogleSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
        });
    }

    public updateGoogleSigninStatus(isSignedIn: boolean) {
        // When signin status changes, this function is called.
        // If the signin status is changed to signedIn, we make an API call.
        if (isSignedIn) {
            gapi.client.people.people.get({
                resourceName: 'people/me'
            }).then((response) => {
                this.getJwtToken(SocialMediaTypes.Google, this._authService, response.result, this._isRegister);
            }, (reason) => {
            });
        }
    }

    public OnLinkedInAuth() {
        IN.API.Raw("/people/~:(id,first-name,email-address,last-name)?format=json")
            .result((data) => {
                if (data) {
                    this.getJwtToken(SocialMediaTypes.LinkedIn, this._authService, data, this._isRegister);
                }
            })
            .error(error => {
                console.log(error.message);
            });
    }

    public facebookStatusChangeCallback(resp) {
        if (resp.status === 'connected') {
            // connect here with your server for facebook login by passing access token given by facebook
            this.getJwtToken(SocialMediaTypes.Facebook, this._authService, resp.authResponse, this._isRegister)

        } else {
            //Do we care?
        }
    };

    private setupSocialMediaLogin() {
        //Facebook
        if (FB) {
            FB.init({
                appId: '1475574062514628',
                cookie: false,  // enable cookies to allow the server to access
                // the session
                xfbml: true,  // parse social plugins on this page
                version: 'v2.8' // use graph api version 2.5
            });

            FB.getLoginStatus(response => {
                this.facebookStatusChangeCallback(response);
            });
        }

        //LinkedIn
        //TODO:  LinkedIN Code sucks!  This keeps firing UI Changes over and over...
        //if(IN) {
        //  IN.Event.onOnce(IN, "auth", this.OnLinkedInAuth());
        //}

        //Google API
        if (gapi) {
            gapi.load('client:auth2', () => {
                this.initGoogleApiClient();
            });
        }
    }

    private getJwtToken(socialMediaType: SocialMediaTypes, authService: IAuthenticationService, data: any, register: boolean = false) {
        data.isRecruiter = this._isRegister;

        let email: string = '';
        let password: string = '';
        let firstName: string = '';
        let lastName: string = '';

        switch(socialMediaType) {
            case SocialMediaTypes.LinkedIn: {
                let body = data && data.body || null;
                let profileId = data && data.body && data.body.id || null;
                if(profileId) {
                    email = body.emailAddress;
                    password = profileId;
                    firstName = body.firstName || "";
                    lastName = body.lastName || "";
                    
                    if(register) {
                        this.register(email, password, firstName, lastName);
                    }
                    else {
                        this.login(email, password);
                    }
                }
            }
            break;
            case SocialMediaTypes.Facebook: {
                let body = data && data.body || null;
                let profileId = data && data.body && data.body.metadata && data.body.metadata.sources && data.body.metadata.sources[0] || null;
                
                if(profileId) {
                    email = body.emailAddresses && body.emailAddresses[0] && body.emailAddresses[0].value;
                    firstName = body.names && body.names[0] && body.names[0].givenName || "";
                    lastName = body.names && body.names[0] && body.names[0].familyName || "";
                    password = profileId;
                    
                    if(register) {
                        this.register(email, password, firstName, lastName);
                    }
                    else {
                        this.login(email, password);
                    }
                }
            }
            break;
            case SocialMediaTypes.Google: {
                let accessToken = data && data.body && data.body.accessToken || null;
                
                if(accessToken) {
                    FB.setAccessToken(accessToken);
                    FB.api("/me", {
                        fields: ["id", "email", "first_name", "last_name"]
                    }, (response) => {
                        if (response && !response.error) {
                            email = response.email;
                            password = response.id;
                            firstName = response.first_name;
                            lastName = response.last_name;

                            if(register) {
                                this.register(email, password, firstName, lastName);
                            }
                            else {
                                this.login(email, password);
                            }
                        }
                    })
                }
            }
            break;
        }
    }

    private login(email: string, password: string): void {
        this._authService.login(email, password).subscribe((success) => {
            if(success) {
                this._router.navigate([this._routeLocation]);
            }
            else{
                this._notificationService.error("Invalid credentials.", "Please try again.  You may need to register first.");
            }
        }, (error) => {
            this._notificationService.error("An error occured.  You may need to register first.", error);
        });        
    }

    private register(email: string, password: string, firstName: string, lastName: string): void {

        let applicationUser: IApplicationUser = new ApplicationUser();

        applicationUser.email = email;
        applicationUser.password = password;
        applicationUser.firstName = firstName;
        applicationUser.lastName = lastName;

        this._applicationUserService.create(applicationUser).subscribe((user: IApplicationUser) => {
            if(user) {
                this.login(email, password);
            }
            else {
                this._notificationService.error("Invalid credentials.", "Please try again.");
            }
        }, (error: string) => {
             this._notificationService.error("An error occured.", error);
        });
    }
    
}