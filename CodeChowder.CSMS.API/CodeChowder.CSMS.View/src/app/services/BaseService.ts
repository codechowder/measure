import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from "@angular/router";
import { Observable } from 'rxjs/Rx';

import { AuthManager } from '../config/AuthManager';
import { ApiTools } from './data-access/ApiTools';
import { IBaseService } from './interfaces/IBaseService';
import { NotificationsService } from "angular2-notifications";

@Injectable()
export class BaseService<T> implements IBaseService<T> {
    private _apiTools: ApiTools;
    private _authManager: AuthManager;

    public constructor(
        authManager: AuthManager,
        http: Http,
        routePath: string,
        router: Router,
        notificationService: NotificationsService
    ) {
        this._apiTools = new ApiTools(routePath, authManager, http, notificationService, router);
        this._authManager = authManager;
    }

    public get apiTools(): ApiTools {
        return this._apiTools;
    }

    public get authManager(): AuthManager {
        return this._authManager;
    }

    public getMany(skip: number, limit: number): Observable<T> {
        let routePartial: string = `${skip}/${limit}`;
        return this._apiTools.get(routePartial).map(
                (body: any) => {
                    if (body && body.success) {
                        return body.message;
                    }
                    else {
                        return null;
                    }
                },
                (error: string) => {
                    return error;
                }
            );
    }
    public getOne(id: number): Observable<T> {
        let routePartial: string = `${id}`;
        return this._apiTools.get(routePartial).map(
                (body: any) => {
                    if (body && body.success) {
                        return body.message;
                    }
                    else {
                        return null;
                    }
                },
                (error: string) => {
                    return error;
                }
            );
    }
    public create(createObject: T): Observable<T> {
        let routePartial: string = ``;
        return this.mapHttpResponse(
            this._apiTools.post(routePartial, createObject)
        );
    }
    public edit(editObject: T): Observable<T> {
        let routePartial: string = ``;
        return this.mapHttpResponse(
            this._apiTools.put(routePartial, editObject)
        );
    }

    public mapHttpResponse(httpObservable: Observable<any>): Observable<any> {
        return httpObservable.map(
            (body: any) => {
                if (body.success) {
                    let message = body && body.message;
                    if (isNaN(message) && !message) {
                        message = false;
                    }
                    return message;
                }
                else {
                    return false;
                }
            },
            (error: string) => {
                return error;
            }
        );
    }
}