import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { AuthPagesGuard } from './authGuard/AuthPagesGuard';
import { PublicPagesGuard } from './authGuard/PublicPagesGuard';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
// import { PricingComponent } from './pages/pricing/pricing.component';

const routes: Routes = [
    // {
    //     path: "pricing",
    //     component: PricingComponent
    // },
    {
        path: "",
        canActivateChild: [PublicPagesGuard],
        children: [
            {
                path: "",
                redirectTo: "login",
                pathMatch: "full"
            },
            {
                path: "login",
                component: LoginComponent
            },
            {
                path: "register",
                component: RegisterComponent
            },
        ]
    },
    {
        path: "",
        canActivateChild: [AuthPagesGuard],
        children: [
            {
                path: "",
                redirectTo: "dashboard",
                pathMatch: "full"
            },
            {
                path: "dashboard",
                component: DashboardComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
