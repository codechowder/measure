export enum ProjectSelectionType {
    NoneSelected = 0,
    Settings = 1,
    AddRun = 2,
    ChartAverage = 3,
    ChartByFile = 4,
    ChartCautious = 5,
    ChartSevere = 6,
    ExportCSV = 7,
    Delete = 8
}