export class ChartColorConstants {
    // Green: Material Color Green 500 (#4CAF50)
    public static readonly GREEN_BG: string = "rgba(76,175,80,.5)";
    public static readonly GREEN_BORDER: string = "rgba(76,175,80,1)";
    // Red: Material Color Red 500 (#F44336)
    public static readonly RED_BG: string = "rgba(244,67,54,.5)";
    public static readonly RED_BORDER: string = "rgba(244,67,54,1)";
    // Yellow: Material Color Amber 500 (#FFC107)
    public static readonly YELLOW_BG: string = "rgba(255,193,7,.5)";
    public static readonly YELLOW_BORDER: string = "rgba(255,193,7,1)";

    // Source Types
    public static readonly VB_BG: string = "rgba(144,164,174.5)";
    public static readonly VB_BORDER: string = "rgba(144,164,174,1)";
    public static readonly CSHARP_BG: string = "rgba(129,55,135,.5)";
    public static readonly CSHARP_BORDER: string = "rgba(129,55,135,1)";
    public static readonly JAVASCRIPT_BG: string = "rgba(247,223,30,.5)";
    public static readonly JAVASCRIPT_BORDER: string = "rgba(247,223,30,1)";
    public static readonly JSX_BG: string = "rgba(83,193,222,.5)";
    public static readonly JSX_BORDER: string = "rgba(83,193,222,1)";
    public static readonly TSX_BG: string = "rgba(32,176,255,.5)";
    public static readonly TSX_BORDER: string = "rgba(32,176,255,1)";
    public static readonly TYPESCRIPT_BG: string = "rgba(0,111,197,.5)";
    public static readonly TYPESCRIPT_BORDER: string = "rgba(0,111,197,1)";
    public static readonly JAVA_BG: string = "rgba(225, 31, 34, .5)";
    public static readonly JAVA_BORDER: string = "rgba(225, 31, 34, 1)";
}