import { IChartDataArray } from "app/chartProcessing/models/IChartDataArray";

export interface IChartProcessedData {
    wmcDataArr: IChartDataArray;
    lcomDataArr: IChartDataArray;
    dccDataArr: IChartDataArray;
    slocDataArr: IChartDataArray;
}