export interface IChartDataArray {
    [key: number]: {
        [key: string]: number[]
    };
}