export interface IChartBottomData {
    current: number;
    average: number;
    delta: number;
}