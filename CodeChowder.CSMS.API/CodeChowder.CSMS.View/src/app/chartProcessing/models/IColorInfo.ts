export interface IColorInfo {
    color: string;
    border: string;
}