export interface IAverageDataArray {
    [key: number]: number[];
}