export interface IMetricKvp {
    [key: string]: number[];
}