import { IChartBottomData } from './IChartBottomData';

export interface IChartPageData {
    chart: Chart;
    chartBottomData: IChartBottomData;
}