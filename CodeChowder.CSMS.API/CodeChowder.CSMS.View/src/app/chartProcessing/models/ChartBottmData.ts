import { IChartBottomData } from "app/chartProcessing/models/IChartBottomData";

export class ChartBottomData implements IChartBottomData {
    public current: number;
    public average: number;
    public delta: number;
    
    public constructor(chartData?: IChartBottomData) {
        this.current = chartData && chartData.current || 0;
        this.average = chartData && chartData.average || 0;
        this.delta = chartData && chartData.delta || 0;
    }
}