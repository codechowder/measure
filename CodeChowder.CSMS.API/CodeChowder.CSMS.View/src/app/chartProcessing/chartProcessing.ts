import { DateUtils } from '../utilities/DateUtils';
import { IProjectMetrics } from '../models/interfaces/IProjectMetrics';
import { IChartPageData } from "./models/IChartPageData";
import { IAverageDataArray } from "app/chartProcessing/models/IAverageDataArray";
import { IChartDataArray } from "app/chartProcessing/models/IChartDataArray";
import { ChartDataSets, ChartData, ChartOptions } from "chart.js";
import { ElementRef } from "@angular/core";
import * as Chart from 'chart.js';
import { SourceCodeType } from "app/models/enums/SourceCodeType";
import { MetricSeverityType } from "app/models/enums/MetricSeverityType";
import { ChartColorConstants } from "app/constants/clientConstants/ChartColorConstants";
import { IMetricSeverity } from "app/models/interfaces/IMetricSeverity";
import { MetricResult } from "app/models/MetricResult";
import { IMetricResult } from "app/models/interfaces/IMetricResult";
import { ProjectMetrics } from "app/models/ProjectMetrics";
import { IProject } from "app/models/interfaces/IProject";
import { IMetricKvp } from "app/chartProcessing/models/IMetricKvp";
import { Select2OptionData } from "ng2-select2/ng2-select2";
import { IColorInfo } from "app/chartProcessing/models/IColorInfo";
import { IChartProcessedData } from "app/chartProcessing/models/IChartProcessedData";
import { IChartBottomData } from "app/chartProcessing/models/IChartBottomData";
import { ChartBottomData } from "app/chartProcessing/models/ChartBottmData";

export class chartProcessing {

    private static _chartType = "line";
    private static _lineFill = false;
    private static _chartOptions: ChartOptions = {
        responsive: true,
        title: {
            display: true,
            text: "Chart.js Line Chart"
        },
        tooltips: {
            mode: "index"
        },
        hover: {
            mode: "nearest"
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: "Runs"
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: "Value"
                }
            }]
        }
    }

    /**
     * Gets the chart labels
     * @param numberOfRuns the number of runs (size of project metrics)
     */
    public static GetChartLabels(runs: IProjectMetrics[]): string[] {
        let retValues: string[] = [];

        for (let run of runs) {
            let formattedDate = DateUtils.formatDateTime(run.created);
            let label = `${formattedDate} - ${run.name}`;
            retValues.push(label);
        }

        return retValues;
    }

    /**
     * Creates/Updates the average-type chart given the metric
     * @param dataArray the data array to process
     * @param chartLabels the labels for the chart
     * @param title the title for the chart
     * @param severityTypes the metric severities from the project
     * @param metricName the metric name
     * @param chart the chart to update
     */
    public static CreateAverageChartData(dataArray: IChartDataArray,
        chartLabels: string[],
        title: string,
        severityTypes: IMetricSeverity[],
        metricName: string,
        chart: ElementRef
    ): void {
        let runs: { [key: string]: number[] } = {};
        let chartDataSets: any[] = [];

        for (let run in dataArray) {
            for (let source in dataArray[run]) {
                if (this.hasRunData(dataArray, source)) {
                    if (!runs[source]) {
                        runs[source] = [];
                    }
                    runs[source].push(...dataArray[run][source])
                }
            }
        }

        for (let source in runs) {
            let count = runs[source].length;
            let lastElement: number = runs[source][count - 1];
            let colorInfo = this.getLanguageColor(parseInt(source));
            let projectRunAvg: ChartDataSets = {
                label: `${SourceCodeType[source].toString()}`,
                fill: this._lineFill,
                backgroundColor: colorInfo.color,
                borderColor: colorInfo.border,
                data: runs[source]
            }
            chartDataSets.push(projectRunAvg);
        }

        let chartData: ChartData = {
            labels: chartLabels,
            datasets: chartDataSets
        };


        this._chartOptions.title.text = title;

        let wmcCtx = (<HTMLCanvasElement>chart.nativeElement).getContext("2d");
        new Chart(wmcCtx, {
            type: this._chartType,
            data: chartData,
            options: this._chartOptions
        });
    }

    /**
     * Creates/Updates a chart based off file
     * @param dataArray the data to process
     * @param chartLabels the labels for the chart
     * @param selectedFileKey the selected file to process
     * @param title the title of the chart
     * @param color the colors for the points
     * @param background the background color for the points
     * @param canvasRef the chart to update
     * @param destroyChart optional chart to destroy before rendering a new one.
     * @param severityTypes optional parameter for the project severity types
     * @param metricName optional parameter for the metric name
     */
    public static CreateChartData(
        dataArray: IChartDataArray,
        chartLabels: string[],
        selectedFileKey: string,
        title: string,
        color: IColorInfo,
        canvasRef: ElementRef,
        destroyChart?: Chart,
        severityTypes?: IMetricSeverity[],
        metricName?: string,
        fileSourceType?: SourceCodeType,
    ): IChartPageData {
        let chartBottomData: IChartBottomData = new ChartBottomData();

        let runs: { [key: string]: number[] } = {};
        let chartDataSets: any[] = [];

        for (let parentKey in dataArray) {
            for (let childKey in dataArray[parentKey]) {
                if (childKey.indexOf(selectedFileKey) >= 0) {
                    let addRun: boolean = false;
                    //If severity types is passed in, we are looking at all files so show even zeros
                    if (severityTypes) {
                        addRun = true;
                    }
                    else {
                        addRun = this.hasRunData(dataArray, childKey);
                    }
                    if (addRun) {
                        if (!runs[childKey]) {
                            runs[childKey] = [];
                        }
                        if(runs[childKey].length === 0 || runs[childKey].length < Object.keys(dataArray).length - 1) {
                            let currentIndex = parseInt(parentKey);

                            for(let i = 0; i < currentIndex; i++) {
                                if(!dataArray[i][childKey]) {
                                    runs[childKey].push(0);
                                }
                            }
                        }
                        runs[childKey].push(...dataArray[parentKey][childKey]);
                    }
                }
            }
        }

        for (let key in runs) {
            if (severityTypes) {
                let count = runs[key].length;
                let lastElement: number = runs[key][count - 1];
                color = this.getSeverityColor(severityTypes, metricName, fileSourceType, lastElement);
            }
            let projectRunAvg: ChartDataSets = {
                label: `${key}`,
                fill: this._lineFill,
                backgroundColor: color.color,
                borderColor: color.border,
                data: runs[key]
            }
            chartDataSets.push(projectRunAvg);
        }

        let chartData: ChartData = {
            labels: chartLabels,
            datasets: chartDataSets
        };

        this._chartOptions.title.text = title;

        let lastElement = chartDataSets[chartDataSets.length - 1];
        if (lastElement.data.length > 0) {
            chartBottomData.current = lastElement.data[lastElement.data.length - 1];

            let avg: number = 0;
            lastElement.data.forEach(d => {
                avg += d;
            });
            chartBottomData.average = Number((Math.round(avg / lastElement.data.length)).toFixed(2));

            chartBottomData.delta = <number><any>"N/A";

            if (lastElement.data.length !== 1) {
                //ROC = {(current value / previous value) - 1} x 100.
                let previousFromLast = lastElement.data[lastElement.data.length - 2];
                if(previousFromLast !== 0) {
                    chartBottomData.delta = Number((((chartBottomData.current / previousFromLast) * 100.0) - 100.0).toFixed(2));
                }
            }

        }

        let canvas = <HTMLCanvasElement>canvasRef.nativeElement;
        let chartCtx = canvas.getContext("2d");
        if (destroyChart) {
            try {
                destroyChart.destroy();
            }
            catch (e) {
            }
        }
        let chart = new Chart(chartCtx, {
            type: this._chartType,
            data: chartData,
            options: this._chartOptions
        });

        return {
            chart: chart,
            chartBottomData: chartBottomData
        };
    }

    /**
     * sets up the chart data for processing
     * @param project the project to process
     * @param severityCheck the severity to check against (Acceptable shows all)
     * @param wmcDataArr wmc data array
     * @param lcomDataArr lcom data array
     * @param dccDataArr dcc data array
     * @param slocDataArr sloc data array
     * @param wmcFiles wmc file options
     * @param lcomFiles lcom file options
     * @param dccFiles dcc file options
     * @param slocFiles sloc file options
     */
    public static ProcessChartData(
        project: IProject,
        severityCheck: MetricSeverityType,
        wmcDataArr: IChartDataArray,
        lcomDataArr: IChartDataArray,
        dccDataArr: IChartDataArray,
        slocDataArr: IChartDataArray,
        wmcFiles: Select2OptionData[],
        lcomFiles: Select2OptionData[],
        dccFiles: Select2OptionData[],
        slocFiles: Select2OptionData[]
    ): IChartProcessedData {
        let metricResultsByFile: { [key: string]: IMetricResult[] } = {};

        wmcDataArr = {};
        lcomDataArr = {};
        dccDataArr = {};
        slocDataArr = {};


        project.projectMetrics.forEach((projectMetrics: ProjectMetrics, index: number) => {

            wmcDataArr[index] = {};
            lcomDataArr[index] = {};
            dccDataArr[index] = {};
            slocDataArr[index] = {};

            metricResultsByFile = {};

            projectMetrics.metricResults.sort((a: IMetricResult, b: IMetricResult) => {
                if (a.metricName < b.metricName)
                    return -1;
                if (a.metricName > b.metricName)
                    return 1;
                return 0;
            }).forEach((m: MetricResult) => {

                if (!metricResultsByFile[m.location]) {
                    metricResultsByFile[m.location] = [];
                }

                metricResultsByFile[m.location].push(m);
            });

            for (let key in metricResultsByFile) {

                let metricResults: IMetricResult[] = metricResultsByFile[key];

                let fileSourceType: SourceCodeType = metricResultsByFile[key][0].sourceType;

                let wmcValues: IMetricKvp = {};
                let lcomValues: IMetricKvp = {};
                let dccValues: IMetricKvp = {};
                let slocValues: IMetricKvp = {};

                metricResults.forEach((m: IMetricResult) => {
                    if (m.metricName === "WMC") {
                        if (!wmcValues[m.objectName]) {
                            wmcValues[m.objectName] = [];
                        }
                        wmcValues[m.objectName].push(m.value);
                    }
                    else if (m.metricName === "LCOM") {
                        if (!lcomValues[m.objectName]) {
                            lcomValues[m.objectName] = [];
                        }
                        lcomValues[m.objectName].push(m.value);
                    }
                    if (m.metricName === "DCC") {
                        if (!dccValues[m.objectName]) {
                            dccValues[m.objectName] = [];
                        }
                        dccValues[m.objectName].push(m.value);
                    }
                    if (m.metricName === "SLOC") {
                        if (!slocValues[m.objectName]) {
                            slocValues[m.objectName] = [];
                        }
                        slocValues[m.objectName].push(m.value);
                    }
                });

                wmcDataArr = this.processRankedData(wmcValues, project.severityTypes, "WMC", fileSourceType, key, index, severityCheck, wmcFiles, wmcDataArr);

                lcomDataArr = this.processRankedData(lcomValues, project.severityTypes, "LCOM", fileSourceType, key, index, severityCheck, lcomFiles, lcomDataArr);

                dccDataArr = this.processRankedData(dccValues, project.severityTypes, "DCC", fileSourceType, key, index, severityCheck, dccFiles, dccDataArr);

                slocDataArr = this.processRankedData(slocValues, project.severityTypes, "SLOC", fileSourceType, key, index, severityCheck, slocFiles, slocDataArr);

            }
        });

        return {
            wmcDataArr: wmcDataArr,
            lcomDataArr: lcomDataArr,
            dccDataArr: dccDataArr,
            slocDataArr: slocDataArr
        }
    }

    private static processRankedData(
        dataValues: IMetricKvp,
        severityTypes: IMetricSeverity[],
        metricName: string,
        fileSourceType: SourceCodeType,
        location: string,
        index: number,
        severityCheck: MetricSeverityType,
        options: Select2OptionData[],
        data: IChartDataArray
    ): IChartDataArray {
        if (options && options.length === 0) {
            options.unshift({
                id: "-1",
                text: ""
            });
        }

        for (let objectName in dataValues) {
            let lastItem = dataValues[objectName][dataValues[objectName].length - 1];
            if (severityCheck === MetricSeverityType.Acceptable ||
                (severityCheck === MetricSeverityType.Cautious && (lastItem >= this.findSeverityValue(severityTypes, metricName, fileSourceType, MetricSeverityType.Acceptable)
                    && lastItem < this.findSeverityValue(severityTypes, metricName, fileSourceType, MetricSeverityType.Cautious))) ||
                (severityCheck === MetricSeverityType.Severe && lastItem >= this.findSeverityValue(severityTypes, metricName, fileSourceType, MetricSeverityType.Cautious))) {

                let fileKey = `${location} - ${objectName}`;

                let optionIndex: number = options.findIndex((f: Select2OptionData) => {
                    return f.id === location;
                });

                if (optionIndex < 0) {

                    let wmcFile: Select2OptionData = {
                        id: location,
                        text: location
                    }
                    options.push(wmcFile);
                }

                if (!data[index][fileKey]) {
                    data[index][fileKey] = [];
                }

                data[index][fileKey] = dataValues[objectName];
            }
        }

        return data;
    }

    private static findSeverityValue(severityTypes: IMetricSeverity[], metricName: string, sourceCodeType: SourceCodeType, severityType: MetricSeverityType): number {
        let retValue: number = -1;
        if (severityTypes) {
            let index: number = severityTypes.findIndex((ms: IMetricSeverity) => {
                return ms.metricName === metricName && ms.severityType === severityType && ms.sourceCode === sourceCodeType;
            });

            if (index >= 0) {
                retValue = severityTypes[index].value;
            }
        }

        return retValue;
    }

    private static getLanguageColor(source: SourceCodeType): IColorInfo {
        let background: string = ChartColorConstants.GREEN_BG;
        let border: string = ChartColorConstants.GREEN_BORDER;
        switch (source) {
            case SourceCodeType.CSharp:
                background = ChartColorConstants.CSHARP_BG;
                border = ChartColorConstants.CSHARP_BORDER;
                break;
            case SourceCodeType.TypeScript:
                background = ChartColorConstants.TYPESCRIPT_BG;
                border = ChartColorConstants.TYPESCRIPT_BORDER;
                break;
            case SourceCodeType.TypeScriptReact:
                background = ChartColorConstants.TSX_BG;
                border = ChartColorConstants.TSX_BORDER;
                break;
            case SourceCodeType.JavaScript:
                background = ChartColorConstants.JAVASCRIPT_BG;
                border = ChartColorConstants.JAVASCRIPT_BORDER;
                break;
            case SourceCodeType.JavaScriptReact:
                background = ChartColorConstants.JSX_BG;
                border = ChartColorConstants.JSX_BORDER;
                break;
            case SourceCodeType.VisualBasic:
                background = ChartColorConstants.VB_BG;
                border = ChartColorConstants.VB_BORDER;
                break;
            case SourceCodeType.Java:
                background = ChartColorConstants.JAVA_BG;
                border = ChartColorConstants.JAVA_BORDER;
            default:
                break;
        }
        return {
            color: background,
            border: border
        };
    }

    private static getSeverityColor(severityTypes: IMetricSeverity[], metricType: string, sourceCodeType: SourceCodeType, value: number): IColorInfo {
        let severityColor = ChartColorConstants.GREEN_BG;
        let severityBorder = ChartColorConstants.GREEN_BORDER;
        let acceptableValue = this.findSeverityValue(severityTypes, metricType, sourceCodeType, MetricSeverityType.Acceptable);
        let cautiousValue = this.findSeverityValue(severityTypes, metricType, sourceCodeType, MetricSeverityType.Cautious);
        let severeValue = this.findSeverityValue(severityTypes, metricType, sourceCodeType, MetricSeverityType.Severe);

        if (acceptableValue >= 0 && cautiousValue >= 0) {
            if (value > acceptableValue && value <= cautiousValue) {
                severityColor = ChartColorConstants.YELLOW_BG;
                severityBorder = ChartColorConstants.YELLOW_BORDER;
            }
        }
        if (cautiousValue >= 0 && severeValue >= 0) {
            if (value > cautiousValue && value <= severeValue) {
                severityColor = ChartColorConstants.RED_BG;
                severityBorder = ChartColorConstants.RED_BORDER;
            }
        }

        return {
            color: severityColor,
            border: severityBorder
        };
    }

    private static hasRunData(dataArray: IChartDataArray, source: string): boolean {
        let sum: number = 0;
        for (let key in dataArray) {
            if (dataArray[key] && dataArray[key][source]) {
                sum += dataArray[key][source].reduce((a, b) => a + b, 0);
            }
        }
        return sum > 0;
    }
}
