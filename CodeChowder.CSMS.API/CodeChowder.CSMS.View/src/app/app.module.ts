import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'angular2-cookie/core';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthPagesGuard } from './authGuard/AuthPagesGuard';
import { PublicPagesGuard } from './authGuard/PublicPagesGuard';
import { CreateProjectComponent } from './components/create-project/create-project.component';
import { LoadingCircleComponent } from './components/loading-circle/loading-circle.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ProjectSettingsComponent } from './components/project-settings/project-settings.component';
import { UploadFilesComponent } from './components/upload-files/upload-files.component';
import { AuthManager } from './config/AuthManager';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ChartComponent } from './components/chart/chart.component';
import { RegisterComponent } from './pages/register/register.component';

import { FileDropDirective, FileSelectDirective } from 'ng2-file-upload';

import { Select2Module } from 'ng2-select2';
import { CautiousGraphsComponent } from './components/cautious-graphs/cautious-graphs.component';
import { SevereGraphsComponent } from './components/severe-graphs/severe-graphs.component';
import { ByFileGraphsComponent } from './components/by-file-graphs/by-file-graphs.component';
import { DashboardProjectComponent } from './components/dashboard-project/dashboard-project.component';
import { MetricSettingsComponent } from './components/metric-settings/metric-settings.component';
import { SourceMetricSettingsComponent } from './components/source-metric-settings/source-metric-settings.component';
import { LeftMenuComponent } from './components/left-menu-component/left-menu-component';
import { RightMenuComponent } from './components/right-menu-component/right-menu-component';
import { RightMenuTopComponent } from './components/right-menu-top/right-menu-top.component';
import { RightMenuMiddleComponent } from './components/right-menu-middle/right-menu-middle.component';
import { RightMenuBottomComponent } from './components/right-menu-bottom/right-menu-bottom.component';
import { MainSectionComponent } from './components/main-section/main-section.component';
import { ChartBottomComponent } from './components/chart-bottom/chart-bottom.component';
import { ExportMetricsComponent } from './components/export-metrics/export-metrics.component';
import { ShareProjectModalComponent } from './components/share-project-modal/share-project-modal.component';
import { DeleteMetricsComponent } from './components/delete-metrics/delete-metrics.component';
import { DeleteProjectComponent } from './components/delete-project/delete-project.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { LicenseDataComponent } from './components/license-data/license-data.component';
import { ProjectStatisticsComponent } from './components/project-statistics/project-statistics.component';
import { ProjectRemainingMetricsComponent } from './components/project-remaining-metrics/project-remaining-metrics.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    LoadingCircleComponent,
    ChartComponent,
    DashboardComponent,
    UploadFilesComponent,
    FileDropDirective, 
    FileSelectDirective, 
    CreateProjectComponent, 
    ProjectSettingsComponent,
    CautiousGraphsComponent,
    SevereGraphsComponent,
    ByFileGraphsComponent,
    DashboardProjectComponent,
    MetricSettingsComponent,
    SourceMetricSettingsComponent,
    LeftMenuComponent,
    RightMenuComponent,
    RightMenuTopComponent,
    RightMenuMiddleComponent,
    RightMenuBottomComponent,
    MainSectionComponent,
    ChartBottomComponent,
    ExportMetricsComponent,
    ShareProjectModalComponent,
    DeleteMetricsComponent,
    DeleteProjectComponent,
    UserProfileComponent,
    LicenseDataComponent,
    ProjectStatisticsComponent,
    ProjectRemainingMetricsComponent
  ],
  imports: [
    Select2Module,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    SimpleNotificationsModule.forRoot()
  ],
  providers: [
    AuthManager,
    CookieService,
    PublicPagesGuard,
    AuthPagesGuard,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
