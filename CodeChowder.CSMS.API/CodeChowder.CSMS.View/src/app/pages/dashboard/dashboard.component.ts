import { ISharedProject } from '../../models/interfaces/ISharedProject';
import { ProjectSelection } from "../../models/clientModels/ProjectSelection";
import { AuthManager } from "../../config/AuthManager";
import { ProjectSelectionType } from "../../enums/clientEnums/ProjectSelectionType";
import { IProjectSelection } from "../../models/clientModels/interfaces/IProjectSelection";
import { IProject } from "../../models/interfaces/IProject";
import { Project } from "../../models/Project";
import { ProjectService } from "../../services/ProjectService";
import { IProjectService } from "../../services/interfaces/IProjectService";
import { ApplicationUserService } from "../../services/ApplicationUserService";
import { IApplicationUserService } from "../../services/interfaces/IApplicationUserService";

import { trigger, transition, style, animate } from "@angular/animations";
import { Component, OnInit } from "@angular/core";
import { NotificationsService } from "angular2-notifications";
import { Observable } from "rxjs";

@Component({
    selector: "app-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.scss"],
    animations: [
        trigger(
            "showMenu",
            [
                transition(
                    ":enter", [
                        style({ transform: "translateX(-100%)", opacity: 0 }),
                        animate("500ms", style({ transform: "translateX(0)", "opacity": 1 }))
                    ]
                ),
                transition(
                    ":leave", [
                        style({ transform: "translateX(0)", "opacity": 1 }),
                        animate("500ms", style({ transform: "translateX(-100%)", "opacity": 0 }))
                    ]
                )
            ]
        )
    ],
    providers: [ApplicationUserService, ProjectService]
})
export class DashboardComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;
    private readonly _userService: IApplicationUserService;

    private _loading: boolean;
    private _projects: IProject[];
    private _projectsLoading: boolean;
    private _projectSelectionType: ProjectSelectionType;
    private _selectedOwnProject: IProject;
    private _selectedProject: IProject;
    private _selectedSharedProject: IProject;
    private _sharedProjects: IProject[];
    private _sharedProjectsLoading: boolean;
    private _showLicense: boolean;
    private _showProfile: boolean;

    constructor(
        authManager: AuthManager,
        notificationService: NotificationsService,
        projectService: ProjectService,
        userService: ApplicationUserService
    ) {
        this._authManager = authManager;
        this._loading = false;
        this._notificationService = notificationService;
        this._projects = [];
        this._projectService = projectService;
        this._projectsLoading = false;
        this._sharedProjects = [];
        this._sharedProjectsLoading = false;
        this._projectSelectionType = ProjectSelectionType.NoneSelected;
        this._selectedProject = null;
        this._selectedOwnProject = null;
        this._selectedSharedProject = null;
        this._showLicense = true;
        this._showProfile = false;
        this._userService = userService;
    }

    public ngOnInit(): void {
        this.getProjects();
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get projects(): IProject[] {
        return this._projects;
    }

    public get projectsLoading(): boolean {
        return this._projectsLoading || this._sharedProjectsLoading;
    }

    public get sharedProjects(): IProject[] {
        return this._sharedProjects;
    }

    public get selectedProject(): IProject {
        return this._selectedProject;
    }

    public get selectedOwnProject(): IProject {
        return this._selectedOwnProject;
    }

    public set selectedProject(value: IProject) {
        this._selectedProject = value;
    }

    public get selectedSharedProject(): IProject {
        return this._selectedSharedProject;
    }

    public get showAddRun(): boolean {
        return this._projectSelectionType === ProjectSelectionType.AddRun;
    }

    public get showChartAverage(): boolean {
        return this._projectSelectionType === ProjectSelectionType.ChartAverage;
    }

    public get showChartByFile(): boolean {
        return this._projectSelectionType === ProjectSelectionType.ChartByFile;
    }

    public get showChartCautious(): boolean {
        return this._projectSelectionType === ProjectSelectionType.ChartCautious;
    }

    public get showChartSevere(): boolean {
        return this._projectSelectionType === ProjectSelectionType.ChartSevere;
    }

    public get showDelete(): boolean {
        return this._projectSelectionType === ProjectSelectionType.Delete;
    }

    public get showExport(): boolean {
        return this._projectSelectionType === ProjectSelectionType.ExportCSV;
    }

    public get showLicense(): boolean {
        return this._showLicense;
    }

    public get showProfile(): boolean {
        return this._showProfile;
    }

    public get showProjectStatistics(): boolean {
        return this._projectSelectionType === ProjectSelectionType.NoneSelected;
    }

    public get showMainProfile(): boolean {
        return this._showLicense || this._showProfile;
    }

    public get showMainProject(): boolean {
        return !(this._showLicense || this._showProfile) && !!this._selectedProject;
    }

    public get showSettings(): boolean {
        return this._projectSelectionType === ProjectSelectionType.Settings;
    }

    public onLicenseSelect(): void {
        this._projectSelectionType = ProjectSelectionType.NoneSelected;
        this._showProfile = false;
        this._showLicense = true;
    }

    public onProfileSelect(): void {
        this._projectSelectionType = ProjectSelectionType.NoneSelected;
        this._showLicense = false;
        this._showProfile = true;
    }

    public onProjectAdded(event: IProject[]): void {
        this._projects = event;
    }

    public onProjectDelete(): void {
        let index = this._projects.indexOf(this._selectedProject);
        this._projects.splice(index, 1);
        this._selectedProject = null;
    }

    public onProjectOptionSelect(selection: ProjectSelectionType): void {
        if (this._selectedProject) {
            this.getSelectedProject(selection);
        }
    }

    public onProjectSelect(project: IProject): void {
        this._selectedSharedProject = null;
        this._selectedOwnProject = project;
        this.setSelectedProject(project);
    }

    public onSharedProjectSelect(project: IProject) {
        this._selectedOwnProject = null;
        this._selectedSharedProject = project;
        this.setSelectedProject(project);
    }

    private getOwnProjects(): void {
        this._projectsLoading = true;
        this._userService.getOne(this._authManager.activeUser.id).finally(() => {
            this._projectsLoading = false;
        }).subscribe(user => {
            if (user) {
                this._authManager.activeUser = user;

                //Get the projects
                this._projects = this._authManager.activeUser.projects;

                return this._userService.getSharedProjects(user.id);
            }
            else {
                Observable.throw("Could not get projects.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }

    private getProjects(): void {
        if (this._authManager.activeUser) {
            this.getOwnProjects();
            this.getSharedProjects();
        }
    }

    private getSelectedProject(selection: ProjectSelectionType): void {
        this._showProfile = false;
        let serviceCall: () => Observable<IProject> = null;

        if (selection === ProjectSelectionType.Settings) {
            if (!(
                this._selectedProject.ignoreFiles && this._selectedProject.severityTypes &&
                this._selectedProject.severityTypes.length
            )) {
                serviceCall = () => this._projectService.getProjectWithSettings(this._selectedProject.id);
            }
        }
        else if (selection !== ProjectSelectionType.ExportCSV && selection !== ProjectSelectionType.AddRun) {
            if (!(
                this._selectedProject.projectMetrics && this._selectedProject.projectMetrics.length &&
                this._selectedProject.severityTypes && this._selectedProject.severityTypes.length
            )) {
                serviceCall = () => this._projectService.getProjectWithMetrics(this._selectedProject.id);
            }
        }

        if (serviceCall) {
            let sub: (p: IProject) => void = p => {
                if (p) {
                    this._selectedProject = p;
                    this._projectSelectionType = selection;
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Could not get project info.");
                }
            };
            let err: (error: string) => void = e => {
                this._notificationService.error("Oops! Something went wrong.", e);
            };
            this._loading = true;
            serviceCall().finally(() => {
                this._loading = false;
            }).subscribe(sub, err);
        }
        else {
            this._projectSelectionType = selection;
        }
    }

    private getSharedProjects(): void {
        this._sharedProjectsLoading = true;
        this._userService.getSharedProjects(this._authManager.activeUser.id).finally(() => {
            this._sharedProjectsLoading = false;
        }).subscribe(sharedProjects => {
            if (sharedProjects) {
                this._sharedProjects = sharedProjects.filter(sp => !!sp.project).map(sp => sp.project);
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not get shared projects.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }

    private setSelectedProject(project: IProject): void {
        this._showLicense = false;
        this._showProfile = false;
        this._selectedProject = project;
        this._projectSelectionType = ProjectSelectionType.NoneSelected;
    }
}
