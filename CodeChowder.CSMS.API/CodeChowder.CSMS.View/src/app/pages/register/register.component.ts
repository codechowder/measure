import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthManager } from '../../config/AuthManager';
import { ApplicationUser } from '../../models/ApplicationUser';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { SocialMediaTypes } from '../../models/SocialMediaTypes';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { AuthenticationService } from '../../services/AuthenticationService';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';
import { IAuthenticationService } from '../../services/interfaces/IAuthenticationService';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { SocialMediaLogin } from "../../services/SocialMediaLogin";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    providers: [AuthenticationService, ApplicationUserService]
})
export class RegisterComponent implements OnInit, AfterViewInit {
    SocialMediaTypes = SocialMediaTypes;
    private _account: IApplicationUser;
    private _authService: IAuthenticationService;
    private _applicationUserService: IApplicationUserService;
    private _authManager: AuthManager;
    private _loading: boolean;
    private _notificationService: NotificationsService;
    private _passwordConfirm: string;
    private _router: Router;
    private _socialMediaLogin: SocialMediaLogin;

    constructor(
        applicationUserService: ApplicationUserService,
        authManager: AuthManager,
        authService: AuthenticationService,
        notificationService: NotificationsService,
        router: Router
    ) {
        this._applicationUserService = applicationUserService;
        this._authManager = authManager;
        this._authService = authService;
        this._passwordConfirm = "";
        this._router = router;
        this._account = new ApplicationUser();
        this._socialMediaLogin = null;
        this._notificationService = notificationService;
        this._loading = false;
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this._socialMediaLogin = new SocialMediaLogin(<AuthenticationService>this._authService,
            <ApplicationUserService>this._applicationUserService,
            this._authManager, this._notificationService, this._router, '/dashboard', true);
    }

    public get account(): IApplicationUser {
        return this._account;
    }

    public set account(value: IApplicationUser) {
        this._account = value;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get passwordConfirm(): string {
        return this._passwordConfirm;
    }

    public set passwordConfirm(value: string) {
        this._passwordConfirm = value;
    }

    public onRegister() {
        if (this._account.password === this._passwordConfirm) {
            this._loading = true;
            this._applicationUserService.create(this._account).finally(() => {
                this._loading = false;
            }).subscribe((user: IApplicationUser) => {
                if (user) {
                    this._notificationService.success("Welcome to Measure!", "Please sign in to continue.");
                    this._router.navigate(["/login"]);
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Please try again.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
        else {
            this._notificationService.error("Passwords do not match.", "Please try again.");
        }
    }

    public socialMediaLogin(socialMediaType: SocialMediaTypes) {
        this._socialMediaLogin.onSocialMediaLogin(socialMediaType);
    }
}
