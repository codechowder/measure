import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthManager } from '../../config/AuthManager';
import { ApplicationUser } from '../../models/ApplicationUser';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { SocialMediaTypes } from '../../models/SocialMediaTypes';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { AuthenticationService } from '../../services/AuthenticationService';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';
import { IAuthenticationService } from '../../services/interfaces/IAuthenticationService';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { SocialMediaLogin } from "../../services/SocialMediaLogin";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [AuthenticationService, ApplicationUserService]
})
export class LoginComponent implements OnInit, AfterViewInit {
    SocialMediaTypes = SocialMediaTypes;
    private _account: IApplicationUser;
    private _authService: IAuthenticationService;
    private _applicationUserService: IApplicationUserService;
    private _authManager: AuthManager;
    private _loading: boolean;
    private _notificationService: NotificationsService;
    private _passwordConfirm: string;
    private _router: Router;
    private _socialMediaLogin: SocialMediaLogin;

    constructor(
        applicationUserService: ApplicationUserService,
        authManager: AuthManager,
        authService: AuthenticationService,
        notificationService: NotificationsService,
        router: Router
    ) {
        this._applicationUserService = applicationUserService;
        this._authManager = authManager;
        this._authService = authService;
        this._passwordConfirm = "";
        this._router = router;
        this._account = new ApplicationUser();
        this._socialMediaLogin = null;
        this._notificationService = notificationService;
        this._loading = false;
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this._socialMediaLogin = new SocialMediaLogin(<AuthenticationService>this._authService, 
        <ApplicationUserService>this._applicationUserService, 
        this._authManager, this._notificationService, this._router, '/dashboard');
    }

    public get account(): IApplicationUser {
        return this._account;
    }

    public set account(value: IApplicationUser) {
        this._account = value;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public onLogin() {
        if (this._account.password && this._account.email) {
            this._loading = true;
            this._authService.login(this._account.email, this._account.password).finally(() => {
                this._loading = false;
            }).subscribe((success: boolean) => {
                if (success) {
                    this._router.navigate(["/dashboard"]);
                }
                else {
                    this._notificationService.error("Invalid credentials.", "Please try again.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
        else {
            this._notificationService.error("Missing form data.", "Please try again.");
        }
    }

    public socialMediaLogin(socialMediaType: SocialMediaTypes) {
        this._socialMediaLogin.onSocialMediaLogin(socialMediaType);
    }
}