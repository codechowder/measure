export class DateUtils {
    public static formatDateTime(dateToFormat: Date | string): string {
        // Dates usually come back from the server as strings, so convert to a date.
        dateToFormat = new Date((dateToFormat as Date));

        let year = dateToFormat.getFullYear().toString();
        let month = (dateToFormat.getMonth() + 1).toString();
        if (month.length < 2) {
            month = `0${month}`;
        }
        let date = dateToFormat.getDate().toString();
        if (date.length < 2) {
            date = `0${date}`;
        }
        let hour = dateToFormat.getHours().toString();
        if (hour.length < 2) {
            hour = `0${hour}`;
        }
        let minute = dateToFormat.getMinutes().toString();
        if (minute.length < 2) {
            minute = `0${minute}`;
        }

        return `${year}/${month}/${date} ${hour}:${minute}`;
    }
}