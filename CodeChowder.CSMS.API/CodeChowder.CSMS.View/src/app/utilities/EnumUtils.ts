import { Select2OptionData } from "ng2-select2";

export class EnumUtils {
    public static select2OptionsFromEnum(enumeration: { [key: number]: string }): Select2OptionData[] {
        return Object.keys(enumeration).filter(k => isNaN(<any>k)).map(k => {
            return {
                id: enumeration[k],
                text: k
            };
        });
    }
}