import { ProjectSelectionType } from '../../enums/clientEnums/ProjectSelectionType';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-right-menu-middle',
    templateUrl: './right-menu-middle.component.html',
    styleUrls: ['./right-menu-middle.component.css']
})
export class RightMenuMiddleComponent implements OnInit {
    private _select: EventEmitter<ProjectSelectionType>;

    constructor() {
        this._select = new EventEmitter<ProjectSelectionType>();
    }

    ngOnInit() {
    }

    @Output()
    public get select(): EventEmitter<ProjectSelectionType> {
        return this._select;
    }

    public onAverageClick(): void {
        this._select.emit(ProjectSelectionType.ChartAverage);
    }

    public onByFileClick(): void {
        this._select.emit(ProjectSelectionType.ChartByFile);
    }

    public onCautiousClick(): void {
        this._select.emit(ProjectSelectionType.ChartCautious);
    }

    public onExportClick(): void {
        this._select.emit(ProjectSelectionType.ExportCSV);
    }

    public onSevereClick(): void {
        this._select.emit(ProjectSelectionType.ChartSevere);
    }
}