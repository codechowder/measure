import { AuthManager } from '../../config/AuthManager';

import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
    private _authManager: AuthManager;
    private _router: Router;

    constructor(
        authManager: AuthManager,
        router: Router
    ) {
        this._authManager = authManager;
        this._router = router;
    }

    ngOnInit() {
    }

    public get authenticated(): boolean {
        return this._authManager.authenticated;
    }

    public onLogout(): void {
        this._authManager.destroyAuth();
        this._router.navigate(["/"]);
    }
}
