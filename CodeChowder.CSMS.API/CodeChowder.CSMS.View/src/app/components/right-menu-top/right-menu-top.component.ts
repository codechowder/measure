import { ProjectService } from '../../services/ProjectService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { AuthManager } from '../../config/AuthManager';
import { Project } from '../../models/Project';
import { IProject } from '../../models/interfaces/IProject';
import { Component, Input, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-right-menu-top',
    templateUrl: './right-menu-top.component.html',
    styleUrls: ['./right-menu-top.component.css'],
    providers: [ProjectService]
})
export class RightMenuTopComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;

    private _companyName: string;
    private _loading: boolean;
    private _project: IProject;

    constructor(
        authManager: AuthManager,
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._authManager = authManager;
        this._companyName = "";
        this._loading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
    }

    ngOnInit() {
    }

    public get companyName(): string {
        return this._companyName;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            this._companyName = this._authManager.activeUser.company || "";
        }
    }

    @Input()
    public set sharedProject(value: IProject) {
        if (value) {
            this._project = value;
            this._loading = true;
            this._projectService.getCompanyName(this._project.id).finally(() => {
                this._loading = false;
            }).subscribe(companyName => {
                this._companyName = companyName || "";
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
    }

    // public onImageEdit(): void {
    //     this._notificationService.info("Coming soon!");
    // }
}