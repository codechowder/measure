import { Project } from "../../models/Project";
import { ProjectService } from "../../services/ProjectService";
import { IProjectService } from "../../services/interfaces/IProjectService";
import { IProject } from "../../models/interfaces/IProject";
import { Component, OnInit, EventEmitter, Input, Output } from "@angular/core";
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: "app-delete-project",
    templateUrl: "./delete-project.component.html",
    styleUrls: ["./delete-project.component.scss"],
    providers: [ProjectService]
})
export class DeleteProjectComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectChange: EventEmitter<IProject>;
    private readonly _projectDelete: EventEmitter<void>;
    private readonly _projectService: IProjectService;

    private _loading: boolean;
    private _project: IProject;

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._projectDelete = new EventEmitter<void>();
        this._loading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectChange = new EventEmitter<IProject>();
        this._projectService = projectService;
    }

    public ngOnInit(): void {
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
        }
    }

    @Output()
    public get projectChange(): EventEmitter<IProject> {
        return this._projectChange;
    }

    @Output()
    public get projectDelete(): EventEmitter<void> {
        return this._projectDelete;
    }

    public onDelete(): void {
        this._loading = true;
        this._projectService.delete(this._project.id).finally(() => {
            this._loading = false;
        }).subscribe(result => {
            if (result) {
                this._notificationService.success("Deleted project.");
                this._projectDelete.emit();
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not delete project.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
