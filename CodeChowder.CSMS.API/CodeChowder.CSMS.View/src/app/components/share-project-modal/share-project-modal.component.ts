import { ProjectService } from '../../services/ProjectService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { ApplicationUser } from '../../models/ApplicationUser';
import { Project } from '../../models/Project';
import { IProject } from '../../models/interfaces/IProject';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { AuthManager } from '../../config/AuthManager';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';
import { Component, Input, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-share-project-modal',
    templateUrl: './share-project-modal.component.html',
    styleUrls: ['./share-project-modal.component.scss'],
    providers: [ApplicationUserService, ProjectService]
})
export class ShareProjectModalComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;
    private readonly _userService: IApplicationUserService;

    private _emailSearch: string;
    private _foundUser: IApplicationUser;
    private _loading: boolean;
    private _project: IProject;
    private _sharedUsers: IApplicationUser[];

    constructor(
        authManager: AuthManager,
        notificationService: NotificationsService,
        projectService: ProjectService,
        userService: ApplicationUserService
    ) {
        this._authManager = authManager;
        this._emailSearch = "";
        this._foundUser = null;
        this._loading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
        this._sharedUsers = [];
        this._userService = userService;
    }

    public ngOnInit(): void {
    }

    public get emailSearch(): string {
        return this._emailSearch;
    }

    public set emailSearch(value: string) {
        this._emailSearch = value;
    }

    public get foundUser(): IApplicationUser {
        return this._foundUser;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
        }
    }

    public get sharedUsers(): IApplicationUser[] {
        return this._sharedUsers;
    }

    @Input()
    public set sharedUsers(value: IApplicationUser[]) {
        this._sharedUsers = value;
    }

    public onRemoveShare(user: IApplicationUser): void {
        this._loading = true;
        this._userService.unshareProject(user.id, this._project.id).finally(() => {
            this._loading = false;
        }).subscribe(success => {
            if (success) {
                let index = this._sharedUsers.findIndex(u => u.id === user.id);
                this._sharedUsers.splice(index, 1);
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not unshare project.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }

    public onSearch(): void {
        if (this._emailSearch) {
            this._loading = true;
            this._userService.searchByEmail(this._emailSearch).finally(() => {
                this._loading = false;
            }).subscribe(user => {
                if (user) {
                    this._foundUser = user;
                }
                else {
                    this._notificationService.alert("User not found.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            })
        }
    }

    public onShare(): void {
        if (this._foundUser) {
            this._loading = true;
            this._userService.shareProject(this._foundUser.id, this._project).finally(() => {
                this._loading = false;
            }).subscribe(success => {
                if (success) {
                    this._sharedUsers.push(new ApplicationUser(this._foundUser));
                    this._foundUser = null;
                    this._emailSearch = "";
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Could not share project.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
    }
}
