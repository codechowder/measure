import { Project } from '../../models/Project';
import { IProject } from '../../models/interfaces/IProject';
import { ProjectService } from '../../services/ProjectService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { Component, Input, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-project-remaining-metrics',
    templateUrl: './project-remaining-metrics.component.html',
    styleUrls: ['./project-remaining-metrics.component.scss'],
    providers: [ProjectService]
})
export class ProjectRemainingMetricsComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;

    private _current: number;
    private _loading: boolean;
    private _maxAllowedMetrics: number;
    private _project: IProject;

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._current = 0;
        this._loading = false;
        this._maxAllowedMetrics = 0;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
    }

    public ngOnInit(): void {
    }

    public get current(): number {
        return this._current;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get maxAllowedMetrics(): number {
        return this._maxAllowedMetrics;
    }

    @Input()
    public set maxAllowedMetrics(value: number) {
        this._maxAllowedMetrics = value;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            this.getCurrentMetricsCount();
        }
    }

    private getCurrentMetricsCount(): void {
        this._loading = true;
        this._projectService.getMetricsCount(this._project.id).finally(() => {
            this._loading = false;
        }).subscribe(count => {
            if (!isNaN(parseInt(count as any))) {
                this._current = count;
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not get current metrics.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
