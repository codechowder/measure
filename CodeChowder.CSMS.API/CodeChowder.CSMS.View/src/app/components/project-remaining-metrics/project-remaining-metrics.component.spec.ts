import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRemainingMetricsComponent } from './project-remaining-metrics.component';

describe('ProjectRemainingMetricsComponent', () => {
  let component: ProjectRemainingMetricsComponent;
  let fixture: ComponentFixture<ProjectRemainingMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectRemainingMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRemainingMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
