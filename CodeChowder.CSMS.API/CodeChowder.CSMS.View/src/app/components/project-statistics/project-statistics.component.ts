import { IMetricResult } from '../../models/interfaces/IMetricResult';
import { Project } from '../../models/Project';
import { ProjectService } from '../../services/ProjectService';
import { IProject } from '../../models/interfaces/IProject';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { Component, Input, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-project-statistics',
    templateUrl: './project-statistics.component.html',
    styleUrls: ['./project-statistics.component.scss'],
    providers: [ProjectService]
})
export class ProjectStatisticsComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;

    private _cautiousMetrics: IMetricResult[];
    private _cautiousMetricsLoading: boolean;
    private _project: IProject;
    private _severeMetrics: IMetricResult[];
    private _severeMetricsLoading: boolean;
    private _totalSloc: number;
    private _totalSlocLoading: boolean;

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._cautiousMetrics = [];
        this._cautiousMetricsLoading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
        this._severeMetrics = [];
        this._severeMetricsLoading = false;
        this._totalSloc = 0;
        this._totalSlocLoading = false;
    }

    public ngOnInit(): void {
    }

    public get cautiousMetrics(): IMetricResult[] {
        return this._cautiousMetrics;
    }

    public get cautiousMetricsLoading(): boolean {
        return this._cautiousMetricsLoading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value && this._project.id !== value.id) {
            this._project = value;
            this.getMetrics();
        }
    }

    public get severeMetrics(): IMetricResult[] {
        return this._severeMetrics;
    }

    public get severeMetricsLoading(): boolean {
        return this._severeMetricsLoading;
    }

    public get totalSloc(): number {
        return this._totalSloc;
    }

    public get totalSlocLoading(): boolean {
        return this._totalSlocLoading;
    }

    private getCautious(): void {
        this._cautiousMetricsLoading = true;
        this._projectService.getCautiousMetrics(this._project.id).finally(() => {
            this._cautiousMetricsLoading = false;
        }).subscribe(cm => {
            if (cm) {
                this._cautiousMetrics = cm;
                this._cautiousMetricsLoading = false;
            }
            else {
                this._notificationService.error(
                    "Oops! Something went wrong.", "Could not get cautious metrics."
                );
            }
            return this._projectService.getSevereMetrics(this._project.id);
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
            this._cautiousMetricsLoading = false;
        });
    }

    private getMetrics(): void {
        this.getSloc();
        this.getCautious();
        this.getSevere();
    }

    private getSevere(): void {
        this._severeMetricsLoading = true;
        this._projectService.getSevereMetrics(this._project.id).finally(() => {
            this._severeMetricsLoading = false;
        }).subscribe(sm => {
            if (sm) {
                this._severeMetrics = sm;
                this._severeMetricsLoading = false;
            }
            else {
                this._notificationService.error(
                    "Oops! Something went wrong.", "Could not get severe metrics."
                );
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
            this._severeMetricsLoading = false;
        });
    }

    private getSloc(): void {
        this._totalSlocLoading = true;
        this._projectService.getTotalSloc(this._project.id).finally(() => {
            this._totalSlocLoading = false;
        }).subscribe(count => {
            if (!isNaN(parseInt(count as any))) {
                this._totalSloc = count;
                this._totalSlocLoading = false;
            }
            else {
                this._notificationService.error(
                    "Oops! Something went wrong.", "Could not get total SLOC."
                );
            }
            return this._projectService.getCautiousMetrics(this._project.id);
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
            this._totalSlocLoading = false;
        });
    }
}
