import { AuthManager } from '../../config/AuthManager';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { IProject } from '../../models/interfaces/IProject';
import { Project } from '../../models/Project';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';

import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

declare const UIkit;

@Component({
    selector: 'app-create-project',
    templateUrl: './create-project.component.html',
    styleUrls: ['./create-project.component.scss'],
    providers: [ApplicationUserService]
})
export class CreateProjectComponent implements OnInit {
    private readonly _applicationUserService: IApplicationUserService;
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _router: Router;

    private _project: IProject;
    private _projectAdded: EventEmitter<IProject[]>;
    private _loading: boolean;
    private _modal;

    constructor(applicationUserService: ApplicationUserService,
        notificationService: NotificationsService,
        authManager: AuthManager,
        router: Router) {
        this._applicationUserService = applicationUserService;
        this._notificationService = notificationService;
        this._authManager = authManager;
        this._router = router;
        this._project = new Project();
        this._projectAdded = new EventEmitter<IProject[]>();
        this._loading = false;
    }

    ngOnInit() {
        this._modal = UIkit.modal('#createProject');
    }

    public get project(): IProject {
        return this._project;
    }

    @Output()
    public get projectAdded(): EventEmitter<IProject[]> {
        return this._projectAdded;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public onCreate() {
        this._modal.hide();
        this._loading = true;
        this._applicationUserService.createProject(this._authManager.activeUser.id, this._project).finally(() => {
            this._loading = false;
        }).subscribe((user: IApplicationUser) => {
            if (user) {
                this._authManager.activeUser = user;
                this._notificationService.success("Project created.", `Successfully created ${this._project.name}.`);
                this._projectAdded.emit(user.projects);
            }
            else {
                this._notificationService.warn("Issue Creating Project.", "Failed to create project.");
            }

        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
