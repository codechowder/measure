import { ProjectSelectionType } from '../../enums/clientEnums/ProjectSelectionType';
import { IProject } from '../../models/interfaces/IProject';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-right-menu-component',
    templateUrl: './right-menu-component.html',
    styleUrls: ['./right-menu-component.css']
})
export class RightMenuComponent implements OnInit {
    private readonly _select: EventEmitter<ProjectSelectionType>;

    private _ownProject: IProject;
    private _project: IProject;
    private _sharedProject: IProject;

    /**
     * Default Constructor
     */
    constructor() {
        this._ownProject = null;
        this._project = null;
        this._select = new EventEmitter<ProjectSelectionType>();
        this._sharedProject = null;
    }

    /**
     * On init
     */
    ngOnInit() {
    }

    public get ownProject(): IProject {
        return this._ownProject;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            this._ownProject = value;
            this._sharedProject = null;
        }
    }

    @Output()
    public get select(): EventEmitter<ProjectSelectionType> {
        return this._select;
    }

    public get sharedProject(): IProject {
        return this._sharedProject;
    }

    @Input()
    public set sharedProject(value: IProject) {
        if (value) {
            this._project = value;
            this._sharedProject = value;
            this._ownProject = null;
        }
    }

    public onSelect(selection: ProjectSelectionType) {
        this._select.emit(selection);
    }
}