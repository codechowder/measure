import { IProject } from '../../models/interfaces/IProject';
import { AuthManager } from '../../config/AuthManager';
import { LicenseData } from '../../models/LicenseData';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { ILicenseData } from '../../models/interfaces/ILicenseData';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';
import { Component, Input, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-license-data',
    templateUrl: './license-data.component.html',
    styleUrls: ['./license-data.component.scss'],
    providers: [ApplicationUserService]
})
export class LicenseDataComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _userService: IApplicationUserService;

    private _licenseData: ILicenseData;
    private _licenseDataLoading: boolean;
    private _projectsLoading: boolean;

    constructor(
        authManager: AuthManager,
        notificationService: NotificationsService,
        userService: ApplicationUserService
    ) {
        this._authManager = authManager;
        this._licenseData = new LicenseData();
        this._licenseDataLoading = false;
        this._notificationService = notificationService;
        this._projectsLoading = false;
        this._userService = userService;
    }

    public ngOnInit(): void {
        this.getLicenseData();
    }

    public get licenseData(): ILicenseData {
        return this._licenseData;
    }

    public get licenseDataLoading(): boolean {
        return this._licenseDataLoading;
    }

    public get projects(): IProject[] {
        return this._authManager.activeUser.projects || [];
    }

    public get projectsLoading(): boolean {
        return this._projectsLoading;
    }

    @Input()
    public set projectsLoading(value: boolean) {
        this._projectsLoading = value;
    }

    private getLicenseData(): void {
        this._licenseDataLoading = true;
        this._userService.getLicenseData(this._authManager.activeUser.id).finally(() => {
            this._licenseDataLoading = false;
        }).subscribe(data => {
            if (data) {
                this._licenseData = data;
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not get license data.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
