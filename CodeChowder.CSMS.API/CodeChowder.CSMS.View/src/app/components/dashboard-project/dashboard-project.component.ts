import { ProjectSelectionType } from '../../enums/clientEnums/ProjectSelectionType';
import { IProjectSelection } from '../../models/clientModels/interfaces/IProjectSelection';
import { ProjectSelection } from '../../models/clientModels/ProjectSelection';
import { IProject } from '../../models/interfaces/IProject';
import { Project } from '../../models/Project';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-dashboard-project',
    templateUrl: './dashboard-project.component.html',
    styleUrls: ['./dashboard-project.component.scss']
})
export class DashboardProjectComponent implements OnInit {
    private _project: IProject;
    private _projectSelectionEmitter: EventEmitter<IProjectSelection>;
    private _showOptions: boolean;
    private _showResultsOptions: boolean;

    constructor() {
        this._project = new Project();
        this._projectSelectionEmitter = new EventEmitter<IProjectSelection>();
        this._showOptions = false;
        this._showResultsOptions = false;
    }

    ngOnInit() {
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        this._project = value;
    }

    @Output()
    public get projectSelected(): EventEmitter<IProjectSelection> {
        return this._projectSelectionEmitter;
    }

    public get projectSelectionType(): { [value: number]: string } {
        return ProjectSelectionType;
    }

    public get showOptions(): boolean {
        return this._showOptions;
    }

    public get showResultsOptions(): boolean {
        return this._showResultsOptions;
    }

    public onSelected(selectionType: ProjectSelectionType): void {
        this._projectSelectionEmitter.emit(new ProjectSelection(this._project, selectionType));
    }

    public toggleShowOptions(): void {
        this._showOptions = !this._showOptions;
    }

    public toggleShowResultsOptions(): void {
        this._showResultsOptions = !this._showResultsOptions;
    }
}
