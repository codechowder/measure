import { DateUtils } from "../../utilities/DateUtils";
import { IProjectMetrics } from "../../models/interfaces/IProjectMetrics";
import { Project } from "../../models/Project";
import { ProjectService } from "../../services/ProjectService";
import { IProject } from "../../models/interfaces/IProject";
import { IProjectService } from "../../services/interfaces/IProjectService";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { NotificationsService } from "angular2-notifications";
import { Observable } from "rxjs";

@Component({
    selector: "app-delete-metrics",
    templateUrl: "./delete-metrics.component.html",
    styleUrls: ["./delete-metrics.component.scss"],
    providers: [ProjectService]
})
export class DeleteMetricsComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectChange: EventEmitter<IProject>;
    private readonly _projectService: IProjectService;

    private _deleteMetrics: IProjectMetrics[];
    private _loading: boolean;
    private _project: IProject;

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._deleteMetrics = [];
        this._loading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectChange = new EventEmitter<IProject>();
        this._projectService = projectService;
    }

    public ngOnInit(): void {
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            if (!value.projectMetrics) {
                this._project.projectMetrics = [];
            }
        }
    }

    @Output()
    public get projectChange(): EventEmitter<IProject> {
        return this._projectChange;
    }

    public getMetricLabel(metric: IProjectMetrics): string {
        let formattedDate = DateUtils.formatDateTime(metric.created);
        let label = `${formattedDate} - ${metric.name}`;
        return label;
    }

    public onSave(): void {
        let serviceCalls: Observable<boolean>[] = [];

        this._deleteMetrics.forEach(m => {
            serviceCalls.push(this._projectService.deleteMetrics(this._project.id, m.id).map(success => {
                if (success) {
                    let deleteMetricIndex = this._deleteMetrics.indexOf(m);
                    this._deleteMetrics.splice(deleteMetricIndex, 1);
                }
                return success;
            }));
        });

        if (serviceCalls.length) {
            this._loading = true;
            Observable.zip(...serviceCalls).take(1).finally(() => {
                this._loading = false;
            }).subscribe(results => {
                if (results.findIndex(r => !r) > -1) {
                    this._notificationService.error("Oops! Something went wrong.", "Could not save all changes.");
                }
                else {
                    this._notificationService.success("Saved changes.");
                    this._projectChange.emit(this._project);
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
    }

    public onSelectDelete(metric: IProjectMetrics): void {
        let deleteMetricIndex = this._project.projectMetrics.indexOf(metric);
        let deleteMetrics = this._project.projectMetrics.splice(deleteMetricIndex, 1);
        this._deleteMetrics.push(deleteMetrics[0]);
    }
}
