import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { Project } from '../../models/Project';
import { ProjectService } from '../../services/ProjectService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { IProject } from '../../models/interfaces/IProject';
import { ProjectSelectionType } from '../../enums/clientEnums/ProjectSelectionType';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-right-menu-bottom',
    templateUrl: './right-menu-bottom.component.html',
    styleUrls: ['./right-menu-bottom.component.scss'],
    providers: [ProjectService]
})
export class RightMenuBottomComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;
    private readonly _select: EventEmitter<ProjectSelectionType>;

    private _loading: boolean;
    private _project: IProject;
    private _sharedUsers: IApplicationUser[];

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._loading = false;
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
        this._select = new EventEmitter<ProjectSelectionType>();
        this._sharedUsers = [];
    }

    public ngOnInit(): void {
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            this.getSharedUsers();
        }
    }

    @Output()
    public get select(): EventEmitter<ProjectSelectionType> {
        return this._select;
    }

    public get sharedUsers(): IApplicationUser[] {
        return this._sharedUsers;
    }

    public onAddRunClick(): void {
        this._select.emit(ProjectSelectionType.AddRun);
    }

    public onDelete(): void {
        this._select.emit(ProjectSelectionType.Delete);
    }

    public onSettingsClick(): void {
        this._select.emit(ProjectSelectionType.Settings);
    }

    private getSharedUsers(): void {
        this._loading = true;
        this._projectService.getSharedWith(this._project.id).finally(() => {
            this._loading = false;
        }).subscribe(users => {
            if (users) {
                this._sharedUsers = users;
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not get shared users.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}