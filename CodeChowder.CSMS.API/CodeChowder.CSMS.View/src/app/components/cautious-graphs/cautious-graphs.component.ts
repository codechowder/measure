import { ChartBottomData } from '../../chartProcessing/models/ChartBottmData';
import { ChartColorConstants } from '../../constants/clientConstants/ChartColorConstants';

import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart, ChartData, ChartOptions, ChartColor, ChartDataSets } from "chart.js";
import { Project } from "app/models/Project";
import { ProjectMetrics } from "app/models/ProjectMetrics";
import { MetricResult } from "app/models/MetricResult";
import { IMetricResult } from "app/models/interfaces/IMetricResult";
import { MetricSeverityType } from "app/models/enums/MetricSeverityType";
import { IMetricSeverity } from "app/models/interfaces/IMetricSeverity";
import { SourceCodeType } from "app/models/enums/SourceCodeType";
import { Select2Component, Select2OptionData } from 'ng2-select2';
import { chartProcessing } from "app/chartProcessing/chartProcessing";
import { IChartDataArray } from "app/chartProcessing/models/IChartDataArray";
import { IColorInfo } from "app/chartProcessing/models/IColorInfo";
import { IChartProcessedData } from "app/chartProcessing/models/IChartProcessedData";
import { IChartBottomData } from "app/chartProcessing/models/IChartBottomData";

@Component({
    selector: 'app-cautious-graphs',
    templateUrl: './cautious-graphs.component.html',
    styleUrls: ['./cautious-graphs.component.scss']
})
export class CautiousGraphsComponent implements OnInit {
    private _project: Project;
    private _chartBottomData: IChartBottomData;

    private _dccData: any[];
    private _lcomData: any[];
    private _wmcData: any[];
    private _slocData: any[];

    private _chartLabels: string[];
    private _chartType: string;
    private _lineFill: boolean;
    private _chartOptions: ChartOptions;
    private _colors: IColorInfo;

    private _wmcDataArr: IChartDataArray = {};
    private _lcomDataArr: IChartDataArray = {};
    private _dccDataArr: IChartDataArray = {};
    private _slocDataArr: IChartDataArray = {};

    private _dccFiles: Select2OptionData[];
    private _lcomFiles: Select2OptionData[];
    private _slocFiles: Select2OptionData[];
    private _wmcFiles: Select2OptionData[];

    private _options: Select2Options;

    private _dccChart: Chart;
    private _lcomChart: Chart;
    private _slocChart: Chart;
    private _wmcChart: Chart;

    @ViewChild('dccChart') private _dccCanvas: ElementRef;
    @ViewChild('lcomChart') private _lcomCanvas: ElementRef;
    @ViewChild('slocChart') private _slocCanvas: ElementRef;
    @ViewChild('wmcChart') private _wmcCanvas: ElementRef;

    @ViewChild('dccSelect') private _dccSelect: Select2Component;
    @ViewChild('lcomSelect') private _lcomSelect: Select2Component;
    @ViewChild('slocSelect') private _slocSelect: Select2Component;
    @ViewChild('wmcSelect') private _wmcSelect: Select2Component;

    constructor() {
        this._options = {
            placeholder: "Select File"
        }

        this._chartLabels = [];
        this._colors = {
            color: ChartColorConstants.YELLOW_BG,
            border: ChartColorConstants.YELLOW_BORDER
        }

        this._dccData = [];
        this._lcomData = [];
        this._wmcData = [];
        this._slocData = [];

        this._dccFiles = [];
        this._lcomFiles = [];
        this._slocFiles = [];
        this._wmcFiles = [];

        this._project = new Project();
        this._chartType = "line";
        this._lineFill = false;
        this._chartOptions = {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index'
            },
            hover: {
                mode: 'nearest'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Runs'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        };

        this._dccChart = null;
        this._lcomChart = null;
        this._slocChart = null;
        this._wmcChart = null;
    }

    public ngOnInit(): void {
    }

    @Input()
    public set project(value: Project) {
        this._project = value;
        this.setData();
    }

    public get dccFiles(): Select2OptionData[] {
        return this._dccFiles;
    }

    public get lcomFiles(): Select2OptionData[] {
        return this._lcomFiles;
    }

    public get slocFiles(): Select2OptionData[] {
        return this._slocFiles;
    }

    public get wmcFiles(): Select2OptionData[] {
        return this._wmcFiles;
    }

    public get options(): Select2Options {
        return this._options;
    }

    public get chartData(): IChartBottomData {
        return this._chartBottomData;
    }

    public onDccClick(): void {
        if(this._dccSelect.selector.nativeElement.value !== "-1") {
            
            this.dccFileSelected(this._dccSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onLcomClick(): void {
        if(this._lcomSelect.selector.nativeElement.value !== "-1") {
            this.lcomFileSelected(this._lcomSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onSlocClick(): void {
        if(this._slocSelect.selector.nativeElement.value !== "-1") {
            this.slocFileSelected(this._slocSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onWmcClick(): void {
        if(this._wmcSelect.selector.nativeElement.value !== "-1") {
            this.dccFileSelected(this._wmcSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    // events
    public dccFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let chartPageData = chartProcessing.CreateChartData(this._dccDataArr, this._chartLabels, selectedFileKey, "Cautious Coupling", this._colors, this._dccCanvas, this._dccChart);
        this._dccChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public lcomFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let chartPageData = chartProcessing.CreateChartData(this._lcomDataArr, this._chartLabels, selectedFileKey, "Cautious Lack of Cohesion", this._colors, this._lcomCanvas, this._lcomChart);
        this._lcomChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public slocFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let chartPageData = chartProcessing.CreateChartData(this._slocDataArr, this._chartLabels, selectedFileKey, "Cautious SLOC", this._colors, this._slocCanvas, this._slocChart);
        this._slocChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public wmcFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let chartPageData = chartProcessing.CreateChartData(this._wmcDataArr, this._chartLabels, selectedFileKey, "Cautious Complexity", this._colors, this._wmcCanvas, this._wmcChart);
        this._wmcChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    private findSeverityValue(metricName: string, sourceCodeType: SourceCodeType, severityType: MetricSeverityType): number {
        let retValue: number = -1;
        if (this._project && this._project.severityTypes) {
            let index: number = this._project.severityTypes.findIndex((ms: IMetricSeverity) => {
                return ms.metricName === metricName && ms.severityType === severityType && ms.sourceCode === sourceCodeType;
            });

            if (index >= 0) {
                retValue = this._project.severityTypes[index].value;
            }
        }

        return retValue;
    }

    private setData(): void {
        if (this._project && this._project.projectMetrics && this._project.projectMetrics.length > 0) {

            let processedData: IChartProcessedData = chartProcessing.ProcessChartData(this._project,
                MetricSeverityType.Cautious,
                this._wmcDataArr,
                this._lcomDataArr,
                this._dccDataArr,
                this._slocDataArr,
                this._wmcFiles,
                this._lcomFiles,
                this._dccFiles,
                this._slocFiles);


            this._wmcDataArr = processedData.wmcDataArr;
            this._lcomDataArr = processedData.lcomDataArr;
            this._dccDataArr = processedData.dccDataArr;
            this._slocDataArr = processedData.slocDataArr;

            this._chartLabels = chartProcessing.GetChartLabels(this._project.projectMetrics);
        }
    }
}
