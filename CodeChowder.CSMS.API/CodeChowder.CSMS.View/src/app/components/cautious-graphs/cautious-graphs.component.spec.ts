import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CautiousGraphsComponent } from './cautious-graphs.component';

describe('CautiousGraphsComponent', () => {
  let component: CautiousGraphsComponent;
  let fixture: ComponentFixture<CautiousGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CautiousGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CautiousGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
