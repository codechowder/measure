import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SevereGraphsComponent } from './severe-graphs.component';

describe('SevereGraphsComponent', () => {
  let component: SevereGraphsComponent;
  let fixture: ComponentFixture<SevereGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SevereGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SevereGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
