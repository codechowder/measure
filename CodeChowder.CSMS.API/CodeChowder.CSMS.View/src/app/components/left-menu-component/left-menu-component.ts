import { AuthManager } from '../../config/AuthManager';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from "@angular/router";
import { ApplicationUserService } from "app/services/ApplicationUserService";
import { ProjectService } from "app/services/ProjectService";
import { NotificationsService } from "angular2-notifications";
import { IProjectService } from "app/services/interfaces/IProjectService";
import { IProject } from "app/models/interfaces/IProject";
import { ProjectSelectionType } from "app/enums/clientEnums/ProjectSelectionType";
import { IApplicationUserService } from "app/services/interfaces/IApplicationUserService";

@Component({
    selector: 'app-left-menu-component',
    templateUrl: './left-menu-component.html',
    styleUrls: ['./left-menu-component.scss'],
    providers: [ApplicationUserService, ProjectService]
})
export class LeftMenuComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _licenseSelect: EventEmitter<void>;
    private readonly _notificationService: NotificationsService;
    private readonly _profileSelect: EventEmitter<void>;
    private readonly _projectSelect: EventEmitter<IProject>;
    private readonly _projectService: IProjectService;
    private readonly _router: Router;
    private readonly _sharedProjectSelect: EventEmitter<IProject>;
    private readonly _userService: IApplicationUserService;

    private _loading: boolean;
    private _projects: IProject[];
    private _projectSelectionType: ProjectSelectionType;
    private _sharedProjects: IProject[];

    constructor(
        authManager: AuthManager,
        notificationService: NotificationsService,
        projectService: ProjectService,
        userService: ApplicationUserService,
        router: Router
    ) {
        this._authManager = authManager;
        this._licenseSelect = new EventEmitter<void>();
        this._loading = false;
        this._notificationService = notificationService;
        this._profileSelect = new EventEmitter<void>();
        this._projects = [];
        this._projectSelect = new EventEmitter<IProject>();
        this._projectSelectionType = ProjectSelectionType.Settings;
        this._projectService = projectService;
        this._router = router;
        this._sharedProjects = [];
        this._sharedProjectSelect = new EventEmitter<IProject>();
        this._userService = userService;
    }

    public get authenticated(): boolean {
        return this._authManager.authenticated;
    }

    public ngOnInit(): void {
    }

    @Output()
    public get licenseSelect(): EventEmitter<void> {
        return this._licenseSelect;
    }

    public get loading(): boolean {
        return this._loading;
    }

    @Input()
    public set loading(value: boolean) {
        this._loading = value;
    }

    @Output()
    public get profileSelect(): EventEmitter<void> {
        return this._profileSelect;
    }

    public get projects(): IProject[] {
        return this._projects;
    }

    @Input()
    public set projects(value: IProject[]) {
        this._projects = value;
    }

    public get sharedProjects(): IProject[] {
        return this._sharedProjects;
    }

    @Input()
    public set sharedProjects(value: IProject[]) {
        this._sharedProjects = value;
    }

    @Output()
    public get projectSelect(): EventEmitter<IProject> {
        return this._projectSelect;
    }

    @Output()
    public get sharedProjectSelect(): EventEmitter<IProject> {
        return this._sharedProjectSelect;
    }

    public onLicenseClick(): void {
        this._licenseSelect.emit();
    }

    public onLogout(): void {
        this._authManager.destroyAuth();
        //TODO:  Deal with social media login if needed
        this._router.navigate(["/"]);
    }

    public onProfileClick(): void {
        this._profileSelect.emit();
    }

    public onProjectSelect(project: IProject): void {
        this._projectSelect.emit(project);
    }

    public onSharedProjectSelect(project: IProject): void {
        this._sharedProjectSelect.emit(project);
    }
}