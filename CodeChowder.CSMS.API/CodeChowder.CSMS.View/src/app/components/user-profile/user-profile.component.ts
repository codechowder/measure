import { ILicenseKey } from '../../models/interfaces/ILicenseKey';
import { LicenseKey } from '../../models/LicenseKey';
import { ApplicationUser } from '../../models/ApplicationUser';
import { ApplicationUserService } from '../../services/ApplicationUserService';
import { IApplicationUser } from '../../models/interfaces/IApplicationUser';
import { IApplicationUserService } from '../../services/interfaces/IApplicationUserService';
import { AuthManager } from '../../config/AuthManager';
import { Component, OnInit } from '@angular/core';
import { NotificationsService } from "angular2-notifications";

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss'],
    providers: [ApplicationUserService]
})
export class UserProfileComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _notificationService: NotificationsService;
    private readonly _userService: IApplicationUserService;

    private _license: ILicenseKey;
    private _loading: boolean;
    private _user: IApplicationUser;

    constructor(
        authManager: AuthManager,
        notificationsService: NotificationsService,
        userService: ApplicationUserService
    ) {
        this._authManager = authManager;
        this._loading = false;
        this._notificationService = notificationsService;
        this._user = this._authManager.activeUser || new ApplicationUser();
        this._license = new LicenseKey(this._user.license);
        this._userService = userService;
    }

    public ngOnInit(): void {
    }

    public get license(): ILicenseKey {
        return this._license;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get user(): IApplicationUser {
        return this._user;
    }

    public onSaveUser(): void {
        this._loading = true;
        this._userService.edit(this._user).finally(() => {
            this._loading = false;
        }).subscribe(u => {
            if (u) {
                this._notificationService.success("Profile updated.");
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not update profile.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }

    public onUpdateLicense(): void {
        this._loading = true;
        this._userService.updateLicense(this._user.id, this._license).finally(() => {
            this._loading = false;
        }).subscribe(success => {
            if (success) {
                this._notificationService.success("License updated.");
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not update license.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
