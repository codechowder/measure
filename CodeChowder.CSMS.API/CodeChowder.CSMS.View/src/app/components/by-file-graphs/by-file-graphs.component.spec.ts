import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByFileGraphsComponent } from './by-file-graphs.component';

describe('ByFileGraphsComponent', () => {
  let component: ByFileGraphsComponent;
  let fixture: ComponentFixture<ByFileGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByFileGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByFileGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
