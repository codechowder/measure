import { ChartBottomData } from '../../chartProcessing/models/ChartBottmData';
import { Select2Component } from 'ng2-select2';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { chartProcessing } from "app/chartProcessing/chartProcessing";
import { MetricSeverityType } from "app/models/enums/MetricSeverityType";
import { IMetricSeverity } from "app/models/interfaces/IMetricSeverity";
import { SourceCodeType } from "app/models/enums/SourceCodeType";
import { Select2OptionData } from "ng2-select2/ng2-select2";
import { Project } from "app/models/Project";
import { ChartColorConstants } from "app/constants/clientConstants/ChartColorConstants";
import { IChartDataArray } from "app/chartProcessing/models/IChartDataArray";
import { IColorInfo } from "app/chartProcessing/models/IColorInfo";
import { ChartOptions } from "chart.js";
import { IChartProcessedData } from "app/chartProcessing/models/IChartProcessedData";
import { IChartBottomData } from "app/chartProcessing/models/IChartBottomData";

@Component({
    selector: 'app-by-file-graphs',
    templateUrl: './by-file-graphs.component.html',
    styleUrls: ['./by-file-graphs.component.scss']
})
export class ByFileGraphsComponent implements OnInit {
    private _project: Project;

    private _dccData: any[];
    private _lcomData: any[];
    private _wmcData: any[];
    private _slocData: any[];

    private _chartBottomData: IChartBottomData;
    private _chartLabels: string[];
    private _chartType: string;
    private _lineFill: boolean;
    private _chartOptions: ChartOptions;

    private _wmcDataArr: IChartDataArray = {};
    private _lcomDataArr: IChartDataArray = {};
    private _dccDataArr: IChartDataArray = {};
    private _slocDataArr: IChartDataArray = {};

    private _dccFiles: Select2OptionData[];
    private _lcomFiles: Select2OptionData[];
    private _slocFiles: Select2OptionData[];
    private _wmcFiles: Select2OptionData[];

    private _options: Select2Options;

    private _dccChart: Chart;
    private _lcomChart: Chart;
    private _slocChart: Chart;
    private _wmcChart: Chart;

    @ViewChild('dccChart') private _dccCanvas: ElementRef;
    @ViewChild('lcomChart') private _lcomCanvas: ElementRef;
    @ViewChild('slocChart') private _slocCanvas: ElementRef;
    @ViewChild('wmcChart') private _wmcCanvas: ElementRef;

    @ViewChild('dccSelect') private _dccSelect: Select2Component;
    @ViewChild('lcomSelect') private _lcomSelect: Select2Component;
    @ViewChild('slocSelect') private _slocSelect: Select2Component;
    @ViewChild('wmcSelect') private _wmcSelect: Select2Component;

    constructor() {
        this._options = {
            placeholder: "Select File"
        }

        this._chartLabels = [];

        this._dccData = [];
        this._lcomData = [];
        this._wmcData = [];
        this._slocData = [];

        this._dccFiles = [];
        this._lcomFiles = [];
        this._slocFiles = [];
        this._wmcFiles = [];

        this._project = new Project();
        this._chartType = "line";
        this._lineFill = false;
        this._chartOptions = {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index'
            },
            hover: {
                mode: 'nearest'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Runs'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        };

        this._dccChart = null;
        this._lcomChart = null;
        this._slocChart = null;
        this._wmcChart = null;
    }

    public ngOnInit(): void {
    }

    @Input()
    public set project(value: Project) {
        this._project = value;
        this.setData();
    }

    public get dccFiles(): Select2OptionData[] {
        return this._dccFiles;
    }

    public get lcomFiles(): Select2OptionData[] {
        return this._lcomFiles;
    }

    public get slocFiles(): Select2OptionData[] {
        return this._slocFiles;
    }

    public get wmcFiles(): Select2OptionData[] {
        return this._wmcFiles;
    }

    public get options(): Select2Options {
        return this._options;
    }

    public get chartData(): IChartBottomData {
        return this._chartBottomData;
    }

    public onDccClick(): void {
        if(this._dccSelect.selector.nativeElement.value !== "-1") {
            
            this.dccFileSelected(this._dccSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onLcomClick(): void {
        if(this._lcomSelect.selector.nativeElement.value !== "-1") {
            this.lcomFileSelected(this._lcomSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onSlocClick(): void {
        if(this._slocSelect.selector.nativeElement.value !== "-1") {
            this.slocFileSelected(this._slocSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    public onWmcClick(): void {
        if(this._wmcSelect.selector.nativeElement.value !== "-1") {
            this.dccFileSelected(this._wmcSelect.selector.nativeElement);
        }
        else {
            this._chartBottomData = new ChartBottomData();
        }
    }

    // events
    public dccFileSelected(selected): void {
        let selectedFileKey: string = selected.value;
        let fileExt = selectedFileKey.substring(selectedFileKey.lastIndexOf('.'), selectedFileKey.length);
        let fileSourceType: SourceCodeType = this.getSourceTypeByExt(fileExt);
        let chartPageData = chartProcessing.CreateChartData(this._dccDataArr, this._chartLabels, selectedFileKey, "Coupling", null, this._dccCanvas, this._dccChart, this._project.severityTypes, "DCC", fileSourceType);
        this._dccChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public lcomFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let fileExt = selectedFileKey.substring(selectedFileKey.lastIndexOf('.'), selectedFileKey.length);
        let fileSourceType: SourceCodeType = this.getSourceTypeByExt(fileExt);
        let chartPageData = chartProcessing.CreateChartData(this._lcomDataArr, this._chartLabels, selectedFileKey, "Lack of Cohesion", null, this._lcomCanvas, this._lcomChart, this._project.severityTypes, "LCOM", fileSourceType);
        this._lcomChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public slocFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let fileExt = selectedFileKey.substring(selectedFileKey.lastIndexOf('.'), selectedFileKey.length);
        let fileSourceType: SourceCodeType = this.getSourceTypeByExt(fileExt);
        let chartPageData = chartProcessing.CreateChartData(this._slocDataArr, this._chartLabels, selectedFileKey, "SLOC", null, this._slocCanvas, this._slocChart, this._project.severityTypes, "SLOC", fileSourceType);
        this._slocChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    public wmcFileSelected(selected): void {
        let selectedFileKey = selected.value;
        let fileExt = selectedFileKey.substring(selectedFileKey.lastIndexOf('.'), selectedFileKey.length);
        let fileSourceType: SourceCodeType = this.getSourceTypeByExt(fileExt);
        let chartPageData = chartProcessing.CreateChartData(this._wmcDataArr, this._chartLabels, selectedFileKey, "Complexity", null, this._wmcCanvas, this._wmcChart, this._project.severityTypes, "WMC", fileSourceType);
        this._wmcChart = chartPageData.chart;
        this._chartBottomData = chartPageData.chartBottomData;
    }

    private getSourceTypeByExt(fileExt: string): SourceCodeType {
        let sourceType: SourceCodeType;
        switch (fileExt.toLowerCase()) {
            case "tsx":
                sourceType = SourceCodeType.TypeScriptReact;
                break;
            case "js":
                sourceType = SourceCodeType.JavaScript;
                break;
            case "jsx":
                sourceType = SourceCodeType.JavaScriptReact;
                break;
            case "cs":
                sourceType = SourceCodeType.CSharp;
                break;
            case "vb":
                sourceType = SourceCodeType.VisualBasic;
                break;
            case "coffee":
            case "iced":
            case "c":
            case "cc":
            case "cpp":
            case "cxx":
            case "h":
            case "m":
            case "mm":
            case "hpp":
            case "hx":
            case "hxx":
            case "ino":
            case "java":
            case "ls":
            case "nix":
            case "php":
            case "php5":
            case "go":
            case "css":
            case "sass":
            case "scss":
            case "less":
            case "rs":
            case "styl":
            case "scala":
            case "gs":
            case "groovy":
            case "nut":
            case "kt":
            case "kts":
            case "python":
            case "py":
            case "handlebars":
            case "hbs":
            case "mustache":
            case "hs":
            case "html":
            case "htm":
            case "svg":
            case "xml":
            case "lua":
            case "monkey":
            case "nim":
            case "rb":
            case "rkt":
            case "jl":
            case "ml":
            case "mli":
            default:
                sourceType = SourceCodeType.Other;
                break;
        }

        return sourceType;
    }

    private setData(): void {
        if (this._project && this._project.projectMetrics && this._project.projectMetrics.length > 0) {

            let processedData: IChartProcessedData = chartProcessing.ProcessChartData(this._project,
                MetricSeverityType.Acceptable,
                this._wmcDataArr,
                this._lcomDataArr,
                this._dccDataArr,
                this._slocDataArr,
                this._wmcFiles,
                this._lcomFiles,
                this._dccFiles,
                this._slocFiles);


            this._wmcDataArr = processedData.wmcDataArr;
            this._lcomDataArr = processedData.lcomDataArr;
            this._dccDataArr = processedData.dccDataArr;
            this._slocDataArr = processedData.slocDataArr;

            this._chartLabels = chartProcessing.GetChartLabels(this._project.projectMetrics);
        }
    }
}
