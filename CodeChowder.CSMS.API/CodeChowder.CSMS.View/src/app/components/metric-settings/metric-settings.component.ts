import { MetricSeverityType } from '../../models/enums/MetricSeverityType';
import { SourceCodeType } from '../../models/enums/SourceCodeType';
import { IMetricSeverity } from '../../models/interfaces/IMetricSeverity';
import { IProject } from '../../models/interfaces/IProject';
import { MetricSeverity } from '../../models/MetricSeverity';
import { Project } from '../../models/Project';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-metric-settings',
    templateUrl: './metric-settings.component.html',
    styleUrls: ['./metric-settings.component.scss']
})
export class MetricSettingsComponent implements OnInit {
    private _metricName: string;
    private _metricSeverityAcceptable: IMetricSeverity;
    private _metricSeverityCautious: IMetricSeverity;
    private _metricSeveritySevere: IMetricSeverity;
    private _project: IProject;
    private _metricSeverityChange: EventEmitter<IMetricSeverity>;
    private _sourceType: SourceCodeType;

    constructor() {
        this._metricName = "";
        this._metricSeverityAcceptable = new MetricSeverity();
        this._metricSeverityCautious = new MetricSeverity();
        this._metricSeveritySevere = new MetricSeverity();
        this._project = new Project();
        this._metricSeverityChange = new EventEmitter<IMetricSeverity>();
        this._sourceType = null;
    }

    ngOnInit() {
        this.setValues();
    }

    @Input()
    public set metricName(value: string) {
        this._metricName = value;
    }

    public get metricSeverityAcceptable(): IMetricSeverity {
        return this._metricSeverityAcceptable;
    }

    public get metricSeverityCautious(): IMetricSeverity {
        return this._metricSeverityCautious;
    }

    @Output()
    public get metricSeverityChange(): EventEmitter<IMetricSeverity> {
        return this._metricSeverityChange;
    }

    public get metricSeveritySevere(): IMetricSeverity {
        return this._metricSeveritySevere;
    }

    @Input()
    public set project(value: IProject) {
        this._project = value;
    }

    @Input()
    public set sourceType(value: SourceCodeType) {
        this._sourceType = value;
    }

    public onMetricSeverityChanged(metricSeverity: IMetricSeverity): void {
        this._metricSeverityChange.emit(metricSeverity);
    }

    private setValues(): void {
        let applicableMetricsSeverities: IMetricSeverity[] = this._project.severityTypes.filter(
            st => st.metricName === this._metricName && st.sourceCode === this._sourceType
        );

        let metricSeverityAcceptable: IMetricSeverity = applicableMetricsSeverities.find(
            st => st.severityType === MetricSeverityType.Acceptable
        );
        let metricSeverityCautious: IMetricSeverity = applicableMetricsSeverities.find(
            st => st.severityType === MetricSeverityType.Cautious
        );
        let metricSeveritySevere: IMetricSeverity = applicableMetricsSeverities.find(
            st => st.severityType === MetricSeverityType.Severe
        );

        if (metricSeverityAcceptable) {
            this._metricSeverityAcceptable = metricSeverityAcceptable;
        }
        if (metricSeverityCautious) {
            this._metricSeverityCautious = metricSeverityCautious;
        }
        if (metricSeveritySevere) {
            this._metricSeveritySevere = metricSeveritySevere;
        }
    }
}
