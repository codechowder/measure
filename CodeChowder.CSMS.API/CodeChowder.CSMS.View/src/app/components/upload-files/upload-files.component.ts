import { Project } from '../../models/Project';
import { IGitCommit } from '../../models/interfaces/IGitCommit';
import { GitService } from '../../services/GitService';
import { IGitService } from '../../services/interfaces/IGitService';
import { GitRepoInfo } from '../../models/GitRepoInfo';
import { IGitRepoInfo } from '../../models/interfaces/IGitRepoInfo';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { FileUploader } from 'ng2-file-upload';
import { Select2OptionData } from "ng2-select2";
import { AuthManager } from '../../config/AuthManager';
import { HttpConfig } from '../../config/HttpConfig';
import { IgnoreFile } from '../../models/IgnoreFile';
import { IProject } from '../../models/interfaces/IProject';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { ProjectService } from '../../services/ProjectService';
import { IProjectMetrics } from "../../models/interfaces/IProjectMetrics";
import { DateUtils } from "../../utilities/DateUtils";

@Component({
    selector: 'app-upload-files',
    templateUrl: './upload-files.component.html',
    styleUrls: ['./upload-files.component.scss'],
    providers: [GitService, ProjectService]
})
export class UploadFilesComponent implements OnInit {
    private readonly _authManager: AuthManager;
    private readonly _gitService: IGitService;
    private readonly _notificationService: NotificationsService;
    private readonly _projectChangedEmitter: EventEmitter<IProject>;
    private readonly _projectService: IProjectService;
    private readonly _uploader: FileUploader;

    private _gitCommits: IGitCommit[];
    private _gitCommitsData: Select2OptionData[];
    private _gitCommitsOptions: Select2Options;
    private _hasBaseDropZoneOver: boolean;
    private _info: IGitRepoInfo;
    private _loading: boolean;
    private _project: IProject;
    private _showGitCommits: boolean;

    @ViewChild('directoryUpload') private _directoryUpload: ElementRef;

    constructor(
        authManager: AuthManager,
        gitService: GitService,
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._info = new GitRepoInfo();
        this._project = new Project();

        this._gitService = gitService;
        this._projectService = projectService;
        this._notificationService = notificationService;

        this._authManager = authManager;

        this._loading = false;
        this._hasBaseDropZoneOver = false;

        this._projectChangedEmitter = new EventEmitter<IProject>();

        this._uploader = new FileUploader({});

        this._gitCommits = [];
        this._gitCommitsData = [];
        this._gitCommitsOptions = {
            multiple: true,
            placeholder: "Select none to run only the last commit."
        };
        this._showGitCommits = false;
    }

    public ngOnInit(): void {
        this._directoryUpload.nativeElement.onchange = () => {
            this._loading = true;
            this._uploader.clearQueue();
            let fileArray: File[] = [];
            for (let i = 0; i < this._directoryUpload.nativeElement.files.length; i++) {
                let item: File = this._directoryUpload.nativeElement.files[i];
                if (item.webkitRelativePath.indexOf('node_modules') < 0) {
                    let ignoreSpecific: number = -1;
                    if (this._project.ignoreFiles) {
                        ignoreSpecific = this._project.ignoreFiles.findIndex((ignoreFile: IgnoreFile) => {
                            let slash: string = "/";
                            if (ignoreFile.pattern.indexOf("\\") > -1) {
                                slash = "\\";
                            }
                            // *.ts
                            // path/*.ts
                            if (ignoreFile.pattern.indexOf("*") > -1) {
                                let starSplitIndex: number = ignoreFile.pattern.indexOf("*") + 1;
                                let right: string = ignoreFile.pattern.substring(starSplitIndex);
                                // path/*.ts
                                if (ignoreFile.pattern.indexOf(slash) > -1) {
                                    let left: string = ignoreFile.pattern.substring(0, starSplitIndex);
                                    if ((item.webkitRelativePath.substring(0, left.length) === left) && (item.webkitRelativePath.substring(item.webkitRelativePath.length - right.length, item.webkitRelativePath.length) === right)) {
                                        return true;
                                    }
                                }
                                // *.ts
                                else {
                                    if (item.webkitRelativePath.substring(item.webkitRelativePath.length - right.length, item.webkitRelativePath.length) === right) {
                                        return true;
                                    }
                                }
                            }
                            // path/path
                            // path/path/file.ts
                            else if (ignoreFile.pattern.indexOf(slash) > -1) {
                                // path/path/file.ts
                                if (ignoreFile.pattern.indexOf(".") > -1) {
                                    return item.webkitRelativePath == ignoreFile.pattern;
                                }
                                // path/path
                                else {
                                    return item.webkitRelativePath.indexOf(ignoreFile.pattern) === 0;
                                }
                            }
                            // node_modules
                            // file.ts
                            else {
                                // file.ts
                                if (ignoreFile.pattern.indexOf(".") > -1) {
                                    return item.webkitRelativePath.substring(item.webkitRelativePath.length - ignoreFile.pattern.length) === ignoreFile.pattern;
                                }
                                // node_modules
                                else {
                                    return item.webkitRelativePath.indexOf(ignoreFile.pattern) > -1;
                                }
                            }
                        });
                    }
                    if (ignoreSpecific < 0) {
                        fileArray.push(item);
                    }
                }
            }
            this._uploader.addToQueue(fileArray);
            this._loading = false;
        }
    }

    public get gitCommitsData(): Select2OptionData[] {
        return this._gitCommitsData;
    }

    public get gitCommitsOptions(): Select2Options {
        return this._gitCommitsOptions;
    }

    public get info(): IGitRepoInfo {
        return this._info;
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(v: IProject) {
        if (v) {
            this._project = v;
            if (this._project.sourceRepoUrl) {
                this._info.url = this._project.sourceRepoUrl;
            }
        }
    }

    @Output()
    public get projectChange(): EventEmitter<IProject> {
        return this._projectChangedEmitter;
    }

    public get showGitCommits(): boolean {
        return this._showGitCommits;
    }

    public get uploader(): FileUploader {
        return this._uploader;
    }

    public get hasBaseDropZoneOver(): boolean {
        return this._hasBaseDropZoneOver;
    }

    public set hasBaseDropZoneOver(v: boolean) {
        this._hasBaseDropZoneOver = v;
    }

    public getCommits(): void {
        if (this._info.url && this._info.selectedBranch && this._info.password) {
            this._loading = true;
            this._gitService.getCommits(this._info).finally(() => {
                this._loading = false;
            }).subscribe(commits => {
                if (commits) {
                    this._gitCommitsData = (this._gitCommits = commits).map(c => {
                        let formattedDate = DateUtils.formatDateTime(c.date);
                        return {
                            id: c.commit,
                            text: `${c.commit.substr(0, 11)} (${formattedDate}): ${c.message}`
                        };
                    });
                    this._showGitCommits = true;
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Could not get commit info.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
        else {
            this._notificationService.warn("All fields are required.");
        }
    }

    public getIgnoreFiles(): void {
        if (!this._project.ignoreFiles) {
            this._loading = true;
            this._projectService.getIgnoreFiles(this._project.id).finally(() => {
                this._loading = false;
            }).subscribe(i => {
                if (i) {
                    this._project.ignoreFiles = i;
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Could not retrieve project info.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
    }

    public fileOverBase(e: any): void {
        this._hasBaseDropZoneOver = e;
    }

    public onGitCommitsChange(event: { value: string[] }): void {
        this._info.gitCommits = this._gitCommits.filter(c => {
            return event.value.findIndex(v => v === c.commit) > -1;
        }).map(c => c);
    }

    public runGitMetrics(): void {
        if (this._info.password && this._info.selectedBranch && this._info.url/* && this._info.userName*/) {
            this._loading = true;
            this._projectService.runGitMetrics(this._project.id, this._info).finally(() => {
                this._loading = false;
            }).subscribe(projectMetrics => {
                if (projectMetrics) {
                    this._project.projectMetrics.push(...projectMetrics);
                    this._projectChangedEmitter.emit(this._project);
                    this._notificationService.success("Run complete!", "Successfully ran and stored metrics.");
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Metrics not returned.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
        else {
            this._notificationService.alert("All fields are required.");
        }
    }

    public uploadAndRun(): void {
        if (this._uploader && this._uploader.queue) {
            this._loading = true;
            let files: File[] = this._uploader.queue.map(f => f._file);
            this._projectService.runMetrics(this._project.id, files).finally(() => {
                this._loading = false;
            }).subscribe((projectMetrics: IProjectMetrics) => {
                if (projectMetrics) {
                    this._project.projectMetrics.push(projectMetrics);
                    this._projectChangedEmitter.emit(this._project);
                    this._notificationService.success("Run complete!", "Successfully ran and stored metrics.");
                }
                else {
                    this._notificationService.error("Oops! Something went wrong.", "Metrics not returned.");
                }
            }, (error: string) => {
                this._notificationService.error("Oops! Something went wrong.", error);
            });
        }
    }
}
