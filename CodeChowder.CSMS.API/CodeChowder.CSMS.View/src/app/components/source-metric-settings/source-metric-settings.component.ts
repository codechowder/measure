import { IMetricSeverity } from '../../models/interfaces/IMetricSeverity';
import { MetricSeverityType } from '../../models/enums/MetricSeverityType';
import { SourceCodeType } from '../../models/enums/SourceCodeType';
import { IProject } from '../../models/interfaces/IProject';
import { Project } from '../../models/Project';

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
    selector: 'app-source-metric-settings',
    templateUrl: './source-metric-settings.component.html',
    styleUrls: ['./source-metric-settings.component.scss']
})
export class SourceMetricSettingsComponent implements OnInit {
    private _metricSeverityChange: EventEmitter<IMetricSeverity>;
    private _project: IProject;
    private _sourceType: SourceCodeType;
    private _switcherName: string;

    constructor() {
        this._metricSeverityChange = new EventEmitter<IMetricSeverity>();
        this._project = new Project();
        this._sourceType = null;
        this._switcherName = "";
    }

    ngOnInit() {
    }

    @Output()
    public get metricSeverityChange(): EventEmitter<IMetricSeverity> {
        return this._metricSeverityChange;
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        this._project = value;
    }

    public get sourceType(): SourceCodeType {
        return this._sourceType;
    }

    @Input()
    public set sourceType(value: SourceCodeType) {
        this._sourceType = value;
    }

    public get switcherDataName(): string {
        return `{connect:'#${this._switcherName}'}`;
    }

    public get switcherName(): string {
        return this._switcherName;
    }

    @Input()
    public set switcherName(value: string) {
        this._switcherName = value;
    }

    public onMetricSeverityChanged(metricSeverity: IMetricSeverity): void {
        this._metricSeverityChange.emit(metricSeverity);
    }
}
