import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourceMetricSettingsComponent } from './source-metric-settings.component';

describe('SourceMetricSettingsComponent', () => {
  let component: SourceMetricSettingsComponent;
  let fixture: ComponentFixture<SourceMetricSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceMetricSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourceMetricSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
