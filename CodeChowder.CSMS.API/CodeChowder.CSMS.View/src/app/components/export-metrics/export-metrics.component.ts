import { DateUtils } from "../../utilities/DateUtils";
import { ProjectService } from '../../services/ProjectService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { Project } from '../../models/Project';
import { IProject } from '../../models/interfaces/IProject';
import { Component, Input, OnInit } from "@angular/core";
import { NotificationsService } from "angular2-notifications";
import { Select2OptionData } from "ng2-select2";

@Component({
    selector: "app-export-metrics",
    templateUrl: "./export-metrics.component.html",
    styleUrls: ["./export-metrics.component.scss"],
    providers: [ProjectService]
})
export class ExportMetricsComponent implements OnInit {
    private readonly _notificationService: NotificationsService;
    private readonly _projectService: IProjectService;

    private _loading: boolean;
    private _metricsRunData: Select2OptionData[];
    private _metricsRunOptions: Select2Options;
    private _project: IProject;
    private _selectedRun: number;

    constructor(
        notificationService: NotificationsService,
        projectService: ProjectService
    ) {
        this._loading = false;
        this._metricsRunData = [];
        this._metricsRunOptions = {
        };
        this._notificationService = notificationService;
        this._project = new Project();
        this._projectService = projectService;
        this._selectedRun = 0;
    }

    ngOnInit() {
    }

    public get metricsRunData(): Select2OptionData[] {
        return this._metricsRunData;
    }

    public get metricsRunOptions(): Select2Options {
        return this._metricsRunOptions;
    }

    public get loading(): boolean {
        return this._loading;
    }

    @Input()
    public set project(value: IProject) {
        if (value) {
            this._project = value;
            this.getMetricsRuns();
        }
    }

    public exportMetrics(): void {
        this._loading = true;
        this._projectService.getExportedMetrics(this._project.id, this._selectedRun).finally(() => {
            this._loading = false;
        }).subscribe(blob => {
            if (blob) {
                // TODO: Use file saver package or whatever to support file extension upon download.
                window.open(window.URL.createObjectURL(blob));
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Failed to download metrics.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }

    public onRunSelect(selection: { value: string }) {
        if (!isNaN(parseInt(selection.value))) {
            this._selectedRun = parseInt(selection.value);
        }
    }

    private getMetricsRuns(): void {
        this._loading = true;
        this._projectService.getMetricsRuns(this._project.id).finally(() => {
            this._loading = false;
        }).subscribe(r => {
            if (r) {
                let data: Select2OptionData[] = r.map(run => {
                    let formattedDate = DateUtils.formatDateTime(run.created);
                    return {
                        id: run.id.toString(),
                        text: `${formattedDate} - ${run.name}`
                    };
                });
                data.unshift({
                    id: "0",
                    text: "All Runs"
                });
                this._metricsRunData = data;
            }
            else {
                this._notificationService.error("Oops! Something went wrong.", "Could not retrieve metrics history.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
