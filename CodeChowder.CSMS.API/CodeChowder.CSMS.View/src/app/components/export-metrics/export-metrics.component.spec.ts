import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportMetricsComponent } from './export-metrics.component';

describe('ExportMetricsComponent', () => {
  let component: ExportMetricsComponent;
  let fixture: ComponentFixture<ExportMetricsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExportMetricsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportMetricsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
