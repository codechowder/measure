import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartBottomComponent } from './chart-bottom.component';

describe('ChartBottomComponent', () => {
  let component: ChartBottomComponent;
  let fixture: ComponentFixture<ChartBottomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartBottomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartBottomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
