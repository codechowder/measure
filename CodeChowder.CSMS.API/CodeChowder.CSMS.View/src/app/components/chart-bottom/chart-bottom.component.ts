import { Component, OnInit, Input } from '@angular/core';
import { IChartBottomData } from "app/chartProcessing/models/IChartBottomData";

@Component({
    selector: 'app-chart-bottom',
    templateUrl: './chart-bottom.component.html',
    styleUrls: ['./chart-bottom.component.scss']
})
export class ChartBottomComponent implements OnInit {

    private _chartData: IChartBottomData;

    constructor() { }

    ngOnInit() {
    }

    @Input()
    public set chartData(v: IChartBottomData) {
        this._chartData = v;
    }

    public get chartData(): IChartBottomData {
        return this._chartData;
    }

}
