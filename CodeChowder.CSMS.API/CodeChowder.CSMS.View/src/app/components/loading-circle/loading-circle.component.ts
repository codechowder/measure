import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-loading-circle',
    templateUrl: './loading-circle.component.html',
    styleUrls: ['./loading-circle.component.scss']
})
export class LoadingCircleComponent implements OnInit {
    private _loading: boolean;

    constructor() {
        this._loading = false;
    }

    ngOnInit() { }

    public get loading(): boolean {
        return this._loading;
    }

    @Input()
    public set loading(value: boolean) {
        this._loading = value;
    }
}
