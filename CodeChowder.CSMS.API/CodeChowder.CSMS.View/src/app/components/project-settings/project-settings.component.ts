import { AuthManager } from '../../config/AuthManager';
import { MetricSeverityType } from '../../models/enums/MetricSeverityType';
import { SourceCodeType } from '../../models/enums/SourceCodeType';
import { IgnoreFile } from '../../models/IgnoreFile';
import { IIgnoreFile } from '../../models/interfaces/IIgnoreFile';
import { IMetricSeverity } from '../../models/interfaces/IMetricSeverity';
import { IProject } from '../../models/interfaces/IProject';
import { MetricSeverity } from '../../models/MetricSeverity';
import { Project } from '../../models/Project';
import { IgnoreFileService } from '../../services/IgnoreFileService';
import { IIgnoreFileService } from '../../services/interfaces/IIgnoreFileService';
import { IProjectService } from '../../services/interfaces/IProjectService';
import { ProjectService } from '../../services/ProjectService';
import { EnumUtils } from '../../utilities/EnumUtils';

import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { Select2OptionData } from "ng2-select2";
import { Observable } from "rxjs";

@Component({
    selector: 'app-project-settings',
    templateUrl: './project-settings.component.html',
    styleUrls: ['./project-settings.component.scss'],
    providers: [IgnoreFileService, ProjectService]
})
export class ProjectSettingsComponent implements OnInit {

    private _projectService: IProjectService;
    private _optionsSource: Select2Options;
    private _optionsSeverity: Select2Options;
    private _sourceTypes: Select2OptionData[];
    private _severityTypes: Select2OptionData[];
    private _project: IProject;
    private _addIgnoreFile: IIgnoreFile;
    private _addSeverityType: IMetricSeverity;
    private _loading: boolean;
    private _notificationService: NotificationsService;
    private _authManager: AuthManager;
    private _projectChangeEmitter: EventEmitter<IProject>;
    private _ignoreFileAdds: IIgnoreFile[];
    private _ignoreFileDeletes: IIgnoreFile[];
    private _ignoreFileUpdates: IIgnoreFile[];
    private _ignoreFileService: IIgnoreFileService;
    private _metricSeverityUpdates: IMetricSeverity[];

    constructor(
        ignoreFileService: IgnoreFileService,
        projectService: ProjectService,
        authManager: AuthManager,
        notificationsService: NotificationsService
    ) {
        this._projectService = projectService;
        this._authManager = authManager;
        this._notificationService = notificationsService;
        this._loading = false;
        this._project = new Project();
        this._addIgnoreFile = new IgnoreFile();
        this._addSeverityType = new MetricSeverity();
        this._projectChangeEmitter = new EventEmitter<IProject>();
        this._metricSeverityUpdates = [];
        this._ignoreFileAdds = [];
        this._ignoreFileDeletes = [];
        this._ignoreFileUpdates = [];
        this._ignoreFileService = ignoreFileService;

        this._optionsSource = {
            placeholder: "Select Source Type"
        };

        this._severityTypes = EnumUtils.select2OptionsFromEnum(MetricSeverityType);
        this._sourceTypes = EnumUtils.select2OptionsFromEnum(SourceCodeType);

        this._optionsSeverity = {
            placeholder: "Select Severity Type"
        }
    }

    ngOnInit() {
    }

    public get loading(): boolean {
        return this._loading;
    }

    public get optionsSource(): Select2Options {
        return this._optionsSource;
    }

    public get severityTypes(): Select2OptionData[] {
        return this._severityTypes;
    }

    public get sourceTypes(): Select2OptionData[] {
        return this._sourceTypes;
    }

    public get project(): IProject {
        return this._project;
    }

    public get addIgnoreFile(): IIgnoreFile {
        return this._addIgnoreFile;
    }

    public get addMetricSeverityType(): IMetricSeverity {
        return this._addSeverityType;
    }

    public get ignoreFiles(): IIgnoreFile[] {
        let ignoreFiles: IIgnoreFile[] = [];
        ignoreFiles.push(...this._project.ignoreFiles);
        ignoreFiles.push(...this._ignoreFileAdds);
        return ignoreFiles;
    }

    @Input()
    public set project(v: IProject) {
        this._project = v;
    }

    @Output()
    public get projectChange(): EventEmitter<IProject> {
        return this._projectChangeEmitter;
    }

    public get sourceCodeType(): { [key: number]: string } {
        return SourceCodeType;
    }

    public severityTypeSelected(event): void {
        this._addSeverityType.severityType = MetricSeverityType[MetricSeverityType[event.value]];
    }

    public sourceTypeSelected(event): void {
        this._addSeverityType.sourceCode = SourceCodeType[SourceCodeType[event.value]];
    }

    public removeSeverityType(i: number): void {
        this._project.severityTypes.splice(i, 1);
    }

    public removeIgnoreFile(ignoreFile: IIgnoreFile): void {
        let index: number = this._project.ignoreFiles.findIndex(i => i == ignoreFile);
        if (index > -1) {
            this._ignoreFileDeletes.push(...this._project.ignoreFiles.splice(index, 1));
        }
        else {
            index = this._ignoreFileAdds.findIndex(i => i == ignoreFile);
            if (index > -1) {
                this._ignoreFileDeletes.push(...this._ignoreFileAdds.splice(index, 1));
            }
        }
    }

    public addIgnore(): void {
        if (this._addIgnoreFile.pattern) {
            let starIndex: number = this._addIgnoreFile.pattern.indexOf("*");
            if (starIndex !== this._addIgnoreFile.pattern.lastIndexOf("*")) {
                this._notificationService.error("Invalid Pattern.", "Cannot specify more than one wildcard.");
            }
            else if (this._addIgnoreFile.pattern.indexOf("/") > starIndex || this._addIgnoreFile.pattern.indexOf("\\") > starIndex) {
                this._notificationService.error("Invalid Pattern.", "Cannot specify a wildcard within a path.");
            }
            else {
                this._ignoreFileAdds.push(this._addIgnoreFile);
                this._addIgnoreFile = new IgnoreFile();
            }
        }
        else {
            this._notificationService.warn("Invalid Pattern.", "Specify a valid file or path pattern.");
        }

    }

    public addSeverity(): void {
        if (this._addSeverityType.value && this._addSeverityType.severityType && this._addSeverityType.sourceCode && this._addSeverityType.metricName) {
            this._project.severityTypes.push(this._addSeverityType);
            this._addSeverityType = new MetricSeverity();
        }
        else {
            this._notificationService.warn("Invalid Entry", "Specify a value, severity type, and source type.");
        }
    }

    public getMetricSeverityTypeText(severity: IMetricSeverity): string {
        return MetricSeverityType[severity.severityType];
    }

    public getMetricSeveritySourceText(severity: IMetricSeverity): string {
        return SourceCodeType[severity.sourceCode];
    }

    public onMetricSeverityChanged(metricSeverity: IMetricSeverity): void {
        let index: number = this._metricSeverityUpdates.findIndex(m => m.id === metricSeverity.id);
        if (index > -1) {
            this._metricSeverityUpdates[index] = metricSeverity;
        }
        else {
            this._metricSeverityUpdates.push(metricSeverity);
        }
    }

    public onUpdate(): void {
        let serviceCalls: Observable<IProject | IIgnoreFile | boolean>[] = [];

        this._ignoreFileAdds.forEach(a => {
            serviceCalls.push(this._projectService.addIgnoreFile(this._project.id, a).map(i => {
                if (i) {
                    let index: number = this._ignoreFileAdds.findIndex(x => x.pattern === a.pattern);
                    this._ignoreFileAdds.splice(index, 1);
                    this._project.ignoreFiles.push(i);
                }
                return i;
            }).catch((e, caught) => {
                return Observable.of(false);
            }));
        });
        this._ignoreFileDeletes.forEach(d => {
            serviceCalls.push(this._ignoreFileService.delete(d.id).map(s => {
                if (s) {
                    let index: number = this._ignoreFileDeletes.findIndex(x => x.id === d.id);
                    this._ignoreFileDeletes.splice(index, 1);
                }
                return s;
            }).catch((e, caught) => {
                return Observable.of(false);
            }));
        });
        this._metricSeverityUpdates.forEach(m => {
            serviceCalls.push(this._projectService.updateMetricSeverity(this._project.id, m).map(s => {
                if (s) {
                    let index: number = this._metricSeverityUpdates.findIndex(x => x.id === m.id);
                    this._metricSeverityUpdates.splice(index, 1);
                }
                return s;
            }).catch((e, caught) => {
                return Observable.of(false);
            }));
        })
        serviceCalls.push(this._projectService.edit(this._project).catch((e, caught) => {
            return Observable.of(false);
        }));

        this._loading = true;
        Observable.zip(...serviceCalls).take(1).finally(() => {
            this._loading = false;
        }).subscribe((results: (IProject | IIgnoreFile | boolean)[]) => {
            if (results.findIndex(r => !r) > -1) {
                this._notificationService.error("Oops! Something went wrong.", "Failed to update all settings.");
            }
            else {
                this._projectChangeEmitter.emit(this._project);
                this._notificationService.success("Saved settings.");
            }
        }, (error: string) => {
            this._notificationService.error("Oops! Something went wrong.", error);
        });
    }
}
