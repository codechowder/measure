import { IProject } from '../../models/interfaces/IProject';
import { ChartColorConstants } from '../../constants/clientConstants/ChartColorConstants';

import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart, ChartData, ChartOptions, ChartColor, ChartDataSets } from "chart.js";
import { Project } from "app/models/Project";
import { ProjectMetrics } from "app/models/ProjectMetrics";
import { MetricResult } from "app/models/MetricResult";
import { IMetricResult } from "app/models/interfaces/IMetricResult";
import { MetricSeverityType } from "app/models/enums/MetricSeverityType";
import { IMetricSeverity } from "app/models/interfaces/IMetricSeverity";
import { SourceCodeType } from "app/models/enums/SourceCodeType";
import { IChartByLanguage } from "app/models/interfaces/IChartByLanguage";
import { ChartByLanguage } from "app/models/ChartByLanguage";
import { IChartDataArray } from "app/chartProcessing/models/IChartDataArray";
import { chartProcessing } from "app/chartProcessing/chartProcessing";

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})

export class ChartComponent implements OnInit {
    private _project: IProject;

    private _dccData: any[];
    private _lcomData: any[];
    private _wmcData: any[];
    private _slocData: any[];

    private _chartLabels: string[];
    private _chartType: string;
    private _lineFill: boolean;
    private _chartOptions: ChartOptions;

    @ViewChild('dccChart') private _dccChart: ElementRef;
    @ViewChild('lcomChart') private _lcomChart: ElementRef;
    @ViewChild('slocChart') private _slocChart: ElementRef;
    @ViewChild('wmcChart') private _wmcChart: ElementRef;

    constructor() {
        this._chartLabels = [];

        this._dccData = [];
        this._lcomData = [];
        this._wmcData = [];
        this._slocData = [];

        this._project = new Project();
        this._chartType = "line";
        this._lineFill = false;
        this._chartOptions = {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index'
            },
            hover: {
                mode: 'nearest'
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Runs'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    }

    public ngOnInit(): void {
    }

    public get project(): IProject {
        return this._project;
    }

    @Input()
    public set project(value: IProject) {
        this._project = value;
        this.setData();
    }

    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

    private findSeverityValue(metricName: string, sourceCodeType: SourceCodeType, severityType: MetricSeverityType): number {
        let retValue: number = -1;
        if (this._project && this._project.severityTypes) {
            let index: number = this._project.severityTypes.findIndex((ms: IMetricSeverity) => {
                return ms.metricName === metricName && ms.severityType === severityType && ms.sourceCode === sourceCodeType;
            });

            if (index >= 0) {
                retValue = this._project.severityTypes[index].value;
            }
        }

        return retValue;
    }

    private setupChartsByLanguage(): IChartByLanguage[] {
        let chartsByLanguage: IChartByLanguage[] = [];
        for (let n in SourceCodeType) {
            if (typeof SourceCodeType[n] === 'number') {
                let chartByLanguage: IChartByLanguage = new ChartByLanguage();
                chartByLanguage.sourceType = parseInt(SourceCodeType[n]);
                chartsByLanguage.push(chartByLanguage);
            }
        }
        return chartsByLanguage;
    }

    private setData(): void {
        if (this._project && this._project.projectMetrics && this._project.projectMetrics.length > 0) {

            let wmcDataArr: IChartDataArray = {};
            let lcomDataArr: IChartDataArray = {};
            let dccDataArr: IChartDataArray = {};
            let slocDataArr: IChartDataArray = {};

            this._project.projectMetrics.forEach((projectMetrics: ProjectMetrics, index: number) => {

                wmcDataArr[index] = {};
                lcomDataArr[index] = {};
                dccDataArr[index] = {};
                slocDataArr[index] = {};

                let chartsByLanguage: IChartByLanguage[] = this.setupChartsByLanguage();

                projectMetrics.metricResults.sort((a: IMetricResult, b: IMetricResult) => {
                    if (a.metricName < b.metricName)
                        return -1;
                    if (a.metricName > b.metricName)
                        return 1;
                    return 0;
                }).forEach((m: MetricResult) => {
                    let chartsByLanguageIndex: number = chartsByLanguage.findIndex((cbl: IChartByLanguage) => {
                        return cbl.sourceType === m.sourceType;
                    });
                    if (m.metricName === "WMC") {
                        chartsByLanguage[chartsByLanguageIndex].avgWMC += m.value;
                        chartsByLanguage[chartsByLanguageIndex].wmcCount++;
                    }
                    else if (m.metricName === "LCOM") {
                        chartsByLanguage[chartsByLanguageIndex].avgLCOM += m.value;
                        chartsByLanguage[chartsByLanguageIndex].lcomCount++;
                    }
                    else if (m.metricName === "DCC") {
                        chartsByLanguage[chartsByLanguageIndex].avgDCC += m.value;
                        chartsByLanguage[chartsByLanguageIndex].dccCount++;
                    }
                    else if (m.metricName === "SLOC") {
                        chartsByLanguage[chartsByLanguageIndex].avgSLOC += m.value;
                        chartsByLanguage[chartsByLanguageIndex].slocCount++;
                    }
                });

                for (let item in SourceCodeType) {
                    if (isNaN(Number(item))) {
                        let sourceCodeType: SourceCodeType = parseInt(SourceCodeType[item]);
                        let chartsByLanguageIndex: number = chartsByLanguage.findIndex((cbl: IChartByLanguage) => {
                            return cbl.sourceType === sourceCodeType;
                        });
                        wmcDataArr[index][sourceCodeType] = [];
                        lcomDataArr[index][sourceCodeType] = [];
                        dccDataArr[index][sourceCodeType] = [];
                        slocDataArr[index][sourceCodeType] = [];

                        if (chartsByLanguage[chartsByLanguageIndex].wmcCount > 0) {

                            chartsByLanguage[chartsByLanguageIndex].avgWMC = chartsByLanguage[chartsByLanguageIndex].avgWMC /
                                chartsByLanguage[chartsByLanguageIndex].wmcCount;

                            wmcDataArr[index][sourceCodeType].push(chartsByLanguage[chartsByLanguageIndex].avgWMC);
                        }
                        else {
                            wmcDataArr[index][sourceCodeType].push(0);
                        }

                        if (chartsByLanguage[chartsByLanguageIndex].lcomCount > 0) {

                            chartsByLanguage[chartsByLanguageIndex].avgLCOM = chartsByLanguage[chartsByLanguageIndex].avgLCOM /
                                chartsByLanguage[chartsByLanguageIndex].lcomCount;

                            lcomDataArr[index][sourceCodeType].push(chartsByLanguage[chartsByLanguageIndex].avgLCOM);
                        }
                        else {
                            lcomDataArr[index][sourceCodeType].push(0);
                        }

                        if (chartsByLanguage[chartsByLanguageIndex].dccCount > 0) {

                            chartsByLanguage[chartsByLanguageIndex].avgDCC = chartsByLanguage[chartsByLanguageIndex].avgDCC /
                                chartsByLanguage[chartsByLanguageIndex].dccCount;

                            dccDataArr[index][sourceCodeType].push(chartsByLanguage[chartsByLanguageIndex].avgDCC);
                        }
                        else {
                            dccDataArr[index][sourceCodeType].push(0);
                        }

                        if (chartsByLanguage[chartsByLanguageIndex].slocCount > 0) {

                            chartsByLanguage[chartsByLanguageIndex].avgSLOC = chartsByLanguage[chartsByLanguageIndex].avgSLOC /
                                chartsByLanguage[chartsByLanguageIndex].slocCount;

                            slocDataArr[index][sourceCodeType].push(chartsByLanguage[chartsByLanguageIndex].avgSLOC);
                        }
                        else {
                            slocDataArr[index][sourceCodeType].push(0);
                        }

                    }
                }

            });

            this._chartLabels = chartProcessing.GetChartLabels(this._project.projectMetrics);
            chartProcessing.CreateAverageChartData(wmcDataArr, this._chartLabels, "Average Complexity", this._project.severityTypes, "WMC", this._wmcChart);
            chartProcessing.CreateAverageChartData(lcomDataArr, this._chartLabels, "Average Lack of Cohesion", this._project.severityTypes, "LCOM", this._lcomChart);
            chartProcessing.CreateAverageChartData(dccDataArr, this._chartLabels, "Average Coupling", this._project.severityTypes, "DCC", this._dccChart);
            chartProcessing.CreateAverageChartData(slocDataArr, this._chartLabels, "Average SLOC", this._project.severityTypes, "SLOC", this._slocChart);
        }
    }
}
