#!/bin/sh

#########################
# Backend Function
#########################
run_backend() {
    echo "Startin the backend...."
    get_ip_of_host
    if [ "$MYSQLRUNNING" == "YES" ]; then
        defaultip="$IPADDRESS"
        read -p "What's the IP Address/URL of the MySQL database? ($defaultip)" MYSQLIP 
        : ${MYSQLIP:=$defaultip}
        defaultuserid="measure"
        read -p "What's the user id of the MYSQL user of the database? ($defaultuserid)" MYSQLUSER 
        : ${MYSQLUSER:=$defaultuserid}
        defaultpassword="1qaz@WSX"
        read -p "What's the password of the MYSQL user of the database? ($defaultpassword)" MYSQLPASS 
        : ${MYSQLPASS:=$defaultpassword}
        CONN="server=$MYSQLIP;user id=$MYSQLUSER;password=$MYSQLPASS;database=Measure;persistsecurityinfo=True;allowuservariables=True"
    else
        CONN="server=$IPADDRESS;user id=measure;password=1qaz@WSX;database=Measure;persistsecurityinfo=True;allowuservariables=True"
    fi
    echo $CONN
    #docker load --input measure-backend.tar
    docker pull tshrove/measure-backend:latest
    docker run -dit --restart unless-stopped -p 5000:80 --name measure_backend -e MYSQL_CONNECTION_STRING="$CONN" -d tshrove/measure-backend
    if [ $? -eq 0 ]; then
        echo "Backend Running"
    else
        stop_backend_docker
        run_backend
    fi
}
#########################
# Frontend Function
#########################
run_frontend() {
    get_ip_of_host
    echo $IPADDRESS
    #docker load --input measure-frontend.tar
    docker pull tshrove/measure-frontend:latest
    docker run -dit --restart unless-stopped --name measure_frontend -e API_PATH="http://$IPADDRESS:5000" -p 3000:80 -d tshrove/measure-frontend
    if [ $? -eq 0 ]; then
        echo "Frontend Running"
    else
        stop_frontend_docker
        run_frontend
    fi
}
#########################
# Check for backend running before proceeding.
#########################
check_for_backend() {
    HASBACKEND=$(docker ps | grep "measure_backend")
    if [ "$HASBACKEND" == "" ]; then
        echo "No Backend Running, Please Run Backend Script ex. ./install.sh --backend"
    else
        run_frontend
    fi
}
#########################
# Get IP of host
#########################
get_ip_of_host() {
    Ubuntu=$(cat /etc/*-release | grep "DISTRIB_ID=Ubuntu")
    if [ "$Ubuntu" == "" ]; then
        IPADDRESS=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
    else
	IPADDRESS=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}')
    fi
}
#########################
# Get MySQL running
#########################
run_mysql() {
    echo "Starting MYSQL docker container"
    docker run -dit --restart unless-stopped -p 3306:3306 --name measure_db -e MYSQL_ROOT_PASSWORD=2wsx#EDC -e MYSQL_USER=measure -e MYSQL_PASSWORD=1qaz@WSX -e MYSQL_DATABASE=Measure -e "grant all privileges on *.* to 'measure'@'%';" -d mysql:8.0.2 --default-storage-engine=MyISAM
}
#########################
# Stop the backend if running
#########################
stop_backend_docker() {
    echo "Stopping existing backend container"
    docker stop measure_backend
    docker rm measure_backend
}

#########################
# Stop the backend if running
#########################
stop_frontend_docker() {
    echo "Stopping existing frontend container"
    docker stop measure_frontend
    docker rm measure_frontend
}
#########################
# Stop mysql docker container if running.
#########################
stop_mysql_dockers() {
    echo "Stopping existing docker container"
    docker stop measure_db
    docker rm measure_db
}
#########################
# Start the backend installation here.
#########################
backend_main() {
    default="YES"
    read -p "Do you want MYSQL installed?: YES [Please enter for YES] " YN
    : ${YN:=$default}
    if [[ "$YN" == [Yy]* ]]; then
        echo "Downloading the MYSQL docker image."
        docker pull mysql
        run_mysql
        if [ $? -eq 0 ]; then
            run_backend
        else
            stop_mysql_dockers
            run_mysql
            run_backend
        fi
    else
        MYSQLRUNNING="YES";
        run_backend
    fi
}
#########################
# Start the frontend installation here.
#########################
frontend_main() {
    check_for_backend
    echo "/n/n Successful Installation. Please connect to application from http://localhost:3000"
}
#########################
# Help Menu
#########################
help_menu() {
    echo "-a installs all"
    echo "-b install backend only."
    echo "-f install frontend only"
}
#########################
# Run everything
#########################
main() {
    backend_main
    frontend_main
    echo "/n/n Successful Installation. Please connect to application from http://localhost:3000"
}

docker version
if [ $? -eq 0 ]; then
    if [ $1 == "-b" ]; then
        backend_main
    elif [ $1 == "-f" ]; then
        frontend_main
    elif [ $1 == "-h" ]; then
        help_menu
    elif [ $1 == "-a" ]; then
        main
    else
        main
    fi
else
    echo "Docker needs to be installed"
fi