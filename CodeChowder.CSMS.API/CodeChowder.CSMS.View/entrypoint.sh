#!/bin/sh

echo "Converting path"
echo $API_PATH

if [ -f *main.bundle.js.bak ]; then
    rm -rf *main.bundle.js
    for file in *main.bundle.js.bak; do
        mv "$file" "`basename "$file" .js.bak`.js"
    done
fi

sed -i.bak "s@http://localhost:5000@$API_PATH@g" /usr/share/nginx/html/*main.bundle.js

echo "Things may have happened and other stuff!!"

exec nginx -g 'daemon off;'
