
export class MetricResult { 
    public Id: number;
    public MetricName: string;
    public NameSpace: string;
    public ObjectName: string;
    public Location: string;
    public MethodName: string;
    public Value: number;
    public LineNummber: number;
    public ColumnNumber: number;
    public CreationDate: Date;
    public SourceType: SourceCodeType;
}

