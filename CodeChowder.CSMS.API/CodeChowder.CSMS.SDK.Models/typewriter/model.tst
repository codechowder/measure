﻿$Classes(MetricResult)[
export class $Name { $Properties[
    public $Name: $Type;]
}]

$Enums(SourceCodeType)[
export enum $Name { $Values[
    $Name = $Value][,]
}]