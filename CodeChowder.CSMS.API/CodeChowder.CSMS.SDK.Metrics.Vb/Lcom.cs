﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.Vb
{
    /// <summary>
    /// Represents the Direct Class Coupling Metric
    /// </summary>
    public class Lcom
    {
        #region Private Fields

        /// <summary>
        /// Collection of fields in the class
        /// </summary>
        private List<string> _fields;

        /// <summary>
        /// Collection of variables in the method
        /// </summary>
        private List<string> _methodSet;

        #endregion Private Fields

        #region Public Methods

        public List<MetricResult> LcomMetric(IEnumerable<CompilationUnitSyntax> compUnits, List<string> files, string fileLocation) {
            _methodSet = new List<string>();

            List<MetricResult> lcomResults = new List<MetricResult>();

            int i = 0;
            foreach (CompilationUnitSyntax compUnit in compUnits) {
                lcomResults = ObtainLcomMetric(compUnit, lcomResults, files[i], fileLocation);
                i++;
            }

            return lcomResults;
        }

        #endregion Public Methods

        #region Private Methods

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceBlockSyntax> containingNameSpaces = new List<NamespaceBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.NamespaceStatement.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        private List<MetricResult> ObtainLcom(SyntaxNode node, List<MetricResult> lcomResults, string file, string fileLocation, string lcomObjectName = "Global", int additionalMethodCount = 0) {

            int lcom = 0;
            //The cardinality of set P
            int p = 0;
            //The cardinality of set Q
            int q = 0;

            IEnumerable<MethodBlockSyntax> methods = node.ChildNodes().Where(c => c is MethodBlockSyntax).Select(c => c as MethodBlockSyntax);

            int methodCount = additionalMethodCount + methods.Count();

            List<string> customTypes = new List<string>();
            MetricResult lcomResult = new MetricResult();

            lcomResult.NameSpace = GetContainingNameSpace(node);
            lcomResult.ObjectName = lcomObjectName;
            lcomResult.MetricName = Roslyn.Parsers.Common.LCOM_METRIC_NAME;
            lcomResult.Location = file.Replace(fileLocation, "");
            lcomResult.SourceType = SourceCodeType.VisualBasic;
            lcomResult.CreationDate = DateTime.Now;

            List<List<string>> I = new List<List<string>>();

            //Grab all the decisions from the method body
            foreach (MethodBlockSyntax method in methods) {
                IEnumerable<SyntaxNode> variables = method.ChildNodes();
                RecursiveVariable(variables);
            }

            IEnumerable<PropertyBlockSyntax> properties = node.ChildNodes().Where(c => c is PropertyBlockSyntax).Select(c => c as PropertyBlockSyntax);
            foreach (PropertyBlockSyntax property in properties) {
                IEnumerable<SyntaxNode> variables = property.ChildNodes();
                RecursiveVariable(variables);
            }

            //Keep the sets of accessed instance variables around. We will
            //need them for the next step of the calculation.
            if (_methodSet.Count > 0) {
                I.Add(_methodSet);

                for (int i = 0; i < methodCount; i++) {
                    for (int j = 0; j < methodCount; j++) {
                        IEnumerable<string> intersection = null;
                        if (i < I.Count && j < I.Count) {
                            intersection = I[i].Intersect(I[j]);
                        }
                        //check if the set is empty
                        if (intersection == null || intersection.Count() == 0) {
                            ++p;
                        }
                        else {
                            ++q;
                        }
                    }
                }
            }

            //# Now p is the number of pairs of methods which don't have
            //# have any instance variable accesses in Roslyn.Parsers.Common.
            //#
            //# q is the number of pairs of methods which DO have an instance
            //# variable access in common

            if (p > q) {
                lcom = p - q;
            }

            lcomResult.Value = lcom;
            lcomResults.Add(lcomResult);

            return lcomResults;
        }

        private List<MetricResult> ObtainLcomMetric(SyntaxNode node, List<MetricResult> lcomResults, string file, string fileLocation) {

            lcomResults = ObtainLcom(node, lcomResults, file, fileLocation);

            //Get the classes
            List<ClassBlockSyntax> classes = new List<ClassBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref classes);

            //Iterate through the classes to get the methods
            foreach (ClassBlockSyntax cds in classes) {
                

                IEnumerable<ConstructorBlockSyntax> constructors = cds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);

                foreach (ConstructorBlockSyntax constructor in constructors) {
                    IEnumerable<SyntaxNode> variables = constructor.ChildNodes();
                    RecursiveVariable(variables);
                }

                lcomResults = ObtainLcom(cds, lcomResults, file, fileLocation, cds.ClassStatement.Identifier.ToString(), constructors.Count());
            }

            //Get the classes

            //Get Structs
            List<ModuleBlockSyntax> structs = new List<ModuleBlockSyntax>();

            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref structs);

            foreach (ModuleBlockSyntax sds in structs) {

                IEnumerable<ConstructorBlockSyntax> constructors = sds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);
                
                foreach (ConstructorBlockSyntax constructor in constructors) {
                    IEnumerable<SyntaxNode> variables = constructor.ChildNodes();
                    RecursiveVariable(variables);
                }

                lcomResults = ObtainLcom(sds, lcomResults, file, fileLocation, sds.ModuleStatement.Identifier.ToString(), constructors.Count());
            }

            return lcomResults;
        }


        /// <summary>
        /// Recursively go through each Syntax node to find all variables
        /// </summary>
        /// <param name="nodes"></param>
        private void RecursiveVariable(IEnumerable<SyntaxNode> nodes) {
            foreach (SyntaxNode node in nodes) {
                if (node is VariableDeclaratorSyntax) {
                    VariableDeclaratorSyntax vds = node as VariableDeclaratorSyntax;
                    foreach (ModifiedIdentifierSyntax modIndent in vds.Names) {
                        if (_fields.Any(f => f == modIndent.Identifier.Text) && !_methodSet.Contains(modIndent.Identifier.Text)) {
                            _methodSet.Add(modIndent.Identifier.Text);
                        }
                    }
                }
                else if (node is IdentifierNameSyntax) {
                    IdentifierNameSyntax ident = node as IdentifierNameSyntax;
                    if (_fields.Any(f => f == ident.Identifier.Text) && !_methodSet.Contains(ident.Identifier.Text)) {
                        _methodSet.Add(ident.Identifier.Text);
                    }
                }
                if (node.ChildNodes().Count() > 0) {
                    RecursiveVariable(node.ChildNodes());
                }
            }
        }

        #endregion Private Methods
    }
}