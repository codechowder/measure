﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.Vb
{
    public class Wmc
    {
        #region Public Methods

        public List<MetricResult> WmcMetric(List<CompilationUnitSyntax> compUnits, List<string> files, string fileLocation) {

            List<MetricResult> wmc = new List<MetricResult>();

            int i = 0;
            foreach (CompilationUnitSyntax compUnit in compUnits) {
                ObtainWmc(compUnit, wmc, files[i], fileLocation);
                i++;
            }

            return wmc;
        }

        #endregion Public Methods

        #region Private Methods

        //TODO:  VB is weird that it can have conditions outside a method.  What should we do in this case?

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceBlockSyntax> containingNameSpaces = new List<NamespaceBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.NamespaceStatement.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        private MetricResult ObtainMethodWmc(SyntaxNode node, MetricResult wmcResult) {
            IEnumerable<MethodBlockSyntax> methods = node.ChildNodes().Where(c => c is MethodBlockSyntax).Select(c => c as MethodBlockSyntax);
            //Grab all the decisions from the method body
            foreach (MethodBlockSyntax method in methods) {
                if (method.BlockStatement != null) {
                    int methodWmc = 0;
                    RecursiveBodyWmc(method.BlockStatement.ChildNodes(), ref methodWmc);
                    wmcResult.Value += methodWmc + 1 /*1 is representative of a return*/;
                }
            }

            return wmcResult;
        }

        private List<MetricResult> ObtainWmc(SyntaxNode node, List<MetricResult> wmc, string file, string fileLocation) {

            MetricResult wmcResult = new MetricResult();

            wmcResult.NameSpace = GetContainingNameSpace(node);
            wmcResult.ObjectName = "Global";
            wmcResult.MetricName = Roslyn.Parsers.Common.WMC_METRIC_NAME;
            wmcResult.SourceType = SourceCodeType.VisualBasic;
            wmcResult.Location = file.Replace(fileLocation, "");
            wmcResult.CreationDate = DateTime.Now;

            wmcResult = ObtainMethodWmc(node, wmcResult);

            wmc.Add(wmcResult);

            //Get the classes
            List<ClassBlockSyntax> classes = new List<ClassBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref classes);

            //Iterate through the classes to get the methods
            foreach (ClassBlockSyntax cds in classes) {
                wmcResult = new MetricResult();

                wmcResult.NameSpace = GetContainingNameSpace(cds);
                wmcResult.ObjectName = cds.ClassStatement.Identifier.ToString();
                wmcResult.MetricName = Roslyn.Parsers.Common.WMC_METRIC_NAME;
                wmcResult.Location = file.Replace(fileLocation, "");

                IEnumerable<ConstructorBlockSyntax> constructors = cds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);
                foreach (ConstructorBlockSyntax constructor in constructors) {
                    int methodWmc = 0;
                    RecursiveBodyWmc(constructor.BlockStatement.ChildNodes(), ref methodWmc);
                    wmcResult.Value += methodWmc + 1 /*1 is representative of a return*/;
                }

                wmcResult = ObtainMethodWmc(cds, wmcResult);

                wmc.Add(wmcResult);
            }

            //Get Structs
            List<ModuleBlockSyntax> structs = new List<ModuleBlockSyntax>();

            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref structs);

            foreach (ModuleBlockSyntax sds in structs) {
                wmcResult = new MetricResult();

                wmcResult.NameSpace = GetContainingNameSpace(sds);
                wmcResult.ObjectName = sds.BlockStatement.Identifier.ToString();
                wmcResult.MetricName = Roslyn.Parsers.Common.WMC_METRIC_NAME;
                wmcResult.Location = file.Replace(fileLocation, "");

                IEnumerable<ConstructorBlockSyntax> constructors = sds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);
                foreach (ConstructorBlockSyntax constructor in constructors) {
                    int methodWmc = 0;
                    RecursiveBodyWmc(constructor.BlockStatement.ChildNodes(), ref methodWmc);
                    wmcResult.Value = methodWmc + 1 /*1 is representative of a return*/;
                }

                wmcResult = ObtainMethodWmc(sds, wmcResult);

                wmc.Add(wmcResult);
            }

            return wmc;
        }

        private void RecursiveBodyWmc(IEnumerable<SyntaxNode> nodes, ref int wmc) {
            foreach (SyntaxNode node in nodes) {
                if (node is IfStatementSyntax) {
                    wmc++;
                }
                else if (node is ElseBlockSyntax) {
                    wmc++;
                }
                else if (node is SelectBlockSyntax) {
                    wmc++;
                }
                else if (node is WhileStatementSyntax) {
                    wmc++;
                }
                else if (node is CatchStatementSyntax) {
                    wmc++;
                }
                else if (node is ForEachStatementSyntax) {
                    wmc++;
                }
                else if (node is ForStatementSyntax) {
                    wmc++;
                }

                if (node.ChildNodes().Count() > 0) {
                    RecursiveBodyWmc(node.ChildNodes(), ref wmc);
                }
            }
        }

        #endregion Private Methods
    }
}