﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.Vb
{
    /// <summary>
    /// Represents the Direct Class Coupling Metric
    /// </summary>
    public class Dcc
    {
        #region Public Methods

        public List<MetricResult> DccMetric(IEnumerable<CompilationUnitSyntax> compUnits, List<string> files, string fileName) {
            List<MetricResult> dccResults = new List<MetricResult>();
            int i = 0;
            foreach (CompilationUnitSyntax compUnit in compUnits) {
                dccResults = ObtainDccMetric(compUnit, dccResults, files[i], fileName);
                i++;
            }

            return dccResults;
        }

        #endregion Public Methods

        #region Private Methods

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceBlockSyntax> containingNameSpaces = new List<NamespaceBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.NamespaceStatement.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        private List<MetricResult> ObtainFieldAndPropertyMetrics(SyntaxNode node, List<string> customTypes, List<MetricResult> dccResults) {
            IEnumerable<FieldDeclarationSyntax> fields = node.ChildNodes().Where(c => c is FieldDeclarationSyntax).Select(c => c as FieldDeclarationSyntax);
            foreach (FieldDeclarationSyntax field in fields) {
                RecusiveField(field.ChildNodes(), ref customTypes);
            }

            IEnumerable<PropertyBlockSyntax> properties = node.ChildNodes().Where(c => c is PropertyBlockSyntax).Select(c => c as PropertyBlockSyntax);
            foreach (PropertyBlockSyntax property in properties) {
                ObjectCreationExpressionSyntax oces = property.ChildNodes().Where(p => p is ObjectCreationExpressionSyntax).Select(p => p as ObjectCreationExpressionSyntax).SingleOrDefault();

                if (oces != null) {
                    if (oces.Type is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = oces.Type as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }
            }

            return dccResults;
        }

        private List<MetricResult> ObtainMethodMetrics(SyntaxNode node, List<string> customTypes, List<MetricResult> dccResults) {

            IEnumerable<MethodBlockSyntax> methods = node.ChildNodes().Where(c => c is MethodBlockSyntax).Select(c => c as MethodBlockSyntax);

            //Grab all the decisions from the method body
            foreach (MethodBlockSyntax method in methods) {
                RecusriveParameterList(method.BlockStatement.ParameterList.ChildNodes(), ref customTypes);

                if (method.SubOrFunctionStatement.AsClause != null) {
                    //Check Return Types
                    if (method.SubOrFunctionStatement.AsClause.Type is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = method.SubOrFunctionStatement.AsClause.Type as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }
            }

            return dccResults;
        }

        private List<MetricResult> ObtainDccMetric(SyntaxNode node, List<MetricResult> dccResults, string file, string fileLocation) {

            List<string> customTypes = new List<string>();
            MetricResult dccResult = new MetricResult();

            dccResult.NameSpace = GetContainingNameSpace(node);
            dccResult.ObjectName = "Global";
            dccResult.MetricName = Roslyn.Parsers.Common.DCC_METRIC_NAME;
            dccResult.SourceType = SourceCodeType.VisualBasic;
            dccResult.Location = file.Replace(fileLocation, "");
            dccResult.CreationDate = DateTime.Now;

            dccResults = ObtainMethodMetrics(node, customTypes, dccResults);

            dccResults = ObtainFieldAndPropertyMetrics(node, customTypes, dccResults);

            dccResult.Value += customTypes.Count();
            dccResults.Add(dccResult);

            List<ClassBlockSyntax> classes = new List<ClassBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref classes);

            //Iterate through the classes to get the methods
            foreach (ClassBlockSyntax cds in classes) {
                customTypes = new List<string>();
                dccResult = new MetricResult();

                dccResult.NameSpace = GetContainingNameSpace(cds);
                dccResult.ObjectName = cds.ClassStatement.Identifier.ToString();
                dccResult.Location = file.Replace(fileLocation, "");
                dccResult.MetricName = Roslyn.Parsers.Common.DCC_METRIC_NAME;

                List<ConstructorBlockSyntax> constructors = new List<ConstructorBlockSyntax>(); //cds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);

                Roslyn.Parsers.Common.RecursiveGrabItem(cds, ref constructors);

                foreach (ConstructorBlockSyntax constructor in constructors) {
                    RecusriveParameterList(constructor.BlockStatement.ParameterList.ChildNodes(), ref customTypes);
                }

                dccResults = ObtainMethodMetrics(cds, customTypes, dccResults);

                dccResults = ObtainFieldAndPropertyMetrics(cds, customTypes, dccResults);

                dccResult.Value += customTypes.Count();
                dccResults.Add(dccResult);
            }

            //Get Modules
            List<ModuleBlockSyntax> modules = new List<ModuleBlockSyntax>();
            Roslyn.Parsers.Common.RecursiveGrabItem(node, ref modules);

            foreach (ModuleBlockSyntax sds in modules) {
                customTypes = new List<string>();
                dccResult = new MetricResult();

                dccResult.NameSpace = GetContainingNameSpace(sds);
                dccResult.ObjectName = sds.BlockStatement.Identifier.ToString();
                dccResult.Location = file.Replace(fileLocation, "");
                dccResult.MetricName = Roslyn.Parsers.Common.DCC_METRIC_NAME;

                List<ConstructorBlockSyntax> constructors = new List<ConstructorBlockSyntax>(); //cds.Members.Where(c => c is ConstructorBlockSyntax).Select(c => c as ConstructorBlockSyntax);

                Roslyn.Parsers.Common.RecursiveGrabItem(sds, ref constructors);

                foreach (ConstructorBlockSyntax constructor in constructors) {
                    RecusriveParameterList(constructor.BlockStatement.ParameterList.ChildNodes(), ref customTypes);
                }
                dccResults = ObtainMethodMetrics(sds, customTypes, dccResults);

                dccResults = ObtainFieldAndPropertyMetrics(sds, customTypes, dccResults);

                dccResult.Value += customTypes.Count();
                dccResults.Add(dccResult);
            }

            return dccResults;
        }

        private void RecusiveField(IEnumerable<SyntaxNode> nodes, ref List<string> customTypes) {
            foreach (SyntaxNode node in nodes) {
                if (node is ObjectCreationExpressionSyntax) {
                    ObjectCreationExpressionSyntax oces = node as ObjectCreationExpressionSyntax;
                    if (oces.Type is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = oces.Type as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }

                if (node.ChildNodes().Count() > 0) {
                    RecusiveField(node.ChildNodes(), ref customTypes);
                }
            }
        }

        private void RecusriveParameterList(IEnumerable<SyntaxNode> nodes, ref List<string> customTypes) {
            foreach (SyntaxNode node in nodes) {
                if (!(node is ParameterSyntax)) {
                    if (node is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = node as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }

                if (node.ChildNodes().Count() > 0) {
                    RecusriveParameterList(node.ChildNodes(), ref customTypes);
                }
            }
        }

        #endregion Private Methods
    }
}