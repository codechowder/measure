﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.CSharp
{
    public class Wmc
    {
        #region Public Methods

        /// <summary>
        /// Calulates the Cyclomatic Complexity
        /// </summary>
        /// <param name="csFiles">C# Files passed in</param>
        /// <returns>Collection of WMCResults</returns>
        public List<MetricResult> WmcMetric(IEnumerable<CompilationUnitSyntax> root, List<string> files, string fileLocation) {
            List<MetricResult> wmc = new List<MetricResult>();

            int i = 0;
            foreach (SyntaxNode nds in root) {
                //Get the classes
                List<ClassDeclarationSyntax> classes = new List<ClassDeclarationSyntax>();
                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref classes);

                //Iterate through the classes to get the methods
                foreach (ClassDeclarationSyntax cds in classes) {
                    MetricResult wmcResult = new MetricResult();

                    wmcResult.NameSpace = GetContainingNameSpace(cds);
                    wmcResult.ObjectName = cds.Identifier.ToString();
                    wmcResult.MetricName = Roslyn.Parsers.Common.WMC_METRIC_NAME;
                    wmcResult.SourceType = SourceCodeType.CSharp;
                    wmcResult.Location = files[i].Replace(fileLocation, "");
                    wmcResult.CreationDate = DateTime.Now;

                    IEnumerable<ConstructorDeclarationSyntax> constructors = cds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);
                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        int methodWmc = 0;
                        RecursiveBodyWmc(constructor.Body.ChildNodes(), ref methodWmc);
                        wmcResult.Value += methodWmc + 1 /*1 is representative of a return*/;
                    }

                    IEnumerable<MethodDeclarationSyntax> methods = cds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);
                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        if (method.Body != null) {
                            int methodWmc = 0;
                            wmcResult.MethodName = method.Identifier.ToString();
                            RecursiveBodyWmc(method.Body.ChildNodes(), ref methodWmc);
                            wmcResult.Value += methodWmc + 1 /*1 is representative of a return*/;
                        }
                    }

                    wmc.Add(wmcResult);
                }

                //Get Structs
                List<StructDeclarationSyntax> structs = new List<StructDeclarationSyntax>();

                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref structs);

                foreach (StructDeclarationSyntax sds in structs) {
                    MetricResult wmcResult = new MetricResult();

                    wmcResult.NameSpace = GetContainingNameSpace(sds);
                    wmcResult.ObjectName = sds.Identifier.ToString();
                    wmcResult.Location = files[i].Replace(fileLocation, "");
                    wmcResult.MetricName = Roslyn.Parsers.Common.WMC_METRIC_NAME;

                    IEnumerable<ConstructorDeclarationSyntax> constructors = sds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);
                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        int methodWmc = 0;
                        RecursiveBodyWmc(constructor.Body.ChildNodes(), ref methodWmc);
                        wmcResult.Value = methodWmc + 1 /*1 is representative of a return*/;
                    }

                    IEnumerable<MethodDeclarationSyntax> methods = sds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);
                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        if (method.Body != null) {
                            int methodWmc = 0;
                            wmcResult.MethodName = method.Identifier.ToString();
                            RecursiveBodyWmc(method.Body.ChildNodes(), ref methodWmc);
                            wmcResult.Value = methodWmc + 1 /*1 is representative of a return*/;
                        }
                    }

                    wmc.Add(wmcResult);
                }
                i++;
            }

            return wmc;
        }

        #endregion Public Methods

        #region Private Methods

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceDeclarationSyntax> containingNameSpaces = new List<NamespaceDeclarationSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        private void RecursiveBodyWmc(IEnumerable<SyntaxNode> nodes, ref int wmc) {
            foreach (SyntaxNode node in nodes) {
                if (node is IfStatementSyntax) {
                    wmc++;
                }
                else if (node is ElseClauseSyntax) {
                    wmc++;
                }
                else if (node is SwitchStatementSyntax) {
                    wmc++;
                }
                else if (node is WhileStatementSyntax) {
                    wmc++;
                }
                else if (node is CatchClauseSyntax) {
                    wmc++;
                }
                else if (node is ForEachStatementSyntax) {
                    wmc++;
                }
                else if (node is ForStatementSyntax) {
                    wmc++;
                }
                else if (node is LambdaExpressionSyntax)
                {
                    wmc++;
                }
                else if (node is WhereClauseSyntax)
                {
                    wmc++;
                }
                else if (node is ConditionalExpressionSyntax)
                {
                    wmc++;
                }
                if (node.ChildNodes().Count() > 0) {
                    RecursiveBodyWmc(node.ChildNodes(), ref wmc);
                }
            }
        }

        #endregion Private Methods
    }
}