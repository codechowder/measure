﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.CSharp
{
    /// <summary>
    /// Represents the Direct Class Coupling Metric
    /// </summary>
    public class Dcc
    {
        #region Public Methods

        /// <summary>
        /// Calulates the Direct Class Coupling
        /// </summary>
        /// <param name="csFiles">C# Files passed in</param>
        /// <returns>Collection of WMCResults</returns>
        public List<MetricResult> DccMetric(IEnumerable<CompilationUnitSyntax> root, List<string> files, string fileLocation) {
            List<MetricResult> dccResults = new List<MetricResult>();

            int i = 0;
            foreach (SyntaxNode nds in root) {
                //Get the classes
                List<ClassDeclarationSyntax> classes = new List<ClassDeclarationSyntax>();
                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref classes);

                //Iterate through the classes to get the methods
                foreach (ClassDeclarationSyntax cds in classes) {
                    List<string> customTypes = new List<string>();
                    MetricResult dccResult = new MetricResult();

                   

                    dccResult.NameSpace = GetContainingNameSpace(cds);
                    dccResult.ObjectName = cds.Identifier.ToString();
                    dccResult.MetricName = Roslyn.Parsers.Common.DCC_METRIC_NAME;
                    dccResult.Location = files[i].Replace(fileLocation, "");
                    dccResult.SourceType = SourceCodeType.CSharp;
                    dccResult.CreationDate = DateTime.Now;

                    List<ConstructorDeclarationSyntax> constructors = new List<ConstructorDeclarationSyntax>(); //cds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);

                    Roslyn.Parsers.Common.RecursiveGrabItem(cds, ref constructors);

                    List<MethodDeclarationSyntax> methods = new List<MethodDeclarationSyntax>(); //cds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);

                    Roslyn.Parsers.Common.RecursiveGrabItem(cds, ref methods);

                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        RecusriveParameterList(constructor.ParameterList.ChildNodes(), ref customTypes);
                    }

                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        RecusriveParameterList(method.ParameterList.ChildNodes(), ref customTypes);

                        //Check Return Types
                        if (method.ReturnType is IdentifierNameSyntax) {
                            IdentifierNameSyntax ins = method.ReturnType as IdentifierNameSyntax;
                            if (!customTypes.Contains(ins.Identifier.Text)) {
                                customTypes.Add(ins.Identifier.Text);
                            }
                        }
                    }

                    IEnumerable<FieldDeclarationSyntax> fields = cds.Members.Where(c => c is FieldDeclarationSyntax).Select(c => c as FieldDeclarationSyntax);
                    foreach (FieldDeclarationSyntax field in fields) {
                        RecusiveField(field.ChildNodes(), ref customTypes);
                    }

                    IEnumerable<PropertyDeclarationSyntax> properties = cds.Members.Where(c => c is PropertyDeclarationSyntax).Select(c => c as PropertyDeclarationSyntax);
                    foreach (PropertyDeclarationSyntax property in properties) {
                        if (property.Type is IdentifierNameSyntax) {
                            IdentifierNameSyntax ins = property.Type as IdentifierNameSyntax;
                            if (!customTypes.Contains(ins.Identifier.Text)) {
                                customTypes.Add(ins.Identifier.Text);
                            }
                        }
                    }

                    dccResult.Value += customTypes.Count();
                    dccResults.Add(dccResult);
                }

                //Get Structs
                List<StructDeclarationSyntax> structs = new List<StructDeclarationSyntax>();

                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref structs);

                foreach (StructDeclarationSyntax sds in structs) {
                    List<string> customTypes = new List<string>();
                    MetricResult dccResult = new MetricResult();

                    dccResult.NameSpace = GetContainingNameSpace(sds);
                    dccResult.ObjectName = sds.Identifier.ToString();
                    dccResult.Location = files[i].Replace(fileLocation, "");
                    dccResult.MetricName = Roslyn.Parsers.Common.DCC_METRIC_NAME;

                    List<ConstructorDeclarationSyntax> constructors = new List<ConstructorDeclarationSyntax>(); //cds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);

                    Roslyn.Parsers.Common.RecursiveGrabItem(sds, ref constructors);

                    List<MethodDeclarationSyntax> methods = new List<MethodDeclarationSyntax>(); //cds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);

                    Roslyn.Parsers.Common.RecursiveGrabItem(sds, ref methods);

                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        RecusriveParameterList(constructor.ParameterList.ChildNodes(), ref customTypes);
                    }

                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        RecusriveParameterList(method.ParameterList.ChildNodes(), ref customTypes);

                        //Check Return Types
                        if (method.ReturnType is IdentifierNameSyntax) {
                            IdentifierNameSyntax ins = method.ReturnType as IdentifierNameSyntax;
                            if (!customTypes.Contains(ins.Identifier.Text)) {
                                customTypes.Add(ins.Identifier.Text);
                            }
                        }
                    }

                    IEnumerable<FieldDeclarationSyntax> fields = sds.Members.Where(c => c is FieldDeclarationSyntax).Select(c => c as FieldDeclarationSyntax);
                    foreach (FieldDeclarationSyntax field in fields) {
                        RecusiveField(field.ChildNodes(), ref customTypes);
                    }

                    IEnumerable<PropertyDeclarationSyntax> properties = sds.Members.Where(c => c is PropertyDeclarationSyntax).Select(c => c as PropertyDeclarationSyntax);
                    foreach (PropertyDeclarationSyntax property in properties) {
                        if (property.Type is IdentifierNameSyntax) {
                            IdentifierNameSyntax ins = property.Type as IdentifierNameSyntax;
                            if (!customTypes.Contains(ins.Identifier.Text)) {
                                customTypes.Add(ins.Identifier.Text);
                            }
                        }
                    }

                    dccResult.Value += customTypes.Count();
                    dccResults.Add(dccResult);
                }
                i++;
            }

            return dccResults;
        }

        #endregion Public Methods

        #region Private Methods

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceDeclarationSyntax> containingNameSpaces = new List<NamespaceDeclarationSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        private void RecusiveField(IEnumerable<SyntaxNode> nodes, ref List<string> customTypes) {
            foreach (SyntaxNode node in nodes) {
                if (node is VariableDeclarationSyntax) {
                    VariableDeclarationSyntax vds = node as VariableDeclarationSyntax;
                    if (vds.Type is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = vds.Type as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }

                if (node.ChildNodes().Count() > 0) {
                    RecusiveField(node.ChildNodes(), ref customTypes);
                }
            }
        }

        private void RecusriveParameterList(IEnumerable<SyntaxNode> nodes, ref List<string> customTypes) {
            foreach (SyntaxNode node in nodes) {
                if (!(node is ParameterSyntax)) {
                    if (node is IdentifierNameSyntax) {
                        IdentifierNameSyntax ins = node as IdentifierNameSyntax;
                        if (!customTypes.Contains(ins.Identifier.Text)) {
                            customTypes.Add(ins.Identifier.Text);
                        }
                    }
                }

                if (node.ChildNodes().Count() > 0) {
                    RecusriveParameterList(node.ChildNodes(), ref customTypes);
                }
            }
        }

        #endregion Private Methods
    }
}