﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.DataStructures.Enumerations;

namespace CodeChowder.CSMS.SDK.Metrics.CSharp
{
    /// <summary>
    /// Represents the Direct Class Coupling Metric
    /// </summary>
    public class Lcom
    {
        #region Private Fields

        /// <summary>
        /// Collection of fields in the class
        /// </summary>
        private List<string> _fields;

        /// <summary>
        /// Collection of variables in the method
        /// </summary>
        private List<string> _methodSet;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Calulates the Lack of Cohesion
        /// </summary>
        /// <param name="csFiles">C# Files passed in</param>
        /// <returns>Collection of WMCResults</returns>
        public List<MetricResult> LcomMetric(IEnumerable<CompilationUnitSyntax> root, List<string> files, string fileLocation) {
            

            List<MetricResult> lcomResults = new List<MetricResult>();

            int fileIndex = 0;
            foreach (SyntaxNode nds in root) {
                //Get the classes
                List<ClassDeclarationSyntax> classes = new List<ClassDeclarationSyntax>();
                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref classes);

                //Iterate through the classes to get the methods
                foreach (ClassDeclarationSyntax cds in classes) {
                    _methodSet = new List<string>();
                    _fields = cds.ChildNodes().OfType<FieldDeclarationSyntax>().SelectMany(f => f.Declaration.Variables.Select(v => v.Identifier.Text)).ToList();
                    int lcom = 0;
                    //The cardinality of set P
                    int p = 0;
                    //The cardinality of set Q
                    int q = 0;

                    IEnumerable<ConstructorDeclarationSyntax> constructors = cds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);

                    List<MethodDeclarationSyntax> methods = new List<MethodDeclarationSyntax>(); //cds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);

                    Roslyn.Parsers.Common.RecursiveGrabItem(cds, ref methods);

                    int methodCount = constructors.Count() + methods.Count();

                    List<string> customTypes = new List<string>();
                    MetricResult lcomResult = new MetricResult();

                    lcomResult.NameSpace = GetContainingNameSpace(cds);
                    lcomResult.ObjectName = cds.Identifier.ToString();
                    lcomResult.MetricName = Roslyn.Parsers.Common.LCOM_METRIC_NAME;
                    lcomResult.Location = files[fileIndex].Replace(fileLocation, "");
                    lcomResult.SourceType = SourceCodeType.CSharp;
                    lcomResult.CreationDate = DateTime.Now;

                    List<List<string>> I = new List<List<string>>();

                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        IEnumerable<SyntaxNode> variables = constructor.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        IEnumerable<SyntaxNode> variables = method.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    IEnumerable<PropertyDeclarationSyntax> properties = cds.Members.Where(c => c is PropertyDeclarationSyntax).Select(c => c as PropertyDeclarationSyntax);
                    foreach (PropertyDeclarationSyntax property in properties) {
                        IEnumerable<SyntaxNode> variables = property.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    //Keep the sets of accessed instance variables around. We will
                    //need them for the next step of the calculation.
                    if (_methodSet.Count > 0) {
                        I.Add(_methodSet);

                        for (int i = 0; i < methodCount; i++) {
                            for (int j = 0; j < methodCount; j++) {
                                IEnumerable<string> intersection = null;
                                if (i < I.Count && j < I.Count) {
                                    intersection = I[i].Intersect(I[j]);
                                }
                                //check if the set is empty
                                if (intersection == null || intersection.Count() == 0) {
                                    ++p;
                                }
                                else {
                                    ++q;
                                }
                            }
                        }
                    }

                    //# Now p is the number of pairs of methods which don't have
                    //# have any instance variable accesses in Roslyn.Parsers.Common.
                    //#
                    //# q is the number of pairs of methods which DO have an instance
                    //# variable access in common

                    if (p > q) {
                        lcom = p - q;
                    }

                    lcomResult.Value = lcom;
                    lcomResults.Add(lcomResult);
                }

                //Get the classes

                //Get Structs
                List<StructDeclarationSyntax> structs = new List<StructDeclarationSyntax>();

                Roslyn.Parsers.Common.RecursiveGrabItem(nds, ref structs);

                foreach (StructDeclarationSyntax sds in structs) {
                    _methodSet = new List<string>();
                    _fields = sds.ChildNodes().OfType<FieldDeclarationSyntax>().SelectMany(f => f.Declaration.Variables.Select(v => v.Identifier.Text)).ToList();
                    int lcom = 0;
                    //The cardinality of set P
                    int p = 0;
                    //The cardinality of set Q
                    int q = 0;

                    IEnumerable<ConstructorDeclarationSyntax> constructors = sds.Members.Where(c => c is ConstructorDeclarationSyntax).Select(c => c as ConstructorDeclarationSyntax);

                    IEnumerable<MethodDeclarationSyntax> methods = sds.Members.Where(c => c is MethodDeclarationSyntax).Select(c => c as MethodDeclarationSyntax);

                    int methodCount = constructors.Count() + methods.Count();

                    List<string> customTypes = new List<string>();
                    MetricResult lcomResult = new MetricResult();

                    lcomResult.NameSpace = GetContainingNameSpace(sds);
                    lcomResult.ObjectName = sds.Identifier.ToString();
                    lcomResult.MetricName = Roslyn.Parsers.Common.LCOM_METRIC_NAME;

                    List<List<string>> I = new List<List<string>>();

                    foreach (ConstructorDeclarationSyntax constructor in constructors) {
                        IEnumerable<SyntaxNode> variables = constructor.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    //Grab all the decisions from the method body
                    foreach (MethodDeclarationSyntax method in methods) {
                        IEnumerable<SyntaxNode> variables = method.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    IEnumerable<PropertyDeclarationSyntax> properties = sds.Members.Where(c => c is PropertyDeclarationSyntax).Select(c => c as PropertyDeclarationSyntax);
                    foreach (PropertyDeclarationSyntax property in properties) {
                        IEnumerable<SyntaxNode> variables = property.ChildNodes();
                        RecursiveVariable(variables);
                    }

                    //Keep the sets of accessed instance variables around. We will
                    //need them for the next step of the calculation.
                    if (_methodSet.Count > 0) {
                        I.Add(_methodSet);

                        for (int i = 0; i < methodCount; i++) {
                            for (int j = 0; j < methodCount; j++) {
                                IEnumerable<string> intersection = null;
                                if (i < I.Count && j < I.Count) {
                                    intersection = I[i].Intersect(I[j]);
                                }
                                //check if the set is empty
                                if (intersection == null || intersection.Count() == 0) {
                                    ++p;
                                }
                                else {
                                    ++q;
                                }
                            }
                        }
                    }

                    //# Now p is the number of pairs of methods which don't have
                    //# have any instance variable accesses in Roslyn.Parsers.Common.
                    //#
                    //# q is the number of pairs of methods which DO have an instance
                    //# variable access in common

                    if (p > q) {
                        lcom = p - q;
                    }

                    lcomResult.Value = lcom;
                    lcomResults.Add(lcomResult);
                }
                fileIndex++;
            }

            return lcomResults;
        }

        #endregion Public Methods

        #region Private Methods

        private string GetContainingNameSpace(SyntaxNode child)
        {
            List<NamespaceDeclarationSyntax> containingNameSpaces = new List<NamespaceDeclarationSyntax>();
            Roslyn.Parsers.Common.RecursiveParentGrabItem(child, ref containingNameSpaces);

            List<string> nameSpaceNames = containingNameSpaces.Select(n => n.Name.ToString()).ToList();

            string nameSpaceNamesFlat = string.Empty;

            if (nameSpaceNames != null && nameSpaceNames.Count > 0)
            {
                nameSpaceNames.ForEach((nsn) => {
                    nameSpaceNamesFlat += nsn + ".";
                });

                nameSpaceNamesFlat = nameSpaceNamesFlat.TrimEnd('.');
            }
            else
            {
                nameSpaceNamesFlat = "Default";
            }

            return nameSpaceNamesFlat;
        }

        /// <summary>
        /// Recursively go through each Syntax node to find all variables
        /// </summary>
        /// <param name="nodes"></param>
        private void RecursiveVariable(IEnumerable<SyntaxNode> nodes) {
            foreach (SyntaxNode node in nodes) {
                if (node is VariableDeclarationSyntax) {
                    VariableDeclarationSyntax vds = node as VariableDeclarationSyntax;
                    foreach (VariableDeclaratorSyntax var in vds.Variables) {
                        if (_fields.Any(f => f == var.Identifier.Text) && !_methodSet.Contains(var.Identifier.Text)) {
                            _methodSet.Add(var.Identifier.Text);
                        }
                    }
                }
                else if (node is IdentifierNameSyntax) {
                    IdentifierNameSyntax ident = node as IdentifierNameSyntax;
                    if (_fields.Any(f => f == ident.Identifier.Text) && !_methodSet.Contains(ident.Identifier.Text)) {
                        _methodSet.Add(ident.Identifier.Text);
                    }
                }
                if (node.ChildNodes().Count() > 0) {
                    RecursiveVariable(node.ChildNodes());
                }
            }
        }

        #endregion Private Methods
    }
}