﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CodeChowder.CSMS.Roslyn.Parsers;
using CodeChowder.CSMS.SDK.Metrics.CSharp;
using System.Linq;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.SDK.CSharp
{
    public class Program
    {
        private static Dcc _dcc;

        private static string _outputFile = string.Empty;
        private const string CS_EXT = ".cs";

        private static void StupidTest() {
            bool testies = false ? true : false;

            if (testies == true) {
            }
        }

        public static void Main(string[] args) {
            //The argument will be either a folder or file containing C#/VB
            if (args.Length >= 1) {
                string path = args[0];

                if (args.Length > 1) {
                    _outputFile = args[1];
                }
                FileAttributes attr = File.GetAttributes(path);
                if (attr.HasFlag(FileAttributes.Directory)) {
                    IEnumerable<string> csFiles = Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories);

                    CSharpAst csAst = new CSharpAst(csFiles);

                    Wmc wmc = new Wmc();
                    Dcc dcc = new Dcc();
                    Lcom lcomMetric = new Lcom();

                    List<MetricResult> results = new List<MetricResult>();

                    results.AddRange(wmc.WmcMetric(csAst.ASTRoot, csFiles.ToList(), "dir"));

                    results.AddRange(dcc.DccMetric(csAst.ASTRoot, csFiles.ToList(), "dir"));

                    results.AddRange(lcomMetric.LcomMetric(csAst.ASTRoot, csFiles.ToList(), "dir"));

                    if (!string.IsNullOrEmpty(_outputFile)) {
                        OutputCsv(results);
                    }
                    else {
                        results.ForEach((mr) => {
                            Console.WriteLine(mr.MetricName + "= " + mr.NameSpace + "." + mr.ObjectName + ": " + mr.Value);
                        });
                    }
                }
                else {
                    ParseFile(path);
                }
            }
            else {
                Console.WriteLine("Only takes one argument, a folder or file.");
            }
        }

        private static void ParseFile(string file) {
            if (Path.GetExtension(file).ToLower() == CS_EXT) {
                List<string> list = new List<string>() { file };

                Wmc wmcMetric = new Wmc();
                Dcc dccMetric = new Dcc();
                Lcom lcomMetric = new Lcom();

                CSharpAst csAst = new CSharpAst(list);


                List<MetricResult> metricResults = wmcMetric.WmcMetric(csAst.ASTRoot, list, file);

                metricResults.AddRange(dccMetric.DccMetric(csAst.ASTRoot, list, file));
                metricResults.AddRange(lcomMetric.LcomMetric(csAst.ASTRoot, list, file));

                if (!string.IsNullOrEmpty(_outputFile)) {
                    OutputCsv(metricResults);
                }
                else {
                    metricResults.ForEach((mr) => {
                        Console.WriteLine(mr.MetricName + "= " + mr.NameSpace + "." + mr.ObjectName + ": " + mr.Value);
                    });
                }
            }
            else {
                Console.WriteLine("Unsupported file extension for file: " + file);
            }
        }

        private static void OutputCsv(List<MetricResult> metricResults) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Metric,Location,Value");
            metricResults.ForEach((mr) => {
                sb.AppendLine(mr.MetricName + "," + mr.NameSpace + "." + mr.ObjectName + "," + mr.Value);
            });

            File.WriteAllText(_outputFile, sb.ToString());
        }
    }
}