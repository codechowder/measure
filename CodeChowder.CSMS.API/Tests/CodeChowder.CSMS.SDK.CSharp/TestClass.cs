﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodeChowder.CSMS.SDK.CSharp
{
    public class TestClass
    {

        List<string> testList = new List<string>();

        public TestClass()
        {

            //What about this?

            var aBool = testList != null ? testList.Count() > 0 : false;

            //What About Linq?
            testList.ForEach(s =>
            {
                Console.WriteLine(s);
            });

            var sesameStreet = testList.Where(s => s == "abc");

            var ghettoStreet = testList.Select(s => s.First());

            //Linq it all to hell!
            var otherThings = from test in testList
                              where test == "abc"
                              select test;
        }

        public void TestMethod()
        {
            //What About Linq?
            testList.ForEach(s =>
            {
                Console.WriteLine(s);
            });

            var sesameStreet = testList.Where(s => s == "abc");

            var ghettoStreet = testList.Select(s => s.First());
        }
    }
}
