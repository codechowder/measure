﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CodeChowder.CSMS.Roslyn.Parsers;
using CodeChowder.CSMS.SDK.Metrics.Vb;
using System.Linq;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.SDK.Vb
{
    public class Program
    {
        private static Dcc _dcc;

        private static string _outputFile = string.Empty;
        private const string VB_EXT = ".vb";

        private static void StupidTest() {
            bool testies = false ? true : false;

            if (testies == true) {
            }
        }

        public static void Main(string[] args) {
            //The argument will be either a folder or file containing C#/VB
            if (args.Length >= 1) {
                string path = args[0];

                if (args.Length > 1) {
                    _outputFile = args[1];
                }
                FileAttributes attr = File.GetAttributes(path);
                if (attr.HasFlag(FileAttributes.Directory)) {
                    IEnumerable<string> vbFiles = Directory.GetFiles(path, "*.vb", SearchOption.AllDirectories);

                    VBAst vbAst = new VBAst(vbFiles);

                    Wmc wmc = new Wmc();
                    Dcc dcc = new Dcc();
                    Lcom lcomMetric = new Lcom();

                    List<MetricResult> results = new List<MetricResult>();

                    results.AddRange(wmc.WmcMetric(vbAst.ASTRoot, vbFiles.ToList(), "dir"));

                    results.AddRange(dcc.DccMetric(vbAst.ASTRoot, vbFiles.ToList(), "dir"));

                    results.AddRange(lcomMetric.LcomMetric(vbAst.ASTRoot, vbFiles.ToList(), "dir"));

                    if (!string.IsNullOrEmpty(_outputFile)) {
                        OutputCsv(results);
                    }
                    else {
                        results.ForEach((mr) => {
                            Console.WriteLine(mr.MetricName + "= " + mr.NameSpace + "." + mr.ObjectName + ": " + mr.Value);
                        });
                    }
                }
                else {
                    ParseFile(path);
                }
            }
            else {
                Console.WriteLine("Only takes one argument, a folder or file.");
            }
        }

        private static void ParseFile(string file) {
            if (Path.GetExtension(file).ToLower() == VB_EXT) {
                List<string> list = new List<string>() { file };

                Wmc wmcMetric = new Wmc();
                Dcc dccMetric = new Dcc();
                Lcom lcomMetric = new Lcom();

                VBAst vbAst = new VBAst(list);

                List<MetricResult> metricResults = new List<MetricResult>();

                    metricResults = wmcMetric.WmcMetric(vbAst.ASTRoot, list, file);

                    metricResults.AddRange(dccMetric.DccMetric(vbAst.ASTRoot, list, file));
                    metricResults.AddRange(lcomMetric.LcomMetric(vbAst.ASTRoot, list, file));
                

                if (!string.IsNullOrEmpty(_outputFile)) {
                    OutputCsv(metricResults);
                }
                else {
                    metricResults.ForEach((mr) => {
                        Console.WriteLine(mr.MetricName + "= " + mr.NameSpace + "." + mr.ObjectName + ": " + mr.Value);
                    });
                }
            }
            else {
                Console.WriteLine("Unsupported file extension for file: " + file);
            }
        }

        private static void OutputCsv(List<MetricResult> metricResults) {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Metric,Location,Value");
            metricResults.ForEach((mr) => {
                sb.AppendLine(mr.MetricName + "," + mr.NameSpace + "." + mr.ObjectName + "," + mr.Value);
            });

            File.WriteAllText(_outputFile, sb.ToString());
        }
    }
}