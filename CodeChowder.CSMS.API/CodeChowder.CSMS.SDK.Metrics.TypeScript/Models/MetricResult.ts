
import { SourceCodeType } from "./SourceCodeType";

export class MetricResult {
    public MetricName: string;
    public NameSpace: string;
    public ObjectName: string;
    public Location: string;
    public MethodName: string;
    public Value: number;
    public LineNummber: number;
    public ColumnNumber: number;
    public SourceType: SourceCodeType;
    public CreationDate: Date;
}

