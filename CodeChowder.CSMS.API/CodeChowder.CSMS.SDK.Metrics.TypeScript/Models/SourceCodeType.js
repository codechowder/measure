"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SourceCodeType;
(function (SourceCodeType) {
    SourceCodeType[SourceCodeType["CSharp"] = 0] = "CSharp";
    SourceCodeType[SourceCodeType["TypeScript"] = 1] = "TypeScript";
    SourceCodeType[SourceCodeType["TypeScriptReact"] = 2] = "TypeScriptReact";
    SourceCodeType[SourceCodeType["JavaScript"] = 3] = "JavaScript";
    SourceCodeType[SourceCodeType["JavaScriptReact"] = 4] = "JavaScriptReact";
    SourceCodeType[SourceCodeType["VisualBasic"] = 5] = "VisualBasic";
    SourceCodeType[SourceCodeType["Java"] = 6] = "Java";
    SourceCodeType[SourceCodeType["Other"] = 99] = "Other";
})(SourceCodeType = exports.SourceCodeType || (exports.SourceCodeType = {}));
//# sourceMappingURL=SourceCodeType.js.map