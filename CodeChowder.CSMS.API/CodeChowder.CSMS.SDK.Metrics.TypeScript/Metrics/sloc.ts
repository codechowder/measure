import { MetricResult } from '../Models/MetricResult';
import { SourceCodeType } from '../Models/SourceCodeType';

var sloc  = require('sloc');

export class SlocCounter {

    public Count(fileName: string, fileText: string, ext: string, sourceCodeType: SourceCodeType): MetricResult {

        if(ext && ext.length > 0) {
            try {
                let stats = sloc(fileText, ext);
                
                let slocResult: MetricResult = new MetricResult();
                slocResult.Location = fileName;
                slocResult.ObjectName = "Global";
                slocResult.MetricName = "SLOC";
                slocResult.SourceType = sourceCodeType;
                slocResult.Value = stats["source"];
                
                return slocResult;
            } catch (exp) {

            }
        }

        return null;
    }
}