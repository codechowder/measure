"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const MetricResult_1 = require("../Models/MetricResult");
var sloc = require('sloc');
class SlocCounter {
    Count(fileName, fileText, ext, sourceCodeType) {
        if (ext && ext.length > 0) {
            try {
                let stats = sloc(fileText, ext);
                let slocResult = new MetricResult_1.MetricResult();
                slocResult.Location = fileName;
                slocResult.ObjectName = "Global";
                slocResult.MetricName = "SLOC";
                slocResult.SourceType = sourceCodeType;
                slocResult.Value = stats["source"];
                return slocResult;
            }
            catch (exp) {
            }
        }
        return null;
    }
}
exports.SlocCounter = SlocCounter;
//# sourceMappingURL=sloc.js.map