"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ts = require("typescript");
const common_1 = require("./common");
const MetricResult_1 = require("../Models/MetricResult");
class wmc {
    constructor() {
        this._wmc = 0;
    }
    wmcMetric(node, sourceCodeType, fileName) {
        let wmcMetricResults = [];
        let obtainObjects = new common_1.common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes = obtainObjects.retVal;
        this.obtainWmcMetrics(obtainObjects, node, sourceCodeType, fileName, wmcMetricResults);
        classes.forEach((aClass) => {
            this.obtainWmcMetrics(obtainObjects, aClass, sourceCodeType, fileName, wmcMetricResults);
        });
        return wmcMetricResults;
    }
    obtainWmcMetrics(obtainObjects, node, sourceCodeType, fileName, wmcMetricResults) {
        let wmcMetric = new MetricResult_1.MetricResult();
        let objectName = "Global";
        if (node) {
            let aClass = node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }
        wmcMetric.NameSpace = "Default";
        wmcMetric.ObjectName = objectName;
        wmcMetric.MetricName = "WMC";
        wmcMetric.SourceType = sourceCodeType;
        wmcMetric.Location = fileName;
        wmcMetric.CreationDate = new Date();
        wmcMetric.Value = 0;
        obtainObjects = new common_1.common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods = obtainObjects.retVal;
        obtainObjects = new common_1.common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions = obtainObjects.retVal;
        constructors.forEach((construct) => {
            this._wmc = 0;
            this.recursiveBodyWmc(construct);
            wmcMetric.Value += this._wmc + 1;
        });
        methods.forEach((method) => {
            this._wmc = 0;
            this.recursiveBodyWmc(method.body);
            wmcMetric.Value += this._wmc + 1;
        });
        functions.forEach((func) => {
            this._wmc = 0;
            this.recursiveBodyWmc(func.body);
            wmcMetric.Value += this._wmc + 1;
        });
        obtainObjects = new common_1.common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters = obtainObjects.retVal;
        getters.forEach((property) => {
            this._wmc = 0;
            this.recursiveBodyWmc(property);
            wmcMetric.Value += this._wmc + 1;
        });
        obtainObjects = new common_1.common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters = obtainObjects.retVal;
        setters.forEach((property) => {
            this._wmc = 0;
            this.recursiveBodyWmc(property);
            wmcMetric.Value += this._wmc + 1;
        });
        wmcMetricResults.push(wmcMetric);
    }
    recursiveBodyWmc(node) {
        if (node && node.kind) {
            switch (node.kind) {
                case ts.SyntaxKind.IfStatement:
                case ts.SyntaxKind.SwitchStatement:
                case ts.SyntaxKind.WhileStatement:
                case ts.SyntaxKind.CatchClause:
                case ts.SyntaxKind.ForInStatement:
                case ts.SyntaxKind.ForOfStatement:
                case ts.SyntaxKind.ForStatement:
                case ts.SyntaxKind.FunctionDeclaration:
                case ts.SyntaxKind.ArrowFunction:
                case ts.SyntaxKind.MethodDeclaration:
                case ts.SyntaxKind.ConditionalExpression:
                    this._wmc++;
                    break;
            }
        }
        ts.forEachChild(node, (node) => {
            this.recursiveBodyWmc(node);
        });
    }
}
exports.wmc = wmc;
//# sourceMappingURL=wmc.js.map