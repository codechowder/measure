﻿import * as ts from 'typescript';
import { common } from './common';
import { MetricResult } from '../Models/MetricResult';
import { SourceCodeType } from "../Models/SourceCodeType";

export class dcc {

    private _customTypes: string[];
    public constructor() {
        this._customTypes = [];
    }

    public dccMetric(node: ts.Node, sourceCodeType: SourceCodeType, fileName: string): MetricResult[] {

        let dccMetricResults: MetricResult[] = [];

        let obtainObjects: common = new common(ts.SyntaxKind.ClassDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let classes: ts.ClassDeclaration[] = <ts.ClassDeclaration[]>obtainObjects.retVal;

        this.obtainDccMetric(obtainObjects, node, sourceCodeType, fileName, dccMetricResults);

        classes.forEach((aClass: ts.ClassDeclaration) => {
            this.obtainDccMetric(obtainObjects, aClass, sourceCodeType, fileName, dccMetricResults);
        });

        return dccMetricResults;
    }

    private obtainDccMetric(obtainObjects: common, node: ts.Node | ts.ClassDeclaration, sourceCodeType: SourceCodeType, fileName: string, dccMetricResults: MetricResult[]) {
        this._customTypes = [];

        let objectName = "Global";

        if (<ts.ClassDeclaration>node) {
            let aClass = <ts.ClassDeclaration>node;
            objectName = aClass && aClass.name && aClass.name.text || "Global";
        }

        let dccMetric: MetricResult = new MetricResult();
        dccMetric.NameSpace = "Default";
        dccMetric.ObjectName =  objectName;
        dccMetric.MetricName = "DCC";
        dccMetric.Location = fileName;
        dccMetric.SourceType = sourceCodeType;
        dccMetric.CreationDate = new Date();
        dccMetric.Value = 0;

        obtainObjects = new common(ts.SyntaxKind.Constructor);
        obtainObjects.recursiveGrabItem(node);
        let constructors: ts.ConstructorDeclaration[] = <ts.ConstructorDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.MethodDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let methods: ts.MethodDeclaration[] = <ts.MethodDeclaration[]>obtainObjects.retVal;

        obtainObjects = new common(ts.SyntaxKind.FunctionDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let functions: ts.FunctionDeclaration[] = <ts.FunctionDeclaration[]>obtainObjects.retVal;

        constructors.forEach((construct: ts.ConstructorDeclaration) => {
            this.recursiveParameterList(construct);
        });

        methods.forEach((method: ts.MethodDeclaration) => {
            this.recursiveParameterList(method);
            this.recursiveVars(method);

            if (method && method.type && method.kind && method.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(method.type.getText()) === -1) {
                    this._customTypes.push(method.type.getText());
                }
            }
        });

        functions.forEach((func: ts.FunctionDeclaration) => {
            this.recursiveParameterList(func);
            this.recursiveVars(func);

            if (func && func.type && func.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(func.type.getText()) === -1) {
                    this._customTypes.push(func.type.getText());
                }
            }
        });

        obtainObjects = new common(ts.SyntaxKind.PropertyDeclaration);
        obtainObjects.recursiveGrabItem(node);
        let fields: ts.PropertyDeclaration[] = <ts.PropertyDeclaration[]>obtainObjects.retVal;
        fields.forEach((field: ts.PropertyDeclaration) => {
            this.recursiveFields(field);
        });

        obtainObjects = new common(ts.SyntaxKind.GetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let getters: ts.GetAccessorDeclaration[] = <ts.GetAccessorDeclaration[]>obtainObjects.retVal;
        getters.forEach((property: ts.GetAccessorDeclaration) => {

            this.recursiveParameterList(property);

            if (property && property.type && property.type.kind && property.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(property.type.getText()) === -1) {
                    this._customTypes.push(property.type.getText());
                }
            }
        });

        obtainObjects = new common(ts.SyntaxKind.SetAccessor);
        obtainObjects.recursiveGrabItem(node);
        let setters: ts.SetAccessorDeclaration[] = <ts.SetAccessorDeclaration[]>obtainObjects.retVal;
        setters.forEach((property: ts.SetAccessorDeclaration) => {

            this.recursiveParameterList(property);

            if (property && property.type && property.type.kind && property.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(property.type.getText()) === -1) {
                    this._customTypes.push(property.type.getText());
                }
            }
        });

        dccMetric.Value += this._customTypes.length;
        dccMetricResults.push(dccMetric);
    }

    private recursiveVars(node: ts.Node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.VariableDeclaration) {
            let variable: ts.VariableDeclaration = <ts.VariableDeclaration>node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveVars(node);
        });
    }

    private recursiveFields(node: ts.Node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.PropertyDeclaration) {
            let variable: ts.PropertyDeclaration = <ts.PropertyDeclaration>node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveFields(node);
        });
    }

    private recursiveParameterList(node: ts.Node) {
        if (node && node.kind && node.kind === ts.SyntaxKind.Parameter) {
            let variable: ts.ParameterDeclaration = <ts.ParameterDeclaration>node;
            if (variable && variable.type && variable.type.kind === ts.SyntaxKind.TypeReference) {
                if (this._customTypes.indexOf(variable.type.getText()) === -1) {
                    this._customTypes.push(variable.type.getText());
                }
            }
        }
        ts.forEachChild(node, (node: ts.Node) => {
            this.recursiveParameterList(node);
        });
    }
}