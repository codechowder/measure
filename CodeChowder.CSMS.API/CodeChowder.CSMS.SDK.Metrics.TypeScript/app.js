"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const ts = require("typescript");
const path = require("path");
const dcc_1 = require("./Metrics/dcc");
const wmc_1 = require("./Metrics/wmc");
const lcom_1 = require("./Metrics/lcom");
const SourceCodeType_1 = require("./Models/SourceCodeType");
const sloc_1 = require("./Metrics/sloc");
var recursiveReadSync = require('recursive-readdir-sync');
const fileNames = process.argv[2];
const sourceType = process.argv[3];
const ignoreFilesRaw = process.argv[4];
let ignoreFiles = [];
if (ignoreFilesRaw) {
    ignoreFiles = ignoreFilesRaw.split(',');
}
let sourceCodeType = SourceCodeType_1.SourceCodeType[sourceType];
let dccMetrics = new dcc_1.dcc();
let wmcMetrics = new wmc_1.wmc();
let lcomMetrics = new lcom_1.lcom();
let metricResults = [];
let files = recursiveReadSync(fileNames);
files.forEach((fullFilePath) => {
    if (!fullFilePath.endsWith('.d.ts') && !(ignoreFiles && ignoreFiles.length > 0 && ignoreFiles.indexOf(fullFilePath) >= 0)) {
        let splits = fullFilePath.split(fileNames);
        let fileLocation = fullFilePath;
        if (splits.length >= 2) {
            fileLocation = splits[splits.length - 1];
        }
        let ext = path.extname(fullFilePath);
        let fileText = fs_1.readFileSync(fullFilePath).toString();
        if ((sourceCodeType === SourceCodeType_1.SourceCodeType.TypeScript && fullFilePath.endsWith('.ts')) ||
            (sourceCodeType === SourceCodeType_1.SourceCodeType.TypeScriptReact && fullFilePath.endsWith('.tsx')) ||
            (sourceCodeType === SourceCodeType_1.SourceCodeType.JavaScript && fullFilePath.endsWith('.js')) ||
            (sourceCodeType === SourceCodeType_1.SourceCodeType.JavaScriptReact && fullFilePath.endsWith('.jsx'))) {
            let sourceFile = ts.createSourceFile(fileLocation, fileText, ts.ScriptTarget.ES2015, /*setParentNodes */ true);
            let dccValues = dccMetrics.dccMetric(sourceFile, sourceCodeType, fileLocation);
            let wmcValues = wmcMetrics.wmcMetric(sourceFile, sourceCodeType, fileLocation);
            let lcomValues = lcomMetrics.lcomMetric(sourceFile, sourceCodeType, fileLocation);
            let dccGlobalItems = [];
            let wmcGlobalItems = [];
            let lcomGlobalItems = [];
            dccValues.forEach((mr) => {
                if (mr.ObjectName === "Global") {
                    dccGlobalItems.push(mr);
                }
            });
            lcomValues.forEach((mr) => {
                if (mr.ObjectName === "Global") {
                    lcomGlobalItems.push(mr);
                }
            });
            wmcValues.forEach((mr) => {
                if (mr.ObjectName === "Global") {
                    wmcGlobalItems.push(mr);
                }
            });
            for (let i = 0; i < dccValues.length; i++) {
                let mr = dccValues[i];
                for (let j = 0; j < dccGlobalItems.length; j++) {
                    let gmr = dccGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = dccValues.indexOf(gmr);
                            dccGlobalItems.splice(j, 1);
                            dccValues.splice(indexOfGlobal, 1);
                            break;
                        }
                    }
                }
            }
            for (let i = 0; i < lcomValues.length; i++) {
                let mr = lcomValues[i];
                for (let j = 0; j < lcomGlobalItems.length; j++) {
                    let gmr = lcomGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = lcomValues.indexOf(gmr);
                            lcomGlobalItems.splice(j, 1);
                            lcomValues.splice(indexOfGlobal, 1);
                            break;
                        }
                    }
                }
            }
            for (let i = 0; i < wmcValues.length; i++) {
                let mr = wmcValues[i];
                for (let j = 0; j < wmcGlobalItems.length; j++) {
                    let gmr = wmcGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = wmcValues.indexOf(gmr);
                            wmcGlobalItems.splice(j, 1);
                            wmcValues.splice(indexOfGlobal, 1);
                            break;
                        }
                    }
                }
            }
            metricResults.push(...dccValues);
            metricResults.push(...wmcValues);
            metricResults.push(...lcomValues);
            let slocCounter = new sloc_1.SlocCounter();
            let slocValue = slocCounter.Count(fileLocation, fileText, ext.substr(1, ext.length - 1), sourceCodeType);
            if (slocValue) {
                metricResults.push(slocValue);
            }
        }
        else if (sourceCodeType === SourceCodeType_1.SourceCodeType.Other &&
            !fullFilePath.endsWith('.ts') &&
            !fullFilePath.endsWith('.tsx') &&
            !fullFilePath.endsWith('.js') &&
            !fullFilePath.endsWith('.jsx')) {
            let slocCounter = new sloc_1.SlocCounter();
            let sourceCodeTypeSloc = SourceCodeType_1.SourceCodeType.Other;
            if (fileLocation.endsWith(".cs")) {
                sourceCodeTypeSloc = SourceCodeType_1.SourceCodeType.CSharp;
            }
            else if (fileLocation.endsWith(".vb")) {
                sourceCodeTypeSloc = SourceCodeType_1.SourceCodeType.VisualBasic;
            }
            else if (fileLocation.endsWith(".java")) {
                sourceCodeTypeSloc = SourceCodeType_1.SourceCodeType.Java;
            }
            let slocValue = slocCounter.Count(fileLocation, fileText, ext.substr(1, ext.length - 1), sourceCodeTypeSloc);
            if (slocValue) {
                metricResults.push(slocValue);
            }
        }
    }
});
console.log('[');
for (let i = 0; i < metricResults.length; i++) {
    console.log(JSON.stringify(metricResults[i]));
    if (i < metricResults.length - 1) {
        console.log(',');
    }
}
console.log(']');
//# sourceMappingURL=app.js.map