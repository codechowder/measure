﻿import { readFileSync, readdirSync } from "fs";
import * as ts from "typescript";
import * as path from "path";
import { dcc } from "./Metrics/dcc";
import { wmc } from "./Metrics/wmc";
import { lcom } from "./Metrics/lcom";
import { SourceCodeType } from "./Models/SourceCodeType";
import { MetricResult } from "./Models/MetricResult";
import { SlocCounter } from "./Metrics/sloc";
var recursiveReadSync = require('recursive-readdir-sync');


const fileNames = process.argv[2];
const sourceType = process.argv[3];
const ignoreFilesRaw = process.argv[4];

let ignoreFiles: string[] = [];

if(ignoreFilesRaw) {
    ignoreFiles = ignoreFilesRaw.split(',');
}

let sourceCodeType = SourceCodeType[sourceType];
let dccMetrics: dcc = new dcc();
let wmcMetrics: wmc = new wmc();
let lcomMetrics: lcom = new lcom();

let metricResults: MetricResult[] = [];

let files = recursiveReadSync(fileNames);

files.forEach((fullFilePath: string) => {

    if (!fullFilePath.endsWith('.d.ts') && !(ignoreFiles && ignoreFiles.length > 0 && ignoreFiles.indexOf(fullFilePath) >= 0) ) {
        let splits: string[] = fullFilePath.split(fileNames);
        let fileLocation: string = fullFilePath;
        if(splits.length >= 2) {
            fileLocation = splits[splits.length - 1];
        }
        let ext: string = path.extname(fullFilePath);
        let fileText: string = readFileSync(fullFilePath).toString();

        if ((sourceCodeType === SourceCodeType.TypeScript && fullFilePath.endsWith('.ts')) || 
            (sourceCodeType === SourceCodeType.TypeScriptReact && fullFilePath.endsWith('.tsx')) || 
            (sourceCodeType === SourceCodeType.JavaScript && fullFilePath.endsWith('.js')) || 
            (sourceCodeType === SourceCodeType.JavaScriptReact && fullFilePath.endsWith('.jsx'))) {

            let sourceFile = ts.createSourceFile(fileLocation, fileText, ts.ScriptTarget.ES2015, /*setParentNodes */ true);

            let dccValues = dccMetrics.dccMetric(sourceFile, sourceCodeType, fileLocation);
            let wmcValues = wmcMetrics.wmcMetric(sourceFile, sourceCodeType, fileLocation);
            let lcomValues = lcomMetrics.lcomMetric(sourceFile, sourceCodeType, fileLocation);

            let dccGlobalItems: MetricResult[] = [];
            let wmcGlobalItems: MetricResult[] = [];
            let lcomGlobalItems: MetricResult[] = [];

            dccValues.forEach((mr: MetricResult) => {
                if (mr.ObjectName === "Global") {
                    dccGlobalItems.push(mr);
                }
            })

            lcomValues.forEach((mr: MetricResult) => {
                if (mr.ObjectName === "Global") {
                    lcomGlobalItems.push(mr);
                }
            })

            wmcValues.forEach((mr: MetricResult) => {
                if (mr.ObjectName === "Global") {
                    wmcGlobalItems.push(mr);
                }
            });

            for (let i: number = 0; i < dccValues.length; i++) {
                let mr: MetricResult = dccValues[i];
                for (let j: number = 0; j < dccGlobalItems.length; j++) {
                    let gmr: MetricResult = dccGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = dccValues.indexOf(gmr);
                            dccGlobalItems.splice(j, 1);
                            dccValues.splice(indexOfGlobal, 1);
                            break;
                        }
                    }
                }
            }

            for (let i: number = 0; i < lcomValues.length; i++) {
                let mr: MetricResult = lcomValues[i];
                for (let j: number = 0; j < lcomGlobalItems.length; j++) {
                    let gmr: MetricResult = lcomGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = lcomValues.indexOf(gmr);
                            lcomGlobalItems.splice(j, 1);
                            lcomValues.splice(indexOfGlobal, 1);
                            break; 
                        }
                    }
                }
            }

            for (let i: number = 0; i < wmcValues.length; i++) {
                let mr: MetricResult = wmcValues[i];
                for (let j: number = 0; j < wmcGlobalItems.length; j++) {
                    let gmr: MetricResult = wmcGlobalItems[j];
                    if (mr != gmr) {
                        if (mr.Location === gmr.Location && mr.Value === gmr.Value) {
                            let indexOfGlobal = wmcValues.indexOf(gmr);
                            wmcGlobalItems.splice(j, 1);
                            wmcValues.splice(indexOfGlobal, 1);
                            break;
                        }
                    }
                }
            }

            metricResults.push(...dccValues);
            metricResults.push(...wmcValues);
            metricResults.push(...lcomValues);

            let slocCounter: SlocCounter = new SlocCounter();

            let slocValue: MetricResult = slocCounter.Count(fileLocation, fileText, ext.substr(1, ext.length - 1), sourceCodeType);

            if (slocValue) {
                metricResults.push(slocValue);
            }
        }
        else if(sourceCodeType === SourceCodeType.Other && 
        !fullFilePath.endsWith('.ts') &&
        !fullFilePath.endsWith('.tsx') && 
        !fullFilePath.endsWith('.js') &&
        !fullFilePath.endsWith('.jsx')) {

            let slocCounter: SlocCounter = new SlocCounter();
            let sourceCodeTypeSloc: SourceCodeType = SourceCodeType.Other;

            if(fileLocation.endsWith(".cs")) {
                sourceCodeTypeSloc = SourceCodeType.CSharp;
            }
            else if(fileLocation.endsWith(".vb")) {
                sourceCodeTypeSloc = SourceCodeType.VisualBasic;
            }
            else if(fileLocation.endsWith(".java")) {
                sourceCodeTypeSloc = SourceCodeType.Java;
            }

            let slocValue: MetricResult = slocCounter.Count(fileLocation, fileText, ext.substr(1, ext.length - 1), sourceCodeTypeSloc);

            if (slocValue) {
                metricResults.push(slocValue);
            }
        }
    }

});

console.log('[');
for (let i = 0; i < metricResults.length; i++) {
    console.log(JSON.stringify(metricResults[i]));
    if (i < metricResults.length - 1) {
        console.log(',');
    }
}
console.log(']');