﻿using System;
using AutoMapper;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.License.API.DTOs;

namespace CodeChowder.CSMS.License.API.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            AllowNullCollections = true;

            CreateMap<ApplicationUser, ApplicationUserDto>();
            CreateMap<ApplicationUserDto, ApplicationUser>()
                .ForMember(u => u.Id, opt => opt.Ignore())
                .ForMember(u => u.Projects, opt => opt.Ignore())
                .ForMember(u => u.SharedProjects, opt => opt.Ignore())
                .ForMember(u => u.License, opt => opt.Ignore());
        }
    }
}
