﻿using System;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.License.API.DTOs
{
    public class ApplicationUserDto
    {
		public int Id { get; set; }

		public string Email { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string PhoneNumber { get; set; }

		public string Company { get; set; }

		public bool Active { get; set; }

		public int AllowedProjects { get; set; }

		public LicenseKey License { get; set; }
    }
}
