﻿using System;
using System.Collections.Generic;
using CodeChowder.CSMS.DataStructures.Models;

namespace CodeChowder.CSMS.License.API.DTOs
{
    public class ProjectDto
    {
		public int Id { get; set; }

		public string Name { get; set; }

		public string SourceRepoUrl { get; set; }
    }
}
