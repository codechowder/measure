﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using CodeChowder.CSMS.DAL.Services;
using CodeChowder.CSMS.DAL.Services.Interfaces;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.License.API.Security;
using CodeChowder.CSMS.License.API.Settings;
using CodeChowder.CSMS.Repository;
using CodeChowder.CSMS.Repository.Contexts;
using CodeChowder.CSMS.Repository.Repositories;
using CodeChowder.CSMS.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CodeChowder.CSMS.License.API
{
    public partial class Startup
	{
		public Startup(IHostingEnvironment env)
		{
			var builder = new ConfigurationBuilder()
				.SetBasePath(env.ContentRootPath)
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

			if (env.IsEnvironment("Development"))
			{
				// This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
				builder.AddApplicationInsightsSettings(developerMode: true);
			}

			builder.AddEnvironmentVariables();
			Configuration = builder.Build();
		}

		public IContainer ApplicationContainer { get; private set; }

		public IConfigurationRoot Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container
		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			// Add framework services.
			services.AddApplicationInsightsTelemetry(Configuration);

			services.AddCors(options =>
			{
				options.AddPolicy("CorsPolicy",
					b => b.AllowAnyOrigin()
					.AllowAnyMethod()
					.AllowAnyHeader()
					.AllowCredentials());
			});

			var defaultSettings = Configuration.GetSection("Defaults");

			services.Configure<DefaultSettings>(defaultSettings);

			var tokenSettings = Configuration.GetSection("TokenAuthentication");
			services.Configure<TokenSettings>(tokenSettings);

			services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
			services.AddTransient<IAuthorizationHandler, AccountHandler>();

			services.AddAuthorization(options =>
			{
				options.AddPolicy("Default",
								  policy => policy.Requirements.Add(new AccountRequirement()));
			});

			services.AddMvc().AddJsonOptions(options =>
			{
				options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
			});
			services.AddAutoMapper();

			//Create the container builder
			var builder = new ContainerBuilder();

			// Register dependencies, populate the services from
			// the collection, and build the container. If you want
			// to dispose of the container at the end of the app,
			// be sure to keep a reference to it as a property or field.
			builder.Populate(services);

			string connectionString = defaultSettings.GetValue<string>("ConnectionString");

			string envConnectionString = Environment.GetEnvironmentVariable("MYSQL_CONNECTION_STRING");

			if (!string.IsNullOrEmpty(envConnectionString))
			{
				connectionString = envConnectionString;
			}

			builder.RegisterType<CSMSDbContext>().WithParameter("connectionString", connectionString);
			builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();

			// Repositories
			builder.RegisterType<ApplicationUserRepository>().As<IRepository<ApplicationUser>>();
			builder.RegisterType<LicenseKeyRepository>().As<IRepository<LicenseKey>>();

			// Services
			builder.RegisterType<ApplicationUserService>().As<IApplicationUserService>();
			builder.RegisterType<LicenseKeyService>().As<ILicenseKeyService>();

			this.ApplicationContainer = builder.Build();



			// Create the IServiceProvider based on the container.
			return new AutofacServiceProvider(this.ApplicationContainer);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime appLifetime)
		{
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			ConfigureAuth(app);

			app.UseCors("CorsPolicy");

			app.UseMvc();

			// If you want to dispose of resources that have been resolved in the
			// application container, register for the "ApplicationStopped" event.
			// You can only do this if you have a direct reference to the container,
			// so it won't work with the above ConfigureContainer mechanism.
			appLifetime.ApplicationStopped.Register(() => this.ApplicationContainer.Dispose());
		}
	}
}
