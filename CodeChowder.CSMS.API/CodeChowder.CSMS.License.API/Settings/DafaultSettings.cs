﻿using System;
namespace CodeChowder.CSMS.License.API.Settings
{
	public class DefaultSettings
	{
		public int AllowedProjects { get; set; }
		public int DefaultAccount { get; set; }
		public string ConnectionString { get; set; }
		public int MaxFreeMetrics { get; set; }
	}
}
