﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CodeChowder.CSMS.DataStructures.Models;
using CodeChowder.CSMS.License.API.Security;
using CodeChowder.CSMS.Repository.Contexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace CodeChowder.CSMS.License.API
{
	public partial class Startup
	{
		private void ConfigureAuth(IApplicationBuilder app)
		{
			var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("TokenAuthentication:SecretKey").Value));

			app.UseCors("CorsPolicy");

			var tokenProviderOptions = new TokenProviderOptions
			{
				Path = Configuration.GetSection("TokenAuthentication:TokenPath").Value,
				Audience = Configuration.GetSection("TokenAuthentication:Audience").Value,
				Issuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
				SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
				IdentityResolver = GetIdentity
			};

			var tokenValidationParameters = new TokenValidationParameters
			{
				// The signing key must match!
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = signingKey,
				// Validate the JWT Issuer (iss) claim
				ValidateIssuer = false,
				ValidIssuer = Configuration.GetSection("TokenAuthentication:Issuer").Value,
				// Validate the JWT Audience (aud) claim
				ValidateAudience = false,
				ValidAudience = Configuration.GetSection("TokenAuthentication:Audience").Value,
				// Validate the token expiry
				ValidateLifetime = true,
				// If you want to allow a certain amount of clock drift, set that here:
				ClockSkew = TimeSpan.Zero
			};

			app.UseJwtBearerAuthentication(new JwtBearerOptions
			{
				AutomaticAuthenticate = true,
				AutomaticChallenge = true,
				TokenValidationParameters = tokenValidationParameters
			});

			app.UseMiddleware<TokenProviderMiddleware>(Options.Create(tokenProviderOptions));
		}

		private Task<ClaimsIdentity> GetIdentity(string email, string password)
		{
			var defaultSettings = Configuration.GetSection("Defaults");

			string connectionString = defaultSettings.GetValue<string>("ConnectionString");

			string envConnectionString = Environment.GetEnvironmentVariable("MYSQL_CONNECTION_STRING");

			if (!string.IsNullOrEmpty(envConnectionString))
			{
				connectionString = envConnectionString;
			}

			using (CSMSDbContext context = new CSMSDbContext(connectionString))
			{
				string hash = string.Empty;
				byte[] data = System.Text.Encoding.ASCII.GetBytes(password);
				using (var sha512 = System.Security.Cryptography.SHA512.Create())
				{
					data = sha512.ComputeHash(data);
					hash = Encoding.ASCII.GetString(data);
				}

				ApplicationUser user = context.ApplicationUsers.FirstOrDefault(u => u.Email == email && u.Password == hash);
				if (user != null)
				{
					ClaimsIdentity identity = new ClaimsIdentity();

					Claim userSpecificActions = new Claim("UserId", user.Id.ToString());
					Claim userRole = new Claim("Role", user.Account.ToString());

					return Task.FromResult(new ClaimsIdentity(identity, new Claim[] { userSpecificActions, userRole }));

				}

			}

			// Account doesn't exists
			return Task.FromResult<ClaimsIdentity>(null);
		}
	}
}
