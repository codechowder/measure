﻿using System.Linq;
using System.Security.Claims;
using CodeChowder.CSMS.License.API.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CodeChowder.CSMS.License.API.Controllers
{
    /// <summary>
    /// Base controller for holding the user id from the token.
    /// </summary>
    public class BaseController : Controller
	{
		/// <summary>
		/// Gets the current user identifier.
		/// </summary>
		/// <value>The current user identifier.</value>
		public int CurrentUserId { get; private set; }

		public override void OnActionExecuting(ActionExecutingContext context)
		{
			string authorization = context.HttpContext.Request.Headers["Authorization"];

			if (!string.IsNullOrEmpty(authorization))
			{

				string token = string.Empty;
				ClaimsIdentity identity = null;

				if (authorization.StartsWith("JWT ", System.StringComparison.OrdinalIgnoreCase))
				{
					token = authorization.Substring("JWT ".Length).Trim();

				}

				if (string.IsNullOrEmpty(Utilities.ValidateToken(token, out identity)))
				{
					Claim userIdClaim = identity.Claims.FirstOrDefault(c => c.Type == "UserId");

					if (userIdClaim != null)
					{
						CurrentUserId = int.Parse(userIdClaim.Value);
					}
				}
			}
			base.OnActionExecuting(context);
		}
	}
}
