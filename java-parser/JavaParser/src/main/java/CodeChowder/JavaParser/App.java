package CodeChowder.JavaParser;
/**
 * Hello world!
 *
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.google.gson.Gson;

import CodeChowder.Metrics.Dcc;
import CodeChowder.Metrics.Lcom;
import CodeChowder.Metrics.Wmc;
import CodeChowder.Models.MetricResult;

public class App 
{
    public static void main( String[] args )
    {
    		String path = args[0];
    		
    		Path p = Paths.get(path);
    		
    		ArrayList<MetricResult> metricResults = new ArrayList<MetricResult>();
    		
    	    FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
    	      @Override
    	      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
    	          throws IOException {
    	        int i = file.getFileName().toString().lastIndexOf('.');
    	        if (i > 0) {
    	            String extension = file.getFileName().toString().substring(i+1);
    	            
    	            if(extension.toLowerCase().equals("java")) {
    	            	
    	            		// creates an input stream for the file to be parsed
    	                FileInputStream in = new FileInputStream(file.toString());
    	            	
    	            		// parse the file
    	                CompilationUnit cu = JavaParser.parse(in);
    	            	
    	            		Dcc dcc = new Dcc();
    	            		metricResults.addAll(dcc.DccMetric(cu, file.toString(), path));
    	            		Wmc wmc = new Wmc();
    	            		metricResults.addAll(wmc.WmcMetric(cu, file.toString(), path));
    	            		Lcom lcom = new Lcom();
    	            		metricResults.addAll(lcom.LcomMetric(cu, file.toString(), path));
    	            }
    	            
    	        }
    	        return FileVisitResult.CONTINUE;
    	      }
    	    };

    	    try {
    	      Files.walkFileTree(p, fv);
    	      
    	      /*
    	       * console.log('[');
			for (let i = 0; i < metricResults.length; i++) {
			    console.log(JSON.stringify(metricResults[i]));
			    if (i < metricResults.length - 1) {
			        console.log(',');
			    }
			}
			console.log(']');
    	       */
    	      System.out.println("[");
    	      Gson gson = new Gson();
    	      for(int i = 0; i < metricResults.size(); i++) {
    	    	  	String json = gson.toJson(metricResults.get(i));
    	    	  	System.out.println(json);
    	    	  	if(i < metricResults.size() - 1) {
    	    	  		System.out.println(",");
    	    	  	}
    	      }
    	      
    	      System.out.println("]");
    	      
    	    } catch (IOException e) {
    	    		e.printStackTrace();
    	    }
    }
}
