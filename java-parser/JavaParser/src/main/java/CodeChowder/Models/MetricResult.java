package CodeChowder.Models;

import java.sql.Date;

public class MetricResult {
	public String MetricName;
	public String NameSpace;
	public String ObjectName;
	public String Location;
	public String MethodName;
	public Integer Value;
	public Integer LineNumber;
	public Integer ColumnNumber;
	public Integer SourceType;
	public Date CreationDate;
}
