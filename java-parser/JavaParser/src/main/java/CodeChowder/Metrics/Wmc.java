package CodeChowder.Metrics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.expr.ConditionalExpr;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.LambdaExpr;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.stmt.ForStmt;
import com.github.javaparser.ast.stmt.IfStmt;
import com.github.javaparser.ast.stmt.Statement;
import com.github.javaparser.ast.stmt.SwitchStmt;
import com.github.javaparser.ast.stmt.TryStmt;
import com.github.javaparser.ast.stmt.WhileStmt;
import CodeChowder.Models.MetricResult;

public class Wmc {
    public ArrayList<MetricResult> WmcMetric(CompilationUnit unit, String fileName, String fileLocation) {
    	ArrayList<MetricResult> wmcResults = new ArrayList<MetricResult>();

        ArrayList<ClassOrInterfaceDeclaration> classes = new ArrayList<ClassOrInterfaceDeclaration>();
    		classes = ClassVisitor.Visitor(unit);	        
        
        for(ClassOrInterfaceDeclaration cds : classes) {
        		int wmc = 0;
            MetricResult wmcResult = new MetricResult();
            
            Optional<PackageDeclaration> packageDecl = unit.getPackageDeclaration();
            
            wmcResult.NameSpace = packageDecl.isPresent() ? packageDecl.get().getName().asString() : "Global";
            wmcResult.ObjectName = cds.getNameAsString();
            wmcResult.MetricName = "WMC";
            wmcResult.Location = fileName.replace(fileLocation, "");
            wmcResult.SourceType = 6;
            wmcResult.CreationDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());

            ArrayList<ConstructorDeclaration> constructors = ConstructorVisitor.Visitor(cds);

            ArrayList<MethodDeclaration> methods = MethodVisitor.Visitor(cds);

            for (ConstructorDeclaration constructor : constructors) {
            		BlockStmt block = constructor.getBody();
                wmc = BodyWmc(block, wmc);
            }

            //Grab all the decisions from the method body
            for (MethodDeclaration method : methods) {
            	Optional<BlockStmt> block = method.getBody();
            		if(block.isPresent()) {
            			wmc = BodyWmc(block.get(), wmc);
            		}
            }
            
            wmcResult.Value = wmc;
            wmcResults.add(wmcResult);
        }

        return wmcResults;
    }
    
    private int BodyWmc(BlockStmt block, int wmc) {
    		for(Node node : block.getChildNodes()) {
    			wmc = RecursiveGetWmc(node, wmc);
    		}
    		return wmc;
    }
    
    private int StatementWmc(Statement statement, int wmc) {
		if(statement instanceof IfStmt) {
			IfStmt ifStmt = (IfStmt) statement;
			wmc++;
			if(ifStmt.hasElseBlock()) {
				wmc++;
			}
		}
		else if(statement instanceof SwitchStmt) {
			wmc++;
		}
		else if(statement instanceof WhileStmt) {
			wmc++;
		}
		else if(statement instanceof ForStmt) {
			wmc++;
		}
		else if(statement instanceof TryStmt) {
			TryStmt tryStmt = (TryStmt) statement;
			if(tryStmt.getCatchClauses() != null) {
				wmc += tryStmt.getCatchClauses().size();
			}
		}
		
		for(Node node : statement.getChildNodes()) {
			wmc = RecursiveGetWmc(node, wmc);
		}

		return wmc;
    }
    
    private int ExpressionWmc(Expression expression, int wmc) {
    		if(expression instanceof ConditionalExpr) {
    			wmc++;
    		}
    		else if(expression instanceof LambdaExpr) {
    			wmc++;
    		}
    		
    		//TODO: How to count filters?  (equivalent to Where Clause in C#)
    		
    		for(Node node : expression.getChildNodes()) {
    			wmc = RecursiveGetWmc(node, wmc);
    		}
    		
    		return wmc;
    }
    
    private int RecursiveGetWmc(Node node, int wmc) {
    		if(node instanceof BlockStmt) {
    			return BodyWmc((BlockStmt)node, wmc);
    		}
    		else if(node instanceof Statement) {
    			return StatementWmc((Statement)node, wmc);
    		}
    		else if(node instanceof Expression) {
    			return ExpressionWmc((Expression)node, wmc);
    		}
    		else {
    			for(Node cnode : node.getChildNodes()) {
    				return RecursiveGetWmc(cnode, wmc);
    			}
    		}
    		
    		return wmc;
    }
}
