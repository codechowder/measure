package CodeChowder.Metrics;

import java.util.ArrayList;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class ClassVisitor {
	public static ArrayList<ClassOrInterfaceDeclaration> Visitor(CompilationUnit unit) {
		ArrayList<ClassOrInterfaceDeclaration> classes = new ArrayList<ClassOrInterfaceDeclaration>();
	        new VoidVisitorAdapter<Object>() {
			    @Override
			    public void visit(ClassOrInterfaceDeclaration n, Object arg) {
			        super.visit(n, arg);
			        classes.add(n);
			    }
			}.visit(unit, null);
	        
	        return classes;
	}
}
