package CodeChowder.Metrics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.Expression;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.NameExpr;
import com.github.javaparser.ast.stmt.ExpressionStmt;

import CodeChowder.Models.MetricResult;

public class Lcom {
	
	private ArrayList<String> _fields;
	private ArrayList<String> _methodSet;
	
	public ArrayList<MetricResult> LcomMetric(CompilationUnit unit, String fileName, String fileLocation) {
        ArrayList<MetricResult> lcomResults = new ArrayList<MetricResult>();
        ArrayList<ClassOrInterfaceDeclaration> classes = new ArrayList<ClassOrInterfaceDeclaration>();
        
    		classes = ClassVisitor.Visitor(unit);	        
        
        for(ClassOrInterfaceDeclaration cds : classes) {
        		_methodSet = new ArrayList<String>();
        		int lcom = 0;
        		// The Cardinality of set P
        		int 	p = 0;
        		//The Cardinality of set Q
        		int 	q = 0;
    			
            MetricResult lcomResult = new MetricResult();
            
            Optional<PackageDeclaration> packageDecl = unit.getPackageDeclaration();
            
            lcomResult.NameSpace = packageDecl.isPresent() ? packageDecl.get().getName().asString() : "Global";
            lcomResult.ObjectName = cds.getNameAsString();
            lcomResult.MetricName = "LCOM";
            lcomResult.Location = fileName.replace(fileLocation, "");
            lcomResult.SourceType = 6;
            lcomResult.CreationDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());

            ArrayList<FieldDeclaration> fields = FieldVisitor.Visitor(cds);
            _fields = GetFieldNames(fields);
            
            ArrayList<ConstructorDeclaration> constructors = ConstructorVisitor.Visitor(cds);

            ArrayList<MethodDeclaration> methods = MethodVisitor.Visitor(cds);
            
            int methodCount = constructors.size() + methods.size();
            
            ArrayList<ArrayList<String>> I = new ArrayList<ArrayList<String>>();

            for (ConstructorDeclaration constructor : constructors) {
                RecursiveVariables(constructor.getChildNodes());
            }

            //Grab all the decisions from the method body
            for (MethodDeclaration method : methods) {
            		RecursiveVariables(method.getChildNodes());
            }

            //Keep the sets of accessed instance variables around. We will
            //need them for the next step of the calculation.
            if(_methodSet.size() > 0) {
            		I.add(_methodSet);
            		for(int i = 0; i < methodCount; i++) {
            			for(int j = 0; j < methodCount; j++) {
            				ArrayList<String> intersection = null;
            				if(i < I.size() && j < I.size()) {
            					intersection = I.get(i);
            					intersection.retainAll(I.get(j));
            				}
            				//check if the set is empty
            				if(intersection == null || intersection.size() == 0) {
            					++p;
            				}
            				else {
            					++q;
            				}
            			}
            		}
            }
            
            //# Now p is the number of pairs of methods which don't have
            //# have any instance variable accesses in Roslyn.Parsers.Common.
            //#
            //# q is the number of pairs of methods which DO have an instance
            //# variable access in common

            if (p > q) {
                lcom = p - q;
            }

            lcomResult.Value = lcom;
            lcomResults.add(lcomResult);
        }

        return lcomResults;
    }
	
	private ArrayList<String> GetFieldNames(ArrayList<FieldDeclaration> fields) {
		_fields = new ArrayList<String>();
		for (FieldDeclaration field : fields) {
            for (VariableDeclarator vds : field.getVariables()) {
                String identifier = vds.getNameAsString();
                if(!_fields.contains(identifier)) {
                		_fields.add(identifier);
                }
            }
		}
		
		return _fields;
	}
	
	private void RecursiveVariables(List<Node> nodes) {
		for(Node node : nodes) {
			if(node instanceof VariableDeclarator) {
				VariableDeclarator vd = (VariableDeclarator) node;
				String identifier = vd.getType().getElementType().asString();
				if(_fields.contains(identifier) && !_methodSet.contains(identifier)) {
					_methodSet.add(identifier);
				}
			}
			else if(node instanceof MethodCallExpr) {
				MethodCallExpr expressionStmt = (MethodCallExpr) node;
				NodeList<Expression> arguements = expressionStmt.getArguments();
				for(Expression expression : arguements) {
					if(expression instanceof NameExpr) {
						String identifier = ((NameExpr)expression).getNameAsString();
						if(_fields.contains(identifier) && !_methodSet.contains(identifier)) {
							_methodSet.add(identifier);
						}
					}
				}
				
			}
			if(node.getChildNodes().size() > 0) {
				RecursiveVariables(node.getChildNodes());
			}
		}
	}
    
}
