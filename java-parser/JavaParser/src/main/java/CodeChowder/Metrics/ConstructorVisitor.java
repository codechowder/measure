package CodeChowder.Metrics;

import java.util.ArrayList;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class ConstructorVisitor {
	public static ArrayList<ConstructorDeclaration> Visitor(ClassOrInterfaceDeclaration cds) {
		ArrayList<ConstructorDeclaration> constructors = new ArrayList<ConstructorDeclaration>();
	        new VoidVisitorAdapter<Object>() {
			    @Override
			    public void visit(ConstructorDeclaration n, Object arg) {
			        super.visit(n, arg);
			        constructors.add(n);
			    }
			}.visit(cds, null);
	        
	        return constructors;
	}
}
