package CodeChowder.Metrics;

import java.util.ArrayList;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class MethodVisitor {
	public static ArrayList<MethodDeclaration> Visitor(ClassOrInterfaceDeclaration cds) {
		ArrayList<MethodDeclaration> constructors = new ArrayList<MethodDeclaration>();
	        new VoidVisitorAdapter<Object>() {
			    @Override
			    public void visit(MethodDeclaration n, Object arg) {
			        super.visit(n, arg);
			        constructors.add(n);
			    }
			}.visit(cds, null);
	        
	        return constructors;
	}
}
