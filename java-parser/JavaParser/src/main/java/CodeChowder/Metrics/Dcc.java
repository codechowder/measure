package CodeChowder.Metrics;
import CodeChowder.Models.MetricResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.ConstructorDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.type.PrimitiveType;

public class Dcc {

    public ArrayList<MetricResult> DccMetric(CompilationUnit unit, String fileName, String fileLocation) {
        ArrayList<MetricResult> dccResults = new ArrayList<MetricResult>();

        ArrayList<ClassOrInterfaceDeclaration> classes = new ArrayList<ClassOrInterfaceDeclaration>();
        
    		classes = ClassVisitor.Visitor(unit);	        
        
        for(ClassOrInterfaceDeclaration cds : classes) {
    				
            ArrayList<String> customTypes = new ArrayList<String>();
            MetricResult dccResult = new MetricResult();
            
            Optional<PackageDeclaration> packageDecl = unit.getPackageDeclaration();
            
            dccResult.NameSpace = packageDecl.isPresent() ? packageDecl.get().getName().asString() : "Global";
            dccResult.ObjectName = cds.getNameAsString();
            dccResult.MetricName = "DCC";
            dccResult.Location = fileName.replace(fileLocation, "");
            dccResult.SourceType = 6;
            dccResult.CreationDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            dccResult.Value = 0;

            ArrayList<ConstructorDeclaration> constructors = ConstructorVisitor.Visitor(cds);

            ArrayList<MethodDeclaration> methods = MethodVisitor.Visitor(cds);

            for (ConstructorDeclaration constructor : constructors) {
                customTypes = RecusriveParameterList(constructor.getParameters(), customTypes);
            }

            //Grab all the decisions from the method body
            for (MethodDeclaration method : methods) {
                customTypes = RecusriveParameterList(method.getParameters(), customTypes);

                //Check Return Types
                if (!method.getType().getElementType().asString().equals("void") && !(method.getType() instanceof PrimitiveType)) {
                		String identifier = method.getType().getElementType().asString();
                	   if(!customTypes.contains(identifier)) {
                		   customTypes.add(identifier);
                	   }
                }
            }

            ArrayList<FieldDeclaration> fields = FieldVisitor.Visitor(cds);
            customTypes = RecusiveField(fields, customTypes);
            dccResult.Value += customTypes.size();
            dccResults.add(dccResult);
        }

        return dccResults;
    }

    private ArrayList<String> RecusiveField(ArrayList<FieldDeclaration> nodes, ArrayList<String> customTypes) {
        for (FieldDeclaration node : nodes) {
            for (VariableDeclarator vds : node.getVariables()) {
                
                if (!(vds.getType().getElementType() instanceof PrimitiveType)) {
                    String identifier = vds.getType().getElementType().asString();
                    if (!customTypes.contains(identifier)) {
                        customTypes.add(identifier);
                    }
                }
            }
        }
        return customTypes;
    }

    private ArrayList<String> RecusriveParameterList(NodeList<Parameter> parameters, ArrayList<String> customTypes) {
        for (Parameter node : parameters) {
            if (!node.getType().getElementType().asString().equals("void") && !(node.getType() instanceof PrimitiveType)) {
               String identifier = node.getType().getElementType().asString();
	     	   if(!customTypes.contains(identifier)) {
	     		   customTypes.add(identifier);
	     	   }
            }
        }
        
        return customTypes;
    }
	
}

