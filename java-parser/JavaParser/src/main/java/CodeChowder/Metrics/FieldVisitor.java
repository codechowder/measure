package CodeChowder.Metrics;

import java.util.ArrayList;

import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

public class FieldVisitor {
	public static ArrayList<FieldDeclaration> Visitor(ClassOrInterfaceDeclaration cds) {
		ArrayList<FieldDeclaration> fields = new ArrayList<FieldDeclaration>();
	        new VoidVisitorAdapter<Object>() {
			    @Override
			    public void visit(FieldDeclaration n, Object arg) {
			        super.visit(n, arg);
			        fields.add(n);
			    }
			}.visit(cds, null);
	        
	        return fields;
	}
}
